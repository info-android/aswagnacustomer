package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;

public class ProductDetailsFeaturAdapter extends RecyclerView.Adapter<ProductDetailsFeaturAdapter.ViewHolder> {

    private Activity activity;
    private ArrayList<HashMap<String ,String >> arrayList;

    public ProductDetailsFeaturAdapter(Activity activity, ArrayList<HashMap<String,String>> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_feacher_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map= arrayList.get(position);

        holder.name_tv.setText(map.get(NAME)+": ");
        holder.value_tv.setText(map.get(VARY_VALUE));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name_tv,value_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            name_tv = (TextView) itemView.findViewById(R.id.name_tv);
            value_tv = (TextView) itemView.findViewById(R.id.value_tv);
        }
    }
}
