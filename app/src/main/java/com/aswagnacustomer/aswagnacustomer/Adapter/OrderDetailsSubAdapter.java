package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.BaseRatingBar;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COMMENT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COMMENTS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CURRENCY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IDS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IDSS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_VARIANT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_VARIANT_IDS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.QTY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.RATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.RATINGS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.REVIEW_DONE2;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SELLER_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.STATUS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TOTAL_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.UNIT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;


public class OrderDetailsSubAdapter extends RecyclerView.Adapter<OrderDetailsSubAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;
    String LANG_ID="";

    int pos = 0;
    public OrderDetailsSubAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
        if (PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG","").equals("en")){
         LANG_ID="1";
        }else {
            LANG_ID="2";
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.order_detals_sub_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        Picasso.get().load(PRODUCT_IMAGE_URL+map.get(IMAGE)).placeholder(R.drawable.logo).error(R.drawable.logo).into(holder.pro_pic);
        holder.product_title_tv.setText(map.get(TITLE));
        holder.unit_price_tv.setText(activity.getResources().getString(R.string.unit_price)+CURRENCY+" "+map.get(UNIT_PRICE));
        holder.total_price_tv.setText(activity.getResources().getString(R.string.total_price)+CURRENCY+" "+map.get(TOTAL_PRICE));

        for(int i=0; i < PRODUCT_IDS.length; i++){
            if(PRODUCT_IDS[i]==0) {
                pos = i;;
                break;
            }
        }

        try{
            String variants = map.get(VARY_VALUE);
            String final_vary = "";
            if (!variants.equals("null") && !variants.equals("") && !variants.equals(null)) {
                JSONArray variants_jsonArray = new JSONArray(variants);
                StringBuilder stringBuilder = new StringBuilder();
                for (int j = 0; j < variants_jsonArray.length(); j++) {
                    String variants_str = variants_jsonArray.getJSONObject(j).getJSONObject(LANG_ID).getString("variant");
                    String variants_value_str = variants_jsonArray.getJSONObject(j).getJSONObject(LANG_ID).getString("variant_value");
                    String individual_desc = variants_str + " : " + variants_value_str;
                    Log.e("individual_desc", individual_desc);
                    stringBuilder.append(individual_desc);
                    if (j < (variants_jsonArray.length() - 1)) {
                        stringBuilder.append(" | ");
                    }
                }
                final_vary = stringBuilder.toString();
                holder.variant_tv.setText(final_vary);
                holder.variant_tv.setVisibility(View.VISIBLE);
            }else {
                holder.variant_tv.setVisibility(View.GONE);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        holder.simpleRatingBar.setTag(pos);
        holder.com.setTag(pos);


        RATINGS[pos] = "";
        COMMENTS[pos] = "";
        PRODUCT_IDS[pos] = Integer.parseInt(map.get(PRODUCT_ID));
        PRODUCT_IDSS[pos] = map.get(PRODUCT_ID);
        PRODUCT_VARIANT_IDS[pos] = map.get(PRODUCT_VARIANT_ID);

        Log.e("Comm", TextUtils.join(",",PRODUCT_VARIANT_IDS));
        if(map.get(REVIEW_DONE2).equals("N") && map.get(STATUS).equals("OD")){
            holder.simpleRatingBar.setVisibility(View.VISIBLE);
            holder.com.setVisibility(View.VISIBLE);
        }else {
            Log.e("POS", map.get(RATE)+"     "+map.get(COMMENT) );
            if(map.get(RATE).equals("0")){
                holder.simpleRatingBar.setVisibility(View.GONE);
            }else {
                holder.simpleRatingBar.setRating(Float.parseFloat(map.get(RATE)));
                holder.simpleRatingBar.setClickable(false);
                holder.simpleRatingBar.setScrollable(false);
                holder.simpleRatingBar.setVisibility(View.VISIBLE);
            }


            if(!map.get(COMMENT).equals("null") && !map.get(COMMENT).equals(null) && !map.get(COMMENT).equals("")){
                holder.com.setVisibility(View.VISIBLE);
                holder.com.setText(map.get(COMMENT));
                holder.com.setFocusable(false);
            }else {
                holder.com.setVisibility(View.GONE);
            }
        }
        holder.qty_tv.setText(activity.getString(R.string.qty_with_colon)+" "+map.get(QTY));
        holder.disc_price_tv.setText(activity.getString(R.string.total_discont)+" : "+CURRENCY+" "+map.get(DISCOUNT_PRICE));

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
       private ImageView pro_pic;
       private TextView product_title_tv,variant_tv,qty_tv,unit_price_tv,total_price_tv,disc_price_tv;
        EditText com;
        ScaleRatingBar simpleRatingBar;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            pro_pic=itemView.findViewById(R.id.pro_pic);
            product_title_tv=itemView.findViewById(R.id.product_title_tv);
            variant_tv=itemView.findViewById(R.id.variant_tv);
            qty_tv=itemView.findViewById(R.id.qty_tv);
            unit_price_tv=itemView.findViewById(R.id.unit_price_tv);
            total_price_tv=itemView.findViewById(R.id.total_price_tv);
            disc_price_tv=itemView.findViewById(R.id.disc_price_tv);

            com = itemView.findViewById(R.id.commnet);
            simpleRatingBar = itemView.findViewById(R.id.simpleRatingBar);

            simpleRatingBar.setOnRatingChangeListener(new BaseRatingBar.OnRatingChangeListener() {
                @Override
                public void onRatingChange(BaseRatingBar ratingBar, float rating, boolean fromUser) {
                    int position = (int) ratingBar.getTag();
                    RATINGS[position] = String.valueOf(rating);
                    Log.e("Comm", TextUtils.join(",",RATINGS));
                }
            });

            com.addTextChangedListener(new TextWatcher() {

                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    int comPos = (int) com.getTag();
                    COMMENTS[comPos] = com.getText().toString().trim();
                }



                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    int comPos = (int) com.getTag();
                    COMMENTS[comPos] = com.getText().toString().trim();
                }

                public void afterTextChanged(Editable s) {
                    int comPos = (int) com.getTag();
                    COMMENTS[comPos] = com.getText().toString().trim();
                    Log.e("Comm", TextUtils.join(",",COMMENTS));
                }
            });

        }
    }
}


