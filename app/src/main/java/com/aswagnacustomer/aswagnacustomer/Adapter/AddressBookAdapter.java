package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Activity.AddAddressActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.AddressBookActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.AddressUpdateActivity;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ADDRESS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.EMAIL_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IS_DEFAULT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PHONE_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;


public class AddressBookAdapter extends RecyclerView.Adapter<AddressBookAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public AddressBookAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.address_book_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        if (map.get(IS_DEFAULT).equals("Y")){
            holder.default_img.setVisibility(View.VISIBLE);
            holder.address_title_tv.setText(activity.getResources().getString(R.string.default_shipping_address));
            holder.more_option_img.setVisibility(View.GONE);
            holder.set_default_img.setVisibility(View.GONE);
            holder.edit_img.setVisibility(View.VISIBLE);
            holder.remove_img.setVisibility(View.GONE);
        }else {
            holder.default_img.setVisibility(View.GONE);
            holder.address_title_tv.setText(activity.getResources().getString(R.string.address_information)+" "+position);

            holder.more_option_img.setVisibility(View.VISIBLE);
            holder.set_default_img.setVisibility(View.GONE);
            holder.edit_img.setVisibility(View.GONE);
            holder.remove_img.setVisibility(View.GONE);

            holder.more_option_img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.more_option_img.setVisibility(View.GONE);
                    holder.set_default_img.setVisibility(View.VISIBLE);
                    holder.edit_img.setVisibility(View.VISIBLE);
                    holder.remove_img.setVisibility(View.VISIBLE);
                }
            });

            holder.main_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.more_option_img.setVisibility(View.VISIBLE);
                    holder.set_default_img.setVisibility(View.GONE);
                    holder.edit_img.setVisibility(View.GONE);
                    holder.remove_img.setVisibility(View.GONE);
                }
            });
        }
        holder.full_name_tv.setText(map.get(NAME));
        holder.address_tv.setText(map.get(ADDRESS));
        holder.mobile_tv.setText(map.get(PHONE_NO));
        holder.email_tv.setText(map.get(EMAIL_ID));
        holder.edit_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(activity, AddressUpdateActivity.class);
                intent.putExtra("id",map.get(ID));
                activity.startActivity(intent);
            }
        });

        holder.set_default_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDefaultAddress(map.get(ID));
            }
        });
        holder.remove_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAddress(map.get(ID));
            }
        });


    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView address_title_tv,full_name_tv,address_tv,mobile_tv,email_tv;
        private LinearLayout main_lay;
        private ImageView default_img,more_option_img,set_default_img,edit_img,remove_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            main_lay = itemView.findViewById(R.id.main_lay);
            default_img = itemView.findViewById(R.id.default_img);
            address_title_tv = itemView.findViewById(R.id.address_title_tv);
            more_option_img = itemView.findViewById(R.id.more_option_img);
            set_default_img = itemView.findViewById(R.id.set_default_img);
            edit_img = itemView.findViewById(R.id.edit_img);
            remove_img = itemView.findViewById(R.id.remove_img);
            full_name_tv = itemView.findViewById(R.id.full_name_tv);
            address_tv = itemView.findViewById(R.id.address_tv);
            mobile_tv = itemView.findViewById(R.id.mobile_tv);
            email_tv = itemView.findViewById(R.id.email_tv);

        }
    }

    public void removeAddress(String id) {
        if (!isNetworkConnected(activity)) {
            MethodClass.network_error_alert(activity);
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_url = activity.getString(R.string.SERVER_URL) + "delete-address-book";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    ((AddressBookActivity)activity).address_book();
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(activity, response);
                    SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE);
                    sweetAlertDialog.setTitleText(resultResponse.getJSONObject("success").getString("message"));
                    sweetAlertDialog.setContentText(resultResponse.getJSONObject("success").getString("meaning"));
                    sweetAlertDialog.setConfirmText(activity.getResources().getString(R.string.ok));
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    });
                    sweetAlertDialog.show();
                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(activity));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }
    public void setDefaultAddress(String id) {
        if (!isNetworkConnected(activity)) {
            MethodClass.network_error_alert(activity);
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_url = activity.getString(R.string.SERVER_URL) + "set-default-address-book";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    ((AddressBookActivity)activity).address_book();
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(activity, response);
                    SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE);
                    sweetAlertDialog.setTitleText(resultResponse.getJSONObject("success").getString("message"));
                    sweetAlertDialog.setContentText(resultResponse.getJSONObject("success").getString("meaning"));
                    sweetAlertDialog.setConfirmText(activity.getResources().getString(R.string.ok));
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    });
                    sweetAlertDialog.show();
                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(activity));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }

}


