package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.OrderDetailsActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import java.net.IDN;
import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_ITEM;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_TOTAL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_TYPE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.STATUS;


public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public OrderHistoryAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.order_histori_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.view_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, OrderDetailsActivity.class);
                intent.putExtra("id",map.get(ID));
                activity.startActivity(intent);
            }
        });
        holder.order_no_tv.setText(map.get(ORDER_NO));
        holder.order_date_tv.setText(map.get(CREATED_AT));
        holder.item_tv.setText(map.get(ORDER_ITEM));
        holder.total_price_tv.setText(map.get(ORDER_TOTAL));

        if (map.get(PAYMENT_METHODE).equals("O")){
            holder.payment_method.setText("Online");
        }else {
            holder.payment_method.setText("Cash on delivery");
        }
        Log.e("STAT", map.get(STATUS)+"    "+map.get(ORDER_TYPE) );

        if (map.get(STATUS).equals("N")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.new_order));
        } else if (map.get(STATUS).equals("OA")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.accepted));
        } else if (map.get(STATUS).equals("DA")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.assigned));
        } else if (map.get(STATUS).equals("RP")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.ready_to_pick_up));
        } else if (map.get(STATUS).equals("OP")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.picked_up));
        } else if (map.get(STATUS).equals("OD")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.delivered));
        } else if (map.get(STATUS).equals("OC")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.cancelled));
        } else if (map.get(STATUS).equals("I")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.incomplete));
        }else if (map.get(STATUS).equals("F")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.payment_failed));
        }else if (map.get(STATUS).equals("PP")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.driver_assign));
        }else if (map.get(STATUS).equals("PC")) {
            holder.status_tv.setText(activity.getResources().getString(R.string.driver_assign));
        }
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView order_no_tv,status_tv,order_date_tv,item_tv,total_price_tv,payment_method,view_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            order_no_tv = itemView.findViewById(R.id.order_no_tv);
            status_tv = itemView.findViewById(R.id.status_tv);
            order_date_tv = itemView.findViewById(R.id.order_date_tv);
            item_tv = itemView.findViewById(R.id.item_tv);
            total_price_tv = itemView.findViewById(R.id.total_price_tv);
            payment_method = itemView.findViewById(R.id.payment_method);
            view_tv = itemView.findViewById(R.id.view_tv);
        }
    }
}


