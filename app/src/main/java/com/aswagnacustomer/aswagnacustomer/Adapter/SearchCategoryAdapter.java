package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.SearchActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SUB_CAT_ARRLIST;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;

public class SearchCategoryAdapter extends RecyclerView.Adapter<SearchCategoryAdapter.ViewHolder> {
    private Activity activity;
    private ArrayList<HashMap<String, Object>> arrayList;
    private int selectedItem=1000;
    private boolean isFirst=true;

    public SearchCategoryAdapter(Activity activity, ArrayList<HashMap<String, Object>> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
    }


    @Override
    public int getItemCount() {
        Log.e("ZISS", String.valueOf(arrayList.size()));
        return arrayList == null ? 0 : arrayList.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_category, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String, Object> map = arrayList.get(position);
        Log.e("SIZE", String.valueOf(map.get(TITLE)) );
        holder.category_textView.setText(String.valueOf(map.get(TITLE)));
        if (isFirst){
            isFirst=false;
            SearchChildCategoryAdapter childCategoryAdapter = new SearchChildCategoryAdapter(activity, (ArrayList<HashMap<String, String>>) map.get(SUB_CAT_ARRLIST));
            ((SearchActivity) activity).child_category_recyclerView.setAdapter(childCategoryAdapter);
        }

        Log.e("SLUG", ((SearchActivity) activity).cat_slug+"     "+map.get(SLUG));
         if (((SearchActivity) activity).cat_slug.equals(map.get(SLUG))){

            ((SearchActivity) activity).cat_slug=((String) map.get(SLUG));
            ((SearchActivity) activity).cat_name=((String) map.get(TITLE));
           /* ((SearchActivity) activity).sub_cat_slug="";
            ((SearchActivity) activity).sub_cat_name="";*/
             ((SearchActivity) activity).child_category_recyclerView.setAdapter(null);
            SearchChildCategoryAdapter childCategoryAdapter = new SearchChildCategoryAdapter(activity, (ArrayList<HashMap<String, String>>) map.get(SUB_CAT_ARRLIST));
            ((SearchActivity) activity).child_category_recyclerView.setAdapter(childCategoryAdapter);
             selectedItem=position;
             Log.e("SLIGGG", String.valueOf(selectedItem) );
         }

        holder.raw_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((SearchActivity) activity).child_category_recyclerView.setAdapter(null);
                int previousItem = selectedItem;
                selectedItem = position;
                notifyItemChanged(previousItem);
                notifyItemChanged(position);
                SearchChildCategoryAdapter childCategoryAdapter = new SearchChildCategoryAdapter(activity, (ArrayList<HashMap<String, String>>) map.get(SUB_CAT_ARRLIST));
                ((SearchActivity) activity).child_category_recyclerView.setAdapter(childCategoryAdapter);
                ((SearchActivity) activity).cat_name=((String) map.get(TITLE));
                ((SearchActivity) activity).sub_cat_name="";
                ((SearchActivity) activity).cat_slug=((String) map.get(SLUG));
                ((SearchActivity) activity).sub_cat_slug="";
                ((SearchActivity) activity).variant="";
                ((SearchActivity) activity).finalvarientJonarry=new JSONArray();
                ((SearchActivity) activity).brand="";
                ((SearchActivity) activity).price="";
                ((SearchActivity) activity).order_by="";
                ((SearchActivity) activity).product="";
                SearchActivity.MIN_PRIZE="";
                SearchActivity.MAX_PRIZE="";
                ((SearchActivity) activity).searchList();
            }
        });

        if (selectedItem == position) {
            holder.category_textView.setTextColor(activity.getResources().getColor(R.color.red));
            holder.view.setBackgroundColor(activity.getResources().getColor(R.color.red));
        } else {
            holder.category_textView.setTextColor(activity.getResources().getColor(R.color.black));
            holder.view.setBackgroundColor(activity.getResources().getColor(R.color.textGreyColor));
        }

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView category_textView;
        private LinearLayout raw_layout;
        private View view;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            category_textView = (TextView) itemView.findViewById(R.id.category_textView);
            raw_layout = (LinearLayout) itemView.findViewById(R.id.raw_layout);
            view = (View) itemView.findViewById(R.id.view);
        }
    }

   /* private void changeColor(TextView category_textView,View view){

    }*/
}
