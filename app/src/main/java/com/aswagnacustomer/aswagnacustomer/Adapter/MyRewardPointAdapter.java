package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_AMOUNT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_RECEIVED;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_USED;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;


public class MyRewardPointAdapter extends RecyclerView.Adapter<MyRewardPointAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public MyRewardPointAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.my_reward_point_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.order_no_tv.setText(map.get(ORDER_NO));
        holder.balance_tv.setText(map.get(LOYALTY_AMOUNT));
        holder.point_reci_tv.setText(map.get(LOYALTY_RECEIVED));
        holder.point_reedm_tv.setText(map.get(LOYALTY_USED));
        holder.order_date_tv.setText(map.get(CREATED_AT));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView order_no_tv,balance_tv,point_reci_tv,point_reedm_tv,order_date_tv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            order_no_tv=itemView.findViewById(R.id.order_no_tv);
            balance_tv=itemView.findViewById(R.id.balance_tv);
            point_reci_tv=itemView.findViewById(R.id.point_reci_tv);
            point_reedm_tv=itemView.findViewById(R.id.point_reedm_tv);
            order_date_tv=itemView.findViewById(R.id.order_date_tv);
        }
    }
}


