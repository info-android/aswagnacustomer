package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.FilterActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.FilterSubActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;


public class FiltervariantAdapter extends RecyclerView.Adapter<FiltervariantAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public FiltervariantAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.filter_vary_lay_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.title_name.setText(map.get(TITLE));

        try {
            for (int i = 0; i < FilterActivity.variJsonArray.length(); i++) {
                if (map.get(ID).equals(FilterActivity.variJsonArray.getJSONObject(i).getString("variant_id"))) {
                    String data = FilterActivity.variJsonArray.getJSONObject(i).getString("value_id");
                    String[] listdata = TextUtils.split(data, ",");
                   Log.e("DAat", String.valueOf(listdata.length));
                   Log.e("DAatttt", String.valueOf(listdata));
                    holder.sub_var.setText(listdata.length+" item selected");
                   /* if (data.trim().length()==1){
                        holder.sub_var.setText(data.length()+" item selected");
                    }else {
                        holder.sub_var.setText((data.length()-((data.length()-1)/2))+" item selected");
                    }*/
                }
            }
        } catch (JSONException e) {

        }





        holder.list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((FilterActivity)activity).varientId=map.get(ID);
                Intent intent=new Intent(activity, FilterSubActivity.class);
                intent.putExtra("array",map.get(VARY_VALUE));
                intent.putExtra("filter_by",map.get(TITLE));
                intent.putExtra("type","var");
                activity.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title_name,sub_var;
        private LinearLayout list_item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title_name=itemView.findViewById(R.id.title_name);
            sub_var=itemView.findViewById(R.id.sub_var);
            list_item=itemView.findViewById(R.id.list_item);
        }
    }
}


