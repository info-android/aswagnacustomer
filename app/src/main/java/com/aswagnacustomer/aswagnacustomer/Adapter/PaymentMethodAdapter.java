package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aswagnacustomer.aswagnacustomer.Activity.PayNowActivity;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.AMOUNT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CURR;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE_AR;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TOTAL_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TRANSACTION_NO;

public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    private Integer row_index = -1;
    public PaymentMethodAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.paymeth_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);

        Picasso.get().load(map.get(IMAGE)).placeholder(R.drawable.applogo).error(R.drawable.applogo).into(holder.pro_pic);
        if(MethodClass.check_locale_lang(activity).equals("en")){
            holder.product_title_tv.setText(map.get(PAYMENT_METHODE));
            Log.e("TITLE", map.get(PAYMENT_METHODE));
        }else {
            holder.product_title_tv.setText(map.get(PAYMENT_METHODE_AR));
        }

        holder.main_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row_index=position;
                ((PayNowActivity)activity).setParam(Double.parseDouble(map.get(TOTAL_PRICE)),map.get(ID),map.get(CURR));
                notifyDataSetChanged();
            }
        });
        if(row_index==position){
            holder.main_lay.setBackground(activity.getResources().getDrawable(R.drawable.blue_button_background));
        }else {
            holder.main_lay.setBackground(activity.getResources().getDrawable(R.drawable.grey_back_strock));
        }

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView product_title_tv;
        private ImageView pro_pic;
        private LinearLayout main_lay;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            product_title_tv = itemView.findViewById(R.id.product_title_tv);
            pro_pic = itemView.findViewById(R.id.pro_pic);
            main_lay = itemView.findViewById(R.id.main_lay);
        }
    }
}
