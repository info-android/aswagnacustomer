package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.FilterActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.FilterSubActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;


public class FilterSubBrandsAdapter extends RecyclerView.Adapter<FilterSubBrandsAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public FilterSubBrandsAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
        FilterActivity.brandArrayID.clear();
        FilterSubActivity.variantArrayID.clear();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.filter_sub_brand_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.checkbox_title.setText(map.get(TITLE));
        holder.checkbox_title.setTag(map.get(ID));
        holder.checkbox_title.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    for (int i = 0; i < FilterActivity.brandArrayID.size(); i++) {
                        if (FilterActivity.brandArrayID.get(i).equals((String) holder.checkbox_title.getTag())) {
                            FilterActivity.brandArrayID.remove(i);
                        }
                    }
                    FilterActivity.brandArrayID.add((String) holder.checkbox_title.getTag());
                } else {
                    for (int i = 0; i < FilterActivity.brandArrayID.size(); i++) {
                        if (FilterActivity.brandArrayID.get(i).equals((String) holder.checkbox_title.getTag())) {
                            FilterActivity.brandArrayID.remove(i);
                        }
                    }
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CheckBox checkbox_title;
        private LinearLayout list_item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            checkbox_title=itemView.findViewById(R.id.checkbox_title);
            list_item=itemView.findViewById(R.id.list_item);
        }
    }
}


