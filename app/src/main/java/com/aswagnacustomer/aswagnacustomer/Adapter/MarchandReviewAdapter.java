package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COMMENT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FNAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LNAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PROFILE_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.RATING;


public class MarchandReviewAdapter extends RecyclerView.Adapter<MarchandReviewAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public MarchandReviewAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.seller_review_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        Picasso.get().load(PROFILE_IMAGE_URL+map.get(IMAGE)).error(R.drawable.icon74).placeholder(R.drawable.icon74).into(holder.user_img);
        holder.user_name_tv.setText(map.get(FNAME)+" "+map.get(LNAME));
        holder.comment_tv.setText(map.get(COMMENT));
        holder.rating.setRating(Float.parseFloat(map.get(RATING)));
        holder.date_tv.setText(map.get(map.get(DATE)));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView user_name_tv,date_tv,comment_tv;
        private RatingBar rating;
        private CircleImageView user_img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            user_img=itemView.findViewById(R.id.user_img);
            user_name_tv=itemView.findViewById(R.id.user_name_tv);
            date_tv=itemView.findViewById(R.id.date_tv);
            comment_tv=itemView.findViewById(R.id.comment_tv);
            rating=itemView.findViewById(R.id.rating);

        }
    }
}


