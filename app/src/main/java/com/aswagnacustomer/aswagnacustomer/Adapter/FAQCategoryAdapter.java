package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.FAQActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.OrderDetailsActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FAQ_DATA;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_ITEM;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_TOTAL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;


public class FAQCategoryAdapter extends RecyclerView.Adapter<FAQCategoryAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public FAQCategoryAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.faq_cat_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.title_name.setText(map.get(TITLE));
        holder.list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity, FAQActivity.class);
                intent.putExtra("data",map.get(FAQ_DATA));
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title_name;
        private LinearLayout list_item;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            title_name=itemView.findViewById(R.id.title_name);
            list_item=itemView.findViewById(R.id.list_item);
        }
    }
}


