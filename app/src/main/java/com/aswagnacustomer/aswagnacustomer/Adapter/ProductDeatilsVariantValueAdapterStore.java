package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ProductDeatilsVariantValueAdapterStore extends RecyclerView.Adapter<ProductDeatilsVariantValueAdapterStore.ViewHolder> {

    private Activity activity;
    private ArrayList<HashMap<String, String>> arrayList;
    private int selectedItem = 1000;
    private boolean isMatch = true;

    public ProductDeatilsVariantValueAdapterStore(Activity activity, ArrayList<HashMap<String, String>> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
        isMatch=true;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_details_size_item1, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
       /* final HashMap<String, String> hashMap = arrayList.get(position);
        holder.subCategoryName.setText(hashMap.get(NAME));

        if (isMatch){
            for (int i = 0; i < ProductDetailsActivity.SELECTED_COMMBINATION.size(); i++) {
                String id = ProductDetailsActivity.SELECTED_COMMBINATION.get(i);
                if (id.equals(hashMap.get(ID))) {
                    isMatch=false;
                    selectedItem = position;
                    for (int j = 0; j < ProductDetailsActivity.productVariantValueArrayID.size(); j++) {
                        String addId = ProductDetailsActivity.productVariantValueArrayID.get(j);
                        if (hashMap.get(ID).equals(addId)) {
                            ProductDetailsActivity.productVariantValueArrayID.remove(j);
                        }
                    }
                    ProductDetailsActivity.productVariantValueArrayID.add(hashMap.get(ID));
                    Log.e("VariantValueArrayID1",ProductDetailsActivity.productVariantValueArrayID.toString());
                }
            }
        }

        holder.view_layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProductDetailsActivity.productVariantValueArrayID.clear();
                Log.e("chek111",ProductDetailsActivity.productVariantValueArrayID.toString());
                int previousItem = selectedItem;
                selectedItem = position;
                notifyItemChanged(previousItem);
                notifyItemChanged(position);
                for (int i = 0; i < arrayList.size(); i++) {
                    String selected = arrayList.get(i).get(ID);
                    for (int j = 0; j < ProductDetailsActivity.productVariantValueArrayID.size(); j++) {
                        String id = ProductDetailsActivity.productVariantValueArrayID.get(j);
                        if (selected.equals(id)) {
                            ProductDetailsActivity.productVariantValueArrayID.remove(j);
                        }
                    }
                }
                ProductDetailsActivity.productVariantValueArrayID.add(hashMap.get(ID));

                for (int k = 0; k < ProductDetailsActivity.COMBINATION_ARRAY.size(); k++) {
                    ArrayList<String> cList = ProductDetailsActivity.COMBINATION_ARRAY.get(k);
                    if (MethodClass.equalLists(ProductDetailsActivity.productVariantValueArrayID, cList)) {
                        Log.e("True", cList.toString() + " = " + ProductDetailsActivity.productVariantValueArrayID.toString());
                    } else {
                        Log.e("false", cList.toString() + " = " + ProductDetailsActivity.productVariantValueArrayID.toString());
                        if (ProductDetailsActivity.COMBINATION_ARRAY.size() == (k+1)) {
                            for (int j = 0; j < ProductDetailsActivity.COMBINATION_ARRAY.size(); j++) {
                                ArrayList<String> firstCommbinationArray = ProductDetailsActivity.COMBINATION_ARRAY.get(j);
                                for (int i = 0; i < firstCommbinationArray.size(); i++) {
                                    String id = firstCommbinationArray.get(i);
                                    if (id.equals(hashMap.get(ID))) {
                                        ProductDetailsActivity.SELECTED_COMMBINATION = firstCommbinationArray;
                                        ((ProductDetailsActivity) activity).updateApdater1();
                                        Log.e("Call1","111");
                                        return;
                                    }
                                }
                            }

                        }
                    }
                }
            }
        });



        if (selectedItem == position) {
            holder.view_layout1.setBackgroundResource(R.drawable.circle_shape_red);
            holder.subCategoryName.setTextColor(activity.getResources().getColor(R.color.white));
        } else {
            holder.view_layout1.setBackgroundResource(R.drawable.circle_shape);
            holder.subCategoryName.setTextColor(activity.getResources().getColor(R.color.black));
        }*/
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView subCategoryName;
        private LinearLayout view_layout1;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            subCategoryName = (TextView) itemView.findViewById(R.id.subCategoryName);
            view_layout1 = (LinearLayout) itemView.findViewById(R.id.view_layout1);
        }
    }
}






  /* if (((ProductDetailsActivity) activity).iscallActivity) {
            ((ProductDetailsActivity) activity).iscallActivity = false;
            for (int j = 0; j < ProductDetailsActivity.COMBINATION_ARRAY.size(); j++) {
                ArrayList<String> firstCommbinationArray = ProductDetailsActivity.COMBINATION_ARRAY.get(j);
                for (int i = 0; i < firstCommbinationArray.size(); i++) {
                    String id = firstCommbinationArray.get(i);
                    Log.e("ifFirst1", id + "=" + hashMap.get(ID));
                    if (id.equals(hashMap.get(ID))) {
                        selectedItem = position;
                        ProductDetailsActivity.SELECTED_COMMBINATION = firstCommbinationArray;
                        Log.e("MAtchFirst1", String.valueOf(position));
                        Log.e("MAtchFirst1", ProductDetailsActivity.SELECTED_COMMBINATION.toString());
                        break;
                    }
                }
            }
        }*/
