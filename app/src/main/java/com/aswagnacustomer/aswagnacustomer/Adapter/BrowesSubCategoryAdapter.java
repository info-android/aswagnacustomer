package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.SearchActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;

public class BrowesSubCategoryAdapter extends RecyclerView.Adapter<BrowesSubCategoryAdapter.ViewHolder> {

    private Activity activity;
    private ArrayList<HashMap<String ,String >> childCategoryModelArrayList;
    private int selectedItem;
    private boolean isFirst=true;
    private Object cat_slug="";

    public BrowesSubCategoryAdapter(Activity activity, ArrayList<HashMap<String,String>> childCategoryModelArrayList,Object cat_slug) {
        this.activity = activity;
        this.childCategoryModelArrayList = childCategoryModelArrayList;
        this.cat_slug = cat_slug;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.browse_sub_cat_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map=childCategoryModelArrayList.get(position);
        holder.subCategoryName.setText(map.get(TITLE));
        holder.view_layout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                final String slug= (String) map.get(SLUG);
                Intent intent=new Intent(activity, SearchActivity.class);
                intent.putExtra("cat_slug",String.valueOf(cat_slug));
                intent.putExtra("sub_cat_slug",slug);
                intent.putExtra("brand","0");
                intent.putExtra("variant","0");
                intent.putExtra("other_variant","0");
                intent.putExtra("price","0");
                intent.putExtra("order_by","0");
                intent.putExtra("product","");
                activity.startActivity(intent);
            }

        });
    }

    @Override
    public int getItemCount() {
        return childCategoryModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView subCategoryName;
        private LinearLayout view_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            subCategoryName = (TextView) itemView.findViewById(R.id.subCategoryName);
            view_layout = (LinearLayout) itemView.findViewById(R.id.view_layout1);
        }
    }
}
