package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.SearchActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;

public class SearchChildCategoryAdapter extends RecyclerView.Adapter<SearchChildCategoryAdapter.ViewHolder> {

    private Activity activity;
    private ArrayList<HashMap<String ,String >> childCategoryModelArrayList;
    private int selectedItem=1000;
    private boolean isFirst=true;

    public SearchChildCategoryAdapter(Activity activity, ArrayList<HashMap<String,String>> childCategoryModelArrayList) {
        this.activity = activity;
        this.childCategoryModelArrayList = childCategoryModelArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_child_category, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String,String> map=childCategoryModelArrayList.get(position);
        holder.subCategoryName.setText(map.get(TITLE));
        if (((SearchActivity) activity).sub_cat_slug.equals(map.get(SLUG))){
            selectedItem=position;
            ((SearchActivity) activity).sub_cat_name=map.get(TITLE);
        }
        holder.view_layout.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                int previousItem = selectedItem;
                selectedItem = position;
                notifyItemChanged(previousItem);
                notifyItemChanged(position);
                ((SearchActivity) activity).brand="";
                ((SearchActivity) activity).variant="";
                ((SearchActivity) activity).price="";
                ((SearchActivity) activity).order_by="";
                ((SearchActivity) activity).sub_cat_slug=((String) map.get(SLUG));
                ((SearchActivity) activity).sub_cat_name=((String) map.get(TITLE));
                SearchActivity.MIN_PRIZE="";
                SearchActivity.MAX_PRIZE="";
                ((SearchActivity) activity).searchList();
            }

        });

        if (selectedItem == position) {
            holder.view_layout.setBackgroundResource(R.drawable.circle_shape_red);
            holder.subCategoryName.setTextColor(activity.getResources().getColor(R.color.white));
        }else {
            holder.view_layout.setBackgroundResource(R.drawable.circle_shape);
            holder.subCategoryName.setTextColor(activity.getResources().getColor(R.color.black));
        }



    }

    @Override
    public int getItemCount() {
        return childCategoryModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView subCategoryName;
        private LinearLayout view_layout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            subCategoryName = (TextView) itemView.findViewById(R.id.subCategoryName);
            view_layout = (LinearLayout) itemView.findViewById(R.id.view_layout1);
        }
    }
}
