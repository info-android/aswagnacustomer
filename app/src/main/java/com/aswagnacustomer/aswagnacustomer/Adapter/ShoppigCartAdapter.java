package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;
import com.google.gson.JsonArray;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CART_MASTER_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IN_STOCK;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCTS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_VARIANT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.QTY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SElLER_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.STCOK_QUAN;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TOTAL_PRICE;


public class ShoppigCartAdapter extends RecyclerView.Adapter<ShoppigCartAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public ShoppigCartAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.shopping_cart_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        topList(map.get(PRODUCTS),holder.recy_view);
        holder.sold_by_tv.setText(map.get(NAME));
        //Picasso.get().load(SElLER_IMAGE_URL+map.get(IMAGE)).placeholder(R.drawable.icon74).error(R.drawable.icon74).into(holder.selImg);
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView recy_view;
        private TextView sold_by_tv;
        private ImageView selImg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recy_view=itemView.findViewById(R.id.recy_view);
            sold_by_tv=itemView.findViewById(R.id.sold_by_tv);
            selImg=itemView.findViewById(R.id.selImg);
        }
    }
    private void topList(String productStr,RecyclerView recyclerView){
        try{
            JSONArray jsonArray =new JSONArray(productStr);
            ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String,String>  hashMap=new HashMap<>();
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                hashMap.put(ID,jsonObject.getString("id"));
                hashMap.put(PRODUCT_ID,jsonObject.getString("product_id"));
                hashMap.put(CART_MASTER_ID,jsonObject.getString("cart_master_id"));
                hashMap.put(PRODUCT_VARIANT_ID,jsonObject.getString("product_variant_id"));
                if(!jsonObject.getString("product_variant_id").equals("null")){
                    String stock_quantity = jsonObject.getJSONObject("product_variant_details").getString("stock_quantity");
                    if(Integer.parseInt(stock_quantity)>=Integer.parseInt(jsonObject.getString("quantity"))){
                        hashMap.put(IN_STOCK,"Y");
                        hashMap.put(STCOK_QUAN,stock_quantity);
                    }else {
                        hashMap.put(IN_STOCK,"N");
                        hashMap.put(STCOK_QUAN,stock_quantity);
                    }
                }else {
                    String stock_quantity = jsonObject.getJSONObject("get_product").getString("stock");
                    if(Integer.parseInt(stock_quantity)>=Integer.parseInt(jsonObject.getString("quantity"))){
                        hashMap.put(IN_STOCK,"Y");
                        hashMap.put(STCOK_QUAN,stock_quantity);
                    }else {
                        hashMap.put(IN_STOCK,"N");
                        hashMap.put(STCOK_QUAN,stock_quantity);
                    }
                }
                hashMap.put(DISCOUNT_PRICE,jsonObject.getString("subtotal"));
                hashMap.put(TOTAL_PRICE,jsonObject.getString("total"));
                hashMap.put(QTY,jsonObject.getString("quantity"));
                hashMap.put(TITLE,jsonObject.getJSONObject("product_by_language").getString("title"));
                if (!jsonObject.getString("default_image").equals("null")){
                    hashMap.put(IMAGE,jsonObject.getJSONObject("default_image").getString("image"));
                }else {
                    hashMap.put(IMAGE,"null");
                }

                arrayList.add(hashMap);
            }
            ShoppingCartSubAdapter shoppingCartSubAdapter =new ShoppingCartSubAdapter(activity,arrayList);
            recyclerView.setAdapter(shoppingCartSubAdapter);
            recyclerView.setFocusable(false);
        }catch (JSONException e){
            Log.e("JSONException",e.toString());
        }

    }
}


