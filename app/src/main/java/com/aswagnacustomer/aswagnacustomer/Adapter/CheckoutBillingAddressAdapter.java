package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.CheckOutActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ADDRESS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.EMAIL_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PHONE_NO;


public class CheckoutBillingAddressAdapter extends RecyclerView.Adapter<CheckoutBillingAddressAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;
    private int selectedItem=1000;

    public CheckoutBillingAddressAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.checkout_address_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.full_name_tv.setText(map.get(NAME));
        holder.address_tv.setText(map.get(ADDRESS));
        holder.mobile_tv.setText(map.get(PHONE_NO));
        holder.email_tv.setText(map.get(EMAIL_ID));

        holder.main_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CheckOutActivity)activity).bil_sel_add_id=map.get(ID);
                int previousItem = selectedItem;
                selectedItem = position;
                notifyItemChanged(previousItem);
                notifyItemChanged(selectedItem);
            }
        });

        if (selectedItem==position){
            holder.main_lay.setCardBackgroundColor(activity.getResources().getColor(R.color.light_green));
        }else {
            holder.main_lay.setCardBackgroundColor(activity.getResources().getColor(R.color.white));
        }

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView full_name_tv, address_tv, mobile_tv, email_tv;
        private CardView main_lay;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            main_lay = itemView.findViewById(R.id.main_lay);
            full_name_tv = itemView.findViewById(R.id.full_name_tv);
            address_tv = itemView.findViewById(R.id.address_tv);
            mobile_tv = itemView.findViewById(R.id.mobile_tv);
            email_tv = itemView.findViewById(R.id.email_tv);
        }
    }
}


