package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Activity.ShoppingCartActivity;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CART_MASTER_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CHECK;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IN_STOCK;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_VARIANT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.QTY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.STCOK_QUAN;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TOTAL_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;


public class ShoppingCartSubAdapter extends RecyclerView.Adapter<ShoppingCartSubAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public ShoppingCartSubAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.shopping_sub_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        Picasso.get().load(PRODUCT_IMAGE_URL + map.get(IMAGE)).placeholder(R.drawable.applogo).error(R.drawable.applogo).into(holder.pro_pic);
        holder.product_title_tv.setText(map.get(TITLE));
        holder.newPrice_tv.setText(map.get(TOTAL_PRICE));
        holder.oldPrice_tv.setText(map.get(DISCOUNT_PRICE));
        holder.count_tv.setText(map.get(QTY));
        holder.minus_count_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qtycheck = Integer.parseInt(holder.count_tv.getText().toString().trim());
                if (qtycheck>1){
                    int qty = Integer.parseInt(holder.count_tv.getText().toString().trim()) - 1;
                    update_cart(holder.count_tv, String.valueOf(qty), map.get(ID), map.get(CART_MASTER_ID), map.get(PRODUCT_ID), map.get(PRODUCT_VARIANT_ID));
                }
            }
        });
        holder.plus_count_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int qty = Integer.parseInt(holder.count_tv.getText().toString().trim()) + 1;
                update_cart(holder.count_tv, String.valueOf(qty), map.get(ID), map.get(CART_MASTER_ID), map.get(PRODUCT_ID), map.get(PRODUCT_VARIANT_ID));
            }
        });

        holder.delete_prod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete_cart(map.get(ID));
            }
        });

        if(map.get(IN_STOCK).equals("Y")){
            holder.stock.setVisibility(View.GONE);
        }else {
            if(!map.get(STCOK_QUAN).equals("0")){
                holder.stock.setText("Only "+map.get(STCOK_QUAN)+" item(s) available of this product");
            }
            holder.stock.setVisibility(View.VISIBLE);
            CHECK = false;
        }

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView recy_view;
        private ImageView pro_pic, delete_prod;
        private TextView plus_count_tv, count_tv, minus_count_tv, product_title_tv, newPrice_tv, oldPrice_tv,stock;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recy_view = itemView.findViewById(R.id.recy_view);
            pro_pic = itemView.findViewById(R.id.pro_pic);
            delete_prod = itemView.findViewById(R.id.delete_prod);
            plus_count_tv = itemView.findViewById(R.id.plus_count_tv);
            count_tv = itemView.findViewById(R.id.count_tv);
            minus_count_tv = itemView.findViewById(R.id.minus_count_tv);
            product_title_tv = itemView.findViewById(R.id.product_title_tv);
            newPrice_tv = itemView.findViewById(R.id.newPrice_tv);
            oldPrice_tv = itemView.findViewById(R.id.oldPrice_tv);
            stock = itemView.findViewById(R.id.stock);
        }
    }

    public void update_cart(final TextView count_tv, final String quantity, String cart_details_id, String cart_master_id, String product_id, String product_variant_id) {
        String android_id = Settings.Secure.getString(activity.getContentResolver(), Settings.Secure.ANDROID_ID);
        if (!isNetworkConnected(activity)) {
            MethodClass.network_error_alert(activity);
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_url = activity.getString(R.string.SERVER_URL) + "update-cart";
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("quantity", quantity);
        params.put("cart_details_id", cart_details_id);
        params.put("cart_master_id", cart_master_id);
        params.put("product_id", product_id);
        if(!product_variant_id.equals("null") && !product_variant_id.equals(null)){
            params.put("product_variant_id", product_variant_id);
        }else {
            params.put("product_variant_id", "");
        }

        if (!PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in", false)) {
            params.put("device_id", android_id);
        }
        JSONObject jsonObject = MethodClass.Json_rpc_format_obj(params);
        Log.e("update_cartREQUEST", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("updateCartRES", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(activity, response);
                    if (resultResponse != null) {
                        String getQty=resultResponse.getJSONObject("cart_details").getString("quantity");
                        count_tv.setText(getQty);
                        JSONObject message = resultResponse.getJSONObject("message");

                        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message.getString("message"))
                                .setContentText(message.getString("meaning"))
                                .setConfirmText(activity.getResources().getString(R.string.done))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        ((ShoppingCartActivity)activity).get_cart();
                                    }
                                }).show();
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(activity));
                if (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }


    public void delete_cart(String cart_details_id) {
        if (!isNetworkConnected(activity)) {
            MethodClass.network_error_alert(activity);
            return;
        }
        MethodClass.showProgressDialog(activity);
        Log.e("CART_ID", cart_details_id );
        String server_url = activity.getString(R.string.SERVER_URL) + "remove-cart/"+cart_details_id;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("RemoveCartRES", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(activity, response);
                    if (resultResponse != null) {

                        JSONObject message = resultResponse.getJSONObject("message");
                        new SweetAlertDialog(activity, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message.getString("message"))
                                .setContentText(message.getString("meaning"))
                                .setConfirmText(activity.getResources().getString(R.string.done))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        ((ShoppingCartActivity)activity).get_cart();
                                    }
                                }).show();
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(activity));
                if (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }


}


