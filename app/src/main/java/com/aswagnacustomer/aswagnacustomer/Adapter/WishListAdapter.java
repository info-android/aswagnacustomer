package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Activity.AddressBookActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.ProductDetailsActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.WishListActivity;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FROM_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TO_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;


public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public WishListAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.wish_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ProductDetailsActivity.class);
                intent.putExtra("id", map.get(ID));
                activity.startActivity(intent);
            }
        });

        float price = Float.parseFloat(map.get(PRICE));
        float discount_price = Float.parseFloat(map.get(DISCOUNT_PRICE));
        String from_date = map.get(FROM_DATE);
        String to_date = map.get(TO_DATE);

        if (discount_price == 0) {
            holder.newPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", price));
            holder.oldPrice_tv.setVisibility(View.GONE);
            holder.offer_lay.setVisibility(View.GONE);
        } else {
            if (from_date.equals("null") || from_date.equals(null) || from_date.equals("")) {
                holder.newPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", price));
                holder.oldPrice_tv.setVisibility(View.GONE);
                holder.offer_lay.setVisibility(View.GONE);
            } else {
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date fromDate = dateFormat.parse(from_date);
                    Date toDate = dateFormat.parse(to_date);
                    Date dateC = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    String formattedDate = df.format(dateC);
                    Date currentDate = dateFormat.parse(formattedDate);
                    if ((currentDate.getTime() >= fromDate.getTime()) && (currentDate.getTime() <= toDate.getTime())) {
                        holder.newPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", discount_price));
                        holder.oldPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", price));
                        float finalPrice = Math.round(((price - discount_price) / price) * 100);
                        holder.prctg_offer_tv.setText(finalPrice + activity.getString(R.string.per_off));
                        holder.oldPrice_tv.setVisibility(View.VISIBLE);
                        holder.offer_lay.setVisibility(View.VISIBLE);
                    } else {
                        holder.newPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", price));
                        holder.oldPrice_tv.setVisibility(View.GONE);
                        holder.offer_lay.setVisibility(View.GONE);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        holder.product_title_tv.setText(map.get(TITLE));
        Picasso.get().load(PRODUCT_IMAGE_URL + map.get(IMAGE)).error(R.drawable.ic_reload).placeholder(R.drawable.ic_reload).into(holder.prod_image);

        holder.delete_prod_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeWishList(activity,map.get(ID));
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout list_item, offer_lay;
        private TextView prctg_offer_tv, product_title_tv, newPrice_tv, oldPrice_tv;
        private ImageView prod_image, delete_prod_img;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            list_item = itemView.findViewById(R.id.list_item);
            prctg_offer_tv = itemView.findViewById(R.id.prctg_offer_tv);
            prod_image = itemView.findViewById(R.id.prod_image);
            product_title_tv = itemView.findViewById(R.id.product_title_tv);
            newPrice_tv = itemView.findViewById(R.id.newPrice_tv);
            oldPrice_tv = itemView.findViewById(R.id.oldPrice_tv);
            delete_prod_img = itemView.findViewById(R.id.delete_prod_img);
            offer_lay = itemView.findViewById(R.id.offer_lay);
        }


    }
    private void removeWishList(Activity activity, String id) {
        if (!isNetworkConnected(activity)) {
            MethodClass.network_error_alert(activity);
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_url = activity.getString(R.string.SERVER_URL) + "add-fav/"+id;
        Log.e("server_url", server_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(activity, response);
                    if (resultResponse != null) {
                        ((WishListActivity)activity).myWishList();
                        Toast.makeText(activity, resultResponse.getJSONObject("message").getString("meaning"), Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(activity));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }
}


