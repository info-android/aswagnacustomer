package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;


public class ProductDetailsVariantAdapter extends RecyclerView.Adapter<ProductDetailsVariantAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public ProductDetailsVariantAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.product_details_variant_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.variant_tv.setText((String) map.get(NAME));
        try {
            JSONArray jsonArray=new JSONArray(map.get(VARY_VALUE));
            ArrayList<HashMap<String,String>> arrayList =new ArrayList<>();
            for (int j = 0; j < jsonArray.length(); j++) {
                String variant_value_id=jsonArray.getJSONObject(j).getString("variant_value_id");
                String variant_value_name=jsonArray.getJSONObject(j).getJSONObject("variant_value_by_language").getString("name");
                HashMap<String,String> hashMap=new HashMap<>();
                hashMap.put(ID,variant_value_id);
                hashMap.put(NAME,variant_value_name);
                arrayList.add(hashMap);
            }
            ProductDeatilsVariantValueAdapter1 valueAdapter = new ProductDeatilsVariantValueAdapter1(activity,arrayList);
            holder.vari_valu_recyc.setAdapter(valueAdapter);
            holder.vari_valu_recyc.setFocusable(false);
        }catch (JSONException e){

        }



    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView variant_tv;
        private RecyclerView vari_valu_recyc;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            variant_tv=itemView.findViewById(R.id.variant_tv);
            vari_valu_recyc=itemView.findViewById(R.id.vari_valu_recyc);

               }
    }
}


