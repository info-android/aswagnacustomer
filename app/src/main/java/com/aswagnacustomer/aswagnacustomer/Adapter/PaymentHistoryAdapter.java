package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.AMOUNT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_TOTAL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TRANSACTION_NO;


public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public PaymentHistoryAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.payment_history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.order_no_tv.setText(map.get(ORDER_NO));
        holder.order_date_tv.setText(map.get(CREATED_AT));
        holder.amount_tv.setText(map.get(AMOUNT));
        if (map.get(PAYMENT_METHODE).equals("O")){
            holder.payment_method.setText("Online");
        }else {
            holder.payment_method.setText("Cash on delivery");
        }
        holder.txt_no_tv.setText(map.get(TRANSACTION_NO));
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView order_no_tv, order_date_tv, amount_tv, payment_method,txt_no_tv;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            order_no_tv = itemView.findViewById(R.id.order_no_tv);
            order_date_tv = itemView.findViewById(R.id.order_date_tv);
            amount_tv = itemView.findViewById(R.id.amount_tv);
            payment_method = itemView.findViewById(R.id.payment_method);
            txt_no_tv = itemView.findViewById(R.id.txt_no_tv);
        }
    }
}


