package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.hdodenhof.circleimageview.CircleImageView;

import com.aswagnacustomer.aswagnacustomer.Activity.ProductDetailsActivity;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COLOR;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARIANT1;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARIANT2;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARIANT3;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARIANT_IMAGE;

public class ProductDeatilsVariantValueAdapter1 extends RecyclerView.Adapter<ProductDeatilsVariantValueAdapter1.ViewHolder> {

    private Activity activity;
    private ArrayList<HashMap<String, String>> arrayList;
    private int selectedItem = 1000;
    private boolean isMatch = true;

    public ProductDeatilsVariantValueAdapter1(Activity activity, ArrayList<HashMap<String, String>> arrayList) {
        this.activity = activity;
        this.arrayList = arrayList;
        isMatch=true;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_details_size_item1, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final HashMap<String, String> hashMap = arrayList.get(position);
        if(hashMap.get(COLOR).equals("Y")){
            holder.subCategoryName.setVisibility(View.GONE);
            holder.img_View.setVisibility(View.VISIBLE);
            Picasso.get().load(VARIANT_IMAGE+hashMap.get(IMAGE)).placeholder(R.drawable.white_corner_border).error(R.drawable.white_corner_border).into(holder.img_View);
        }else {
            holder.subCategoryName.setText(hashMap.get(NAME));
            holder.subCategoryName.setVisibility(View.VISIBLE);
            holder.img_View.setVisibility(View.GONE);
        }


        if (isMatch){
            for (int i = 0; i < ProductDetailsActivity.SELECTED_COMMBINATION.size(); i++) {
                String id = ProductDetailsActivity.SELECTED_COMMBINATION.get(i);
                Log.e("ID", id+ "                         "+hashMap.get(ID) );
                Log.e("Cal11", id );
                if (id.equals(hashMap.get(ID))) {

                    isMatch=false;
                    selectedItem = position;
                    for (int j = 0; j < ProductDetailsActivity.productVariantValueArrayID.size(); j++) {
                        String addId = ProductDetailsActivity.productVariantValueArrayID.get(j);
                        if (hashMap.get(ID).equals(addId)) {
                            ProductDetailsActivity.productVariantValueArrayID.remove(j);
                        }
                    }
                    ProductDetailsActivity.productVariantValueArrayID.add(hashMap.get(ID));
                    Log.e("VariantValueArrayID1",ProductDetailsActivity.productVariantValueArrayID.toString());
                    //((ProductDetailsActivity) activity).updateApdater1();

                }
            }


        }

        holder.view_layout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ProductDetailsActivity.productVariantValueArrayID.clear();
                Log.e("chek111",ProductDetailsActivity.productVariantValueArrayID.toString());
                int previousItem = selectedItem;
                selectedItem = position;
                notifyItemChanged(previousItem);
                notifyItemChanged(position);
                for (int i = 0; i < arrayList.size(); i++) {
                    String selected = arrayList.get(i).get(ID);
                    for (int j = 0; j < ProductDetailsActivity.productVariantValueArrayID.size(); j++) {
                        String id = ProductDetailsActivity.productVariantValueArrayID.get(j);
                        if (selected.equals(id)) {
                            ProductDetailsActivity.productVariantValueArrayID.remove(j);
                        }
                    }
                }

                ProductDetailsActivity.productVariantValueArrayID.add(hashMap.get(ID));
                if(!VARIANT2.equals("")){
                    ProductDetailsActivity.productVariantValueArrayID.add(VARIANT2);
                }if(!VARIANT3.equals("")){
                    ProductDetailsActivity.productVariantValueArrayID.add(VARIANT3);
                }

                for (int k = 0; k < ProductDetailsActivity.COMBINATION_ARRAY.size(); k++) {
                    ArrayList<String> cList = ProductDetailsActivity.COMBINATION_ARRAY.get(k);
                    if (MethodClass.equalLists(ProductDetailsActivity.productVariantValueArrayID, cList)) {
                        Log.e("True", cList.toString() + " = " + ProductDetailsActivity.productVariantValueArrayID.toString());
                    } else {
                        Log.e("false", cList.toString() + " = " + ProductDetailsActivity.productVariantValueArrayID.toString());
                        Log.e("SIZE", String.valueOf(ProductDetailsActivity.COMBINATION_ARRAY.size()));


                        for (int j = 0; j < ProductDetailsActivity.COMBINATION_ARRAY.size(); j++) {
                            Log.e("false", cList.toString() + " = " + ProductDetailsActivity.productVariantValueArrayID.toString());

                            ArrayList<String> firstCommbinationArray = ProductDetailsActivity.COMBINATION_ARRAY.get(j);
                            for (int i = 0; i < firstCommbinationArray.size(); i++) {
                                String id = firstCommbinationArray.get(i);
                                if (firstCommbinationArray.equals(ProductDetailsActivity.productVariantValueArrayID)) {
                                    Log.e("false", cList.toString() + " = " + ProductDetailsActivity.productVariantValueArrayID.toString());

                                    ProductDetailsActivity.SELECTED_COMMBINATION = firstCommbinationArray;

                                    ((ProductDetailsActivity) activity).updateApdater1();
                                    Log.e("Call1",ProductDetailsActivity.SELECTED_COMMBINATION.toString());
                                    return;
                                }else if(!ProductDetailsActivity.COMBINATION_ARRAY.contains(ProductDetailsActivity.productVariantValueArrayID)) {
                                    if(id.equals(hashMap.get(ID))){
                                        ProductDetailsActivity.productVariantValueArrayID.clear();
                                        ProductDetailsActivity.productVariantValueArrayID.add(hashMap.get(ID));
                                        ProductDetailsActivity.SELECTED_COMMBINATION = firstCommbinationArray;

                                        ((ProductDetailsActivity) activity).updateApdater1();
                                        Log.e("Call1",ProductDetailsActivity.SELECTED_COMMBINATION.toString());
                                        return;
                                    }
                                }
                            }
                        }

                    }
                }
            }
        });



        if (selectedItem == position) {
            VARIANT1 = hashMap.get(ID);
            ((ProductDetailsActivity)activity).scrollToPOs1(position);
            holder.view_layout1.setBackgroundResource(R.drawable.circle_shape_red);
            holder.subCategoryName.setTextColor(activity.getResources().getColor(R.color.white));
        } else {
            holder.view_layout1.setBackgroundResource(R.drawable.circle_shape);
            holder.subCategoryName.setTextColor(activity.getResources().getColor(R.color.black));
        }
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView subCategoryName;
        private LinearLayout view_layout1;
        private CircleImageView img_View;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            subCategoryName = (TextView) itemView.findViewById(R.id.subCategoryName);
            view_layout1 = (LinearLayout) itemView.findViewById(R.id.view_layout1);
            img_View = (CircleImageView) itemView.findViewById(R.id.img_View);
        }
    }
}






  /* if (((ProductDetailsActivity) activity).iscallActivity) {
            ((ProductDetailsActivity) activity).iscallActivity = false;
            for (int j = 0; j < ProductDetailsActivity.COMBINATION_ARRAY.size(); j++) {
                ArrayList<String> firstCommbinationArray = ProductDetailsActivity.COMBINATION_ARRAY.get(j);
                for (int i = 0; i < firstCommbinationArray.size(); i++) {
                    String id = firstCommbinationArray.get(i);
                    Log.e("ifFirst1", id + "=" + hashMap.get(ID));
                    if (id.equals(hashMap.get(ID))) {
                        selectedItem = position;
                        ProductDetailsActivity.SELECTED_COMMBINATION = firstCommbinationArray;
                        Log.e("MAtchFirst1", String.valueOf(position));
                        Log.e("MAtchFirst1", ProductDetailsActivity.SELECTED_COMMBINATION.toString());
                        break;
                    }
                }
            }
        }*/
