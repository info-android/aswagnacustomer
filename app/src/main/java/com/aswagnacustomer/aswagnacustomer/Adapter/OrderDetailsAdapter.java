package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COMMENT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_DETAILS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_VARIANT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.QTY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.RATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.REVIEW_DONE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.REVIEW_DONE2;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SELLER_NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SElLER_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.STATUS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TOTAL_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.UNIT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;


public class OrderDetailsAdapter extends RecyclerView.Adapter<OrderDetailsAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public OrderDetailsAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.order_detals_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        Picasso.get().load(SElLER_IMAGE_URL+map.get(IMAGE)).placeholder(R.drawable.icon74).error(R.drawable.icon74).into(holder.selImg);
        topList(map.get(ORDER_DETAILS),holder.recy_view,map.get(REVIEW_DONE),map.get(STATUS));
        holder.sold_by_tv.setText(map.get(SELLER_NAME));

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView recy_view;
        private TextView sold_by_tv;
        private ImageView selImg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recy_view=itemView.findViewById(R.id.recy_view);
            sold_by_tv=itemView.findViewById(R.id.sold_by_tv);
            selImg=itemView.findViewById(R.id.selImg);
        }
    }
    private void topList(String order_details,RecyclerView recyclerView,String reviewDOne,String statu){
        try {
            JSONArray jsonArray=new JSONArray(order_details);
            ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object=jsonArray.getJSONObject(i);
                HashMap<String,String>  hashMap=new HashMap<>();
                hashMap.put(TITLE,object.getJSONObject("product_details").getJSONObject("product_by_language").getString("title"));
                hashMap.put(VARY_VALUE,object.getString("variants"));
                hashMap.put(QTY,object.getString("quantity"));
                hashMap.put(UNIT_PRICE,object.getString("original_price"));
                hashMap.put(PRODUCT_ID,object.getString("product_id"));
                hashMap.put(PRODUCT_VARIANT_ID,object.getString("product_variant_id"));
                hashMap.put(TOTAL_PRICE,String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(object.getString("total"))));
                if(!object.getString("discounted_price").equals("0.000")){
                    Double original  = Double.parseDouble(object.getString("original_price"));
                    Double disc  = Double.parseDouble(object.getString("discounted_price"));
                    double tot_pri = (original - disc)*Integer.parseInt(object.getString("quantity"));
                    hashMap.put(DISCOUNT_PRICE,String.format(Locale.ENGLISH,"%.3f", tot_pri));
                }else {
                    hashMap.put(DISCOUNT_PRICE,"0.000");
                }

                hashMap.put(RATE,object.getString("rate"));
                hashMap.put(COMMENT,object.getString("comment"));
                if (!object.getJSONObject("product_details").getString("default_image").equals("null")){
                    hashMap.put(IMAGE,object.getJSONObject("product_details").getJSONObject("default_image").getString("image"));
                }else {
                    hashMap.put(IMAGE,"null");
                }
                hashMap.put(REVIEW_DONE2,reviewDOne);
                hashMap.put(STATUS,statu);
                arrayList.add(hashMap);
            }
            OrderDetailsSubAdapter orderDetailsSubAdapter=new OrderDetailsSubAdapter(activity,arrayList);
            recyclerView.setAdapter(orderDetailsSubAdapter);
            recyclerView.setFocusable(false);
        }catch (JSONException e){
            e.printStackTrace();
            Log.e("JSONException",e.toString());
        }
    }
}


