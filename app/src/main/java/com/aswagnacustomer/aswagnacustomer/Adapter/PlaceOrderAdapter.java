package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_DETAILS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.QTY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SELLER_NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SElLER_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TOTAL_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.UNIT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;


public class PlaceOrderAdapter extends RecyclerView.Adapter<PlaceOrderAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public PlaceOrderAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.place_order_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.seller_name.setText(map.get(SELLER_NAME));
        //Picasso.get().load(SElLER_IMAGE_URL+map.get(IMAGE)).placeholder(R.drawable.icon74).error(R.drawable.icon74).into(holder.selImg);
        topList(map.get(ORDER_DETAILS),holder.recy_view);

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private RecyclerView recy_view;
        private TextView seller_name;
        private ImageView selImg;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            recy_view=itemView.findViewById(R.id.recy_view);
            seller_name=itemView.findViewById(R.id.seller_name);
            selImg=itemView.findViewById(R.id.selImg);
        }
    }
    private void topList(String order_details,RecyclerView recyclerView){
        try {
            JSONArray jsonArray=new JSONArray(order_details);
            ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object=jsonArray.getJSONObject(i);
                HashMap<String,String>  hashMap=new HashMap<>();
                hashMap.put(TITLE,object.getJSONObject("product_details").getJSONObject("product_by_language").getString("title"));
                hashMap.put(VARY_VALUE,object.getString("variants"));
                hashMap.put(QTY,object.getString("quantity"));
                hashMap.put(UNIT_PRICE,object.getString("original_price"));
                hashMap.put(TOTAL_PRICE,object.getString("total"));
                if (!object.getJSONObject("product_details").getString("default_image").equals("null")){
                    hashMap.put(IMAGE,object.getJSONObject("product_details").getJSONObject("default_image").getString("image"));
                }else {
                    hashMap.put(IMAGE,"null");
                }
                arrayList.add(hashMap);
            }
            PlaceOrderSubAdapter  placeOrderSubAdapter=new PlaceOrderSubAdapter(activity,arrayList);
            recyclerView.setAdapter(placeOrderSubAdapter);
            recyclerView.setFocusable(false);
        }catch (JSONException e){
            e.printStackTrace();
            Log.e("JSONException",e.toString());
        }
    }
}


