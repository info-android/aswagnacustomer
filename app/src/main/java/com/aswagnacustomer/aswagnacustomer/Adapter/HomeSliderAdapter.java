package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aswagnacustomer.aswagnacustomer.R;
import com.smarteist.autoimageslider.SliderViewAdapter;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.BANNER_DESC;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.BANNER_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.BUTTON_CAP;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.HEADING;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SUB_HEADING;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.URLS;

public class HomeSliderAdapter extends SliderViewAdapter<HomeSliderAdapter.SliderAdapterVH> {

    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public HomeSliderAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_slider_layout, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, final int position) {
        final HashMap<String, String> map = map_list.get(position);
        Picasso.get().load(BANNER_IMAGE_URL+map.get(IMAGE)).fit().placeholder(R.drawable.applogo).error(R.drawable.applogo).into(viewHolder.imageViewBackground);

        if(!map.get(HEADING).equals("null") && !map.get(HEADING).equals(null) && !map.get(HEADING).equals("")){
            viewHolder.text2.setText(map.get(HEADING));
        }else{
            viewHolder.text2.setText("");
        }
        if(!map.get(SUB_HEADING).equals("null") && !map.get(SUB_HEADING).equals(null) && !map.get(SUB_HEADING).equals("")){
            viewHolder.text3.setText(map.get(SUB_HEADING));
        }else{
            viewHolder.text3.setText("");
        }
        if(!map.get(BANNER_DESC).equals("null") && !map.get(BANNER_DESC).equals(null) && !map.get(BANNER_DESC).equals("")){
            viewHolder.text4.setText(map.get(BANNER_DESC));
        }else{
            viewHolder.text4.setText("");
        }


        if(!map.get(BUTTON_CAP).equals("null") && !map.get(BUTTON_CAP).equals(null) && !map.get(BUTTON_CAP).equals("")){
            viewHolder.btn.setText(map.get(BUTTON_CAP));
            viewHolder.btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Uri uri = Uri.parse(map.get(URLS)); // missing 'http://' will cause crashed
                    Uri webpage = Uri.parse(map.get(URLS));

                    if (!map.get(URLS).startsWith("http://") && !map.get(URLS).startsWith("https://")) {
                        webpage = Uri.parse("http://" + map.get(URLS));
                    }

                    Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                    if (intent.resolveActivity(activity.getPackageManager()) != null) {
                        activity.startActivity(intent);
                    }
                }
            });
            viewHolder.btn.setVisibility(View.VISIBLE);
        }else{
            viewHolder.btn.setVisibility(View.GONE);
            viewHolder.main_lay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(!map.get(URLS).equals("0") && !map.get(URLS).equals("") && !map.get(URLS).equals("null") && !map.get(URLS).equals("-")){
                        Uri webpage = Uri.parse(map.get(URLS));

                        if (!map.get(URLS).startsWith("http://") && !map.get(URLS).startsWith("https://")) {
                            webpage = Uri.parse("http://" + map.get(URLS));
                        }

                        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
                        if (intent.resolveActivity(activity.getPackageManager()) != null) {
                            activity.startActivity(intent);
                        }
                    }

                }
            });

        }

    }

    @Override
    public int getCount() {
        return map_list.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {
        ImageView imageViewBackground;
        TextView text2,text3,text4;
        Button btn;
        RelativeLayout main_lay;
        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.image);
            text2 = itemView.findViewById(R.id.text2);
            text3 = itemView.findViewById(R.id.text3);
            text4 = itemView.findViewById(R.id.text4);
            btn = itemView.findViewById(R.id.btn);
            main_lay = itemView.findViewById(R.id.main_lay);
        }
    }
}
