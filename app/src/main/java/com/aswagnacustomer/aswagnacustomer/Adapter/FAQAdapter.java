package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.aswagnacustomer.aswagnacustomer.R;
import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ANS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.QUESTION;


public class FAQAdapter extends RecyclerView.Adapter<FAQAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public FAQAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.faq_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.que_tv.setText(map.get(QUESTION));
        holder.ans_tv.setText(map.get(ANS));
        holder.head_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.ans_tv.getVisibility()==View.VISIBLE){
                    holder.ans_tv.setVisibility(View.GONE);
                }else {
                    holder.ans_tv.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public void startAnimations(TextView textView) {
        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.slide_in_bottom);
        anim.reset();
        textView.clearAnimation();
        textView.startAnimation(anim);

    }
    public void endAnimations(TextView textView) {
        Animation anim = AnimationUtils.loadAnimation(activity, R.anim.slide_out_bottom);
        anim.reset();
        textView.clearAnimation();
        textView.startAnimation(anim);

    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView que_tv,ans_tv;
        private LinearLayout head_lin;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            que_tv = (TextView) itemView.findViewById(R.id.que_tv);
            ans_tv = (TextView) itemView.findViewById(R.id.ans_tv);
            head_lin = (LinearLayout) itemView.findViewById(R.id.head_lin);

        }
    }
}


