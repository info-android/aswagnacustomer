package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.ProductDetailsActivity;
import com.aswagnacustomer.aswagnacustomer.Helper.WishListAddRemoveClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FROM_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TO_DATE;


public class HomeNewProdAdapter extends RecyclerView.Adapter<HomeNewProdAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, String>> map_list;

    public HomeNewProdAdapter(Activity activity, ArrayList<HashMap<String, String>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.home_new_prod_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, String> map = map_list.get(position);
        holder.list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ProductDetailsActivity.class);
                intent.putExtra("id", map.get(ID));
                activity.startActivity(intent);
            }
        });

        float price = Float.parseFloat(map.get(PRICE));
        float discount_price = Float.parseFloat(map.get(DISCOUNT_PRICE));
        String from_date = map.get(FROM_DATE);
        String to_date = map.get(TO_DATE);
        if (discount_price == 0) {
            holder.newPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", price));
            holder.oldPrice_tv.setVisibility(View.GONE);
            holder.offer_lay.setVisibility(View.GONE);
        } else {
            if (from_date.equals("null") || from_date.equals(null) || from_date.equals("")) {
                holder.newPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", price));
                holder.oldPrice_tv.setVisibility(View.GONE);
                holder.offer_lay.setVisibility(View.GONE);
            } else {
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date fromDate = dateFormat.parse(from_date);
                    Date toDate = dateFormat.parse(to_date);
                    Date dCurrentDate = dateFormat.parse(dateFormat.format(new Date()));
                    Log.e("TIME", String.valueOf(System.currentTimeMillis())+"      "+String.valueOf(fromDate.getTime()) );
                    if ((dCurrentDate.equals(fromDate) || dCurrentDate.after(fromDate)) && (dCurrentDate.before(toDate) || dCurrentDate.equals(toDate))) {

                        holder.newPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", discount_price));
                        holder.oldPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", price));
                        float finalPrice = Math.round(((price - discount_price) / price) * 100);
                        holder.prctg_offer_tv.setText(finalPrice + activity.getString(R.string.per_off));
                        holder.oldPrice_tv.setVisibility(View.VISIBLE);
                        holder.offer_lay.setVisibility(View.VISIBLE);
                    } else {
                        holder.newPrice_tv.setText(activity.getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", price));
                        holder.oldPrice_tv.setVisibility(View.GONE);
                        holder.offer_lay.setVisibility(View.GONE);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
        holder.like_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in", false)) {
                    WishListAddRemoveClass.addwishList(activity,map.get(ID));
                } else {
                    Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), activity.getResources().getString(R.string.please_login_first), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        });



        holder.product_title_tv.setText(map.get(TITLE));
        Picasso.get().load(PRODUCT_IMAGE_URL + map.get(IMAGE)).error(R.drawable.applogo).placeholder(R.drawable.applogo).into(holder.prod_image);
    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout list_item, offer_lay;
        private TextView prctg_offer_tv, product_title_tv, newPrice_tv, oldPrice_tv;
        private ImageView prod_image, like_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            list_item = itemView.findViewById(R.id.list_item);
            prctg_offer_tv = itemView.findViewById(R.id.prctg_offer_tv);
            prod_image = itemView.findViewById(R.id.prod_image);
            product_title_tv = itemView.findViewById(R.id.product_title_tv);
            newPrice_tv = itemView.findViewById(R.id.newPrice_tv);
            oldPrice_tv = itemView.findViewById(R.id.oldPrice_tv);
            like_img = itemView.findViewById(R.id.like_img);
            offer_lay = itemView.findViewById(R.id.offer_lay);
        }
    }
}


