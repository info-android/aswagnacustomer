package com.aswagnacustomer.aswagnacustomer.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aswagnacustomer.aswagnacustomer.Activity.SearchActivity;
import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SUB_CAT_ARRLIST;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;


public class BrowseCatAdapter extends RecyclerView.Adapter<BrowseCatAdapter.ViewHolder> {
    Activity activity;
    public ArrayList<HashMap<String, Object>> map_list;

    public BrowseCatAdapter(Activity activity, ArrayList<HashMap<String, Object>> map_list) {
        this.activity = activity;
        this.map_list = map_list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.browes_cat_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final HashMap<String, Object> map = map_list.get(position);
        holder.cat_title.setText((String) map.get(TITLE));

        BrowesSubCategoryAdapter childCategoryAdapter = new BrowesSubCategoryAdapter(activity, (ArrayList<HashMap<String, String>>) map.get(SUB_CAT_ARRLIST),map.get(SLUG));
        holder.child_category_recyclerView.setAdapter(childCategoryAdapter);
        holder.child_category_recyclerView.setFocusable(false);


        holder.list_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String slug= (String) map.get(SLUG);
                Intent intent=new Intent(activity, SearchActivity.class);
                intent.putExtra("cat_slug",slug);
                intent.putExtra("sub_cat_slug","0");
                intent.putExtra("brand","0");
                intent.putExtra("variant","0");
                intent.putExtra("other_variant","0");
                intent.putExtra("price","0");
                intent.putExtra("order_by","0");
                intent.putExtra("product","");
                activity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return map_list == null ? 0 : map_list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView cat_title;
        private LinearLayout list_item;
        private RecyclerView child_category_recyclerView;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            cat_title=itemView.findViewById(R.id.cat_title);
            list_item=itemView.findViewById(R.id.list_item);
            child_category_recyclerView=itemView.findViewById(R.id.child_category_recyclerView);
        }
    }
}


