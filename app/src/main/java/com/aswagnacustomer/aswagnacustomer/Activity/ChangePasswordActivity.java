package com.aswagnacustomer.aswagnacustomer.Activity;

import androidx.appcompat.app.AppCompatActivity;
import io.fabric.sdk.android.Fabric;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.error_alert;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.showProgressDialog;

public class ChangePasswordActivity extends AppCompatActivity {
    private EditText password_et, confirm_pass;
    private ImageView pass_hide_show_img, cof_pass_hide_show_img;
    String email = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());//here initialize Facebook Sdk
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_change_password);
        password_et = findViewById(R.id.password_et);
        confirm_pass = findViewById(R.id.confirm_pass);
        cof_pass_hide_show_img = findViewById(R.id.cof_pass_hide_show_img);
        pass_hide_show_img = findViewById(R.id.pass_hide_show_img);

        email = getIntent().getStringExtra("email");

        pass_hide_show_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password_et.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    pass_hide_show_img.setImageDrawable(getResources().getDrawable(R.drawable.icon6));
                    password_et.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    password_et.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    pass_hide_show_img.setImageDrawable(getResources().getDrawable(R.drawable.view));
                    password_et.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    password_et.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });
        cof_pass_hide_show_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirm_pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    cof_pass_hide_show_img.setImageDrawable(getResources().getDrawable(R.drawable.icon6));
                    confirm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    confirm_pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    cof_pass_hide_show_img.setImageDrawable(getResources().getDrawable(R.drawable.view));
                    confirm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    confirm_pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });
    }

    public void confirm(View view) {
        String password = "", conf_password = "";
        password = password_et.getText().toString().trim();
        conf_password = confirm_pass.getText().toString().trim();

        if (password.length() == 0) {
            password_et.setError(getResources().getString(R.string.please_enter_password));
            password_et.requestFocus();
            return;
        }
        if (password.length() < 6) {
            password_et.setError(getResources().getString(R.string.please_enter_minimum_six_password));
            password_et.requestFocus();
            return;
        }
        if (conf_password.length() == 0) {
            confirm_pass.setError(getResources().getString(R.string.please_enter_your_confirm_password));
            confirm_pass.requestFocus();
            return;
        }
        if (!password.equals(conf_password)) {
            confirm_pass.setError(getResources().getString(R.string.password_is_not_matched_with_confirm_password));
            confirm_pass.requestFocus();
            return;
        }

        if (!isNetworkConnected(ChangePasswordActivity.this)) {
            MethodClass.network_error_alert(ChangePasswordActivity.this);
            return;
        }

        showProgressDialog(ChangePasswordActivity.this);
        String url = getString(R.string.SERVER_URL) + "update-pass-user";
        Log.e("url", url);
        HashMap<String, String> hashMap = new HashMap<>();//create mapping model class to send data of server
        hashMap.put("email", email);
        hashMap.put("password", password);
        Log.e("reset", MethodClass.Json_rpc_format(hashMap).toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, MethodClass.Json_rpc_format(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("response", response.toString());
                MethodClass.hideProgressDialog(ChangePasswordActivity.this);
                final JSONObject resultResponce = MethodClass.get_result_from_webservice(ChangePasswordActivity.this, response);
                try {
                    if (resultResponce != null) {
                        Log.e("resultResponce", resultResponce.toString());
                        new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(resultResponce.getString("message"))
                                .setContentText(resultResponce.getString("meaning"))
                                .setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                Intent intent = new Intent(ChangePasswordActivity.this, DashboardActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }).show();

                    }
                } catch (Exception e) {
                    Log.e("ExceptionElse", e.toString());
                    error_alert(ChangePasswordActivity.this);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onErrorResponse: ", error.toString());
                MethodClass.hideProgressDialog(ChangePasswordActivity.this);
                if (error.toString().contains("ConnectException") || error.toString().contains("NoConnectionError")) {
                    MethodClass.network_error_alert(ChangePasswordActivity.this);
                } else {
                    Log.e("ErrorElse", " Else");
                    error_alert(ChangePasswordActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(ChangePasswordActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ChangePasswordActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ChangePasswordActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}