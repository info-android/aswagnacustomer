package com.aswagnacustomer.aswagnacustomer.Activity;
/*Date a, b;   // assume these are set to something
        Date d;      // the date in question
        return a.compareTo(d) * d.compareTo(b) > 0;*/

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.HomeBestDealsAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.HomeCatAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.HomeNewProdAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.HomeSliderAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.HomeTopSellAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.Helper.WishListAddRemoveClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.jama.carouselview.CarouselView;
import com.jama.carouselview.CarouselViewListener;
import com.jama.carouselview.enums.IndicatorAnimationType;
import com.jama.carouselview.enums.OffsetType;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.BANNER_DESC;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.BUTTON_CAP;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FROM_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.HEADING;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SUB_CAT_ARRLIST;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SUB_HEADING;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TO_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.URLS;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class HomeActivity extends AppCompatActivity {
    private RecyclerView recy_view_category, recy_view_image, recy_view_new_prod, recy_view_best_prod, recy_view_top_sell;
    private CarouselView carouselView;
    private SliderView imageSlider;
    private AutoCompleteTextView search_auto_text;
    private ImageView cancel_search_img, search_icon_img;
    private LinearLayout toolBarLay, search_lin;
    private TextView seeAll_tv;
    public static ArrayList<MethodClass.StringWithTag> tagArrayListId_1;
    public static ArrayList<MethodClass.StringWithTag> tagArrayListId_2;
    public static ArrayList<HashMap<String, Object>> catArrayListID_1;
    public static ArrayList<HashMap<String, Object>> catArrayListID_2;
    private TextView announcement_tv;
    private RelativeLayout cart_count_lay;
    private TextView cart_tv;
    private boolean isFirst = false;
    private NestedScrollView scrollView;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        MethodClass.set_locale(this);
        setContentView(R.layout.activity_home);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        tagArrayListId_1 = new ArrayList<>();
        tagArrayListId_2 = new ArrayList<>();
        catArrayListID_1 = new ArrayList<>();
        catArrayListID_2 = new ArrayList<>();
        recy_view_category = findViewById(R.id.recy_view_category);
        recy_view_new_prod = findViewById(R.id.recy_view_new_prod);
        recy_view_best_prod = findViewById(R.id.recy_view_best_prod);
        recy_view_top_sell = findViewById(R.id.recy_view_top_sell);
        carouselView = findViewById(R.id.carouselView);
        imageSlider = findViewById(R.id.imageSlider);
        search_auto_text = findViewById(R.id.search_auto_text);
        cancel_search_img = findViewById(R.id.cancel_search_img);
        search_icon_img = findViewById(R.id.search_icon_img);
        toolBarLay = findViewById(R.id.toolBarLay);
        search_lin = findViewById(R.id.search_lin);
        cart_count_lay = findViewById(R.id.cart_count_lay);
        cart_tv = findViewById(R.id.cart_tv);
        scrollView = findViewById(R.id.scrollView);
        announcement_tv = findViewById(R.id.announcement_tv);
        search_icon_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolBarLay.setVisibility(View.GONE);
                search_lin.setVisibility(View.VISIBLE);
            }
        });
        cancel_search_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolBarLay.setVisibility(View.VISIBLE);
                search_auto_text.setText("");
                search_lin.setVisibility(View.GONE);
            }
        });
        seeAll_tv = findViewById(R.id.seeAll_tv);
        cart_count_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ShoppingCartActivity.class);
                startActivity(intent);
            }
        });
        isFirst = false;
        homeMethod();

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, HomeActivity.class.getSimpleName());
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Android");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    private void homeMethod() {
        isFirst = false;
        if (!isNetworkConnected(HomeActivity.this)) {
            MethodClass.network_error_alert(HomeActivity.this);
            return;
        }
        MethodClass.showProgressDialog(HomeActivity.this);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String server_url = "";
        if (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getBoolean("is_logged_in", false)) {
            server_url = getString(R.string.SERVER_URL) + "home";
        } else {
            server_url = getString(R.string.SERVER_URL) + "home/" + android_id;
        }
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(HomeActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(HomeActivity.this, response);
                    if (resultResponse != null) {
                        if (resultResponse.has("cart_item") && !resultResponse.getString("cart_item").equals("null") && !resultResponse.getString("cart_item").equals(null)) {
                            JSONObject cart_item = resultResponse.getJSONObject("cart_item");
                            int total_item = cart_item.getInt("total_item");
                            if (total_item > 0) {
                                cart_tv.setText(String.valueOf(total_item));
                                cart_tv.setVisibility(View.VISIBLE);

                            } else {
                                cart_tv.setVisibility(View.GONE);
                            }
                        } else {
                            cart_tv.setVisibility(View.GONE);
                        }
                        if (resultResponse.has("annoucement") && !resultResponse.getString("annoucement").equals("null") && !resultResponse.getString("annoucement").equals(null)) {
                            JSONObject annoucement_item = resultResponse.getJSONObject("annoucement");
                            announcement_tv.setText(annoucement_item.getString("title"));
                            announcement_tv.setVisibility(View.VISIBLE);
                        } else {
                            announcement_tv.setVisibility(View.GONE);
                        }
                        JSONArray category_json = resultResponse.getJSONArray("category_json");
                        category_json(category_json);
                        JSONObject product = resultResponse.getJSONObject("product");
                        JSONArray all_banners = product.getJSONArray("all_banners");
                        bannersList(all_banners);
                        JSONArray featured_product = product.getJSONArray("featured_product");
                        feacherList(featured_product);
                        JSONArray new_product = product.getJSONArray("new");
                        newProductList(new_product);
                        JSONArray best_deals = product.getJSONArray("best_deal_product");
                        bestDealsList(best_deals);
                        JSONArray top_sellers = product.getJSONArray("seller");
                        topSellers(top_sellers);
                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(HomeActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(HomeActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(HomeActivity.this);
                } else {
                    MethodClass.error_alert(HomeActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(HomeActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(HomeActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void bannersList(JSONArray all_banners) {
        try {
            Log.e("bannerCount", String.valueOf(all_banners.length()));
            ArrayList<HashMap<String, String>> bannersList = new ArrayList<>();
            for (int i = 0; i < all_banners.length(); i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(ID, all_banners.getJSONObject(i).getString("id"));
                hashMap.put(IMAGE, all_banners.getJSONObject(i).getString("banner_image"));

                hashMap.put(URLS, all_banners.getJSONObject(i).getString("banner_buttton_url"));

                hashMap.put(HEADING, all_banners.getJSONObject(i).getString("banner_heading"));
                hashMap.put(SUB_HEADING, all_banners.getJSONObject(i).getString("banner_sub_heading"));
                hashMap.put(BANNER_DESC, all_banners.getJSONObject(i).getString("banner_desc"));
                hashMap.put(BUTTON_CAP, all_banners.getJSONObject(i).getString("banner_buttton_cap"));
                bannersList.add(hashMap);
            }
            HomeSliderAdapter imageAdapter = new HomeSliderAdapter(HomeActivity.this, bannersList);
            imageSlider.setSliderAdapter(imageAdapter);
            imageSlider.startAutoCycle();
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("bannersList", e.toString());
        }
    }

    private void feacherList(final JSONArray featured_product) {
        Log.e("featured_product", featured_product.toString());
        if(featured_product.length()>0){
            Float afeachurLeFloat = Float.valueOf(featured_product.length());
            int featur_lenth = featured_product.length() % 2 == 0 ? (int) (afeachurLeFloat / 2) : (int) ((afeachurLeFloat / 2) + (0.5));
            final int[] finalPosition = {0};
            carouselView.setSize(featur_lenth);
            carouselView.setResource(R.layout.home_featu_pro_item);
            carouselView.setAutoPlay(true);
            carouselView.setIndicatorAnimationType(IndicatorAnimationType.THIN_WORM);
            carouselView.setCarouselOffset(OffsetType.CENTER);
            carouselView.setCarouselViewListener(new CarouselViewListener() {
                @Override
                public void onBindView(View view, int position) {
                    try {
                        if (position == 0) {
                            finalPosition[0] = 0;
                        }
                        RelativeLayout view_layout = view.findViewById(R.id.view_layout1);
                        TextView title_tv = view.findViewById(R.id.title_tv);
                        TextView prctg_offer_tv = view.findViewById(R.id.prctg_offer_tv);
                        TextView product_new_Price = view.findViewById(R.id.product_new_Price);
                        TextView product_old_Price = view.findViewById(R.id.product_old_Price);
                        ImageView Prod_image1 = view.findViewById(R.id.Prod_image1);
                        ImageView like_img = view.findViewById(R.id.like_img);
                        LinearLayout offer_lay = view.findViewById(R.id.offer_lay);
                        title_tv.setText(featured_product.getJSONObject(finalPosition[0]).getJSONObject("product_by_language").getString("title"));
                        final String id = featured_product.getJSONObject(finalPosition[0]).getString("id");
                        float price = Float.parseFloat(featured_product.getJSONObject(finalPosition[0]).getString("price"));
                        float discount_price = Float.parseFloat(featured_product.getJSONObject(finalPosition[0]).getString("discount_price"));
                        String from_date = featured_product.getJSONObject(finalPosition[0]).getString("from_date");
                        String to_date = featured_product.getJSONObject(finalPosition[0]).getString("to_date");

                        if (discount_price == 0) {
                            product_new_Price.setText(getString(R.string.kd)+" "+ featured_product.getJSONObject(finalPosition[0]).getString("price"));
                            product_old_Price.setVisibility(View.GONE);
                            offer_lay.setVisibility(View.GONE);
                        } else {
                            if (from_date.equals("null") || from_date.equals(null) || from_date.equals("")) {
                                product_new_Price.setText(getString(R.string.kd)+" " + featured_product.getJSONObject(finalPosition[0]).getString("price"));
                                product_old_Price.setVisibility(View.GONE);
                                offer_lay.setVisibility(View.GONE);
                            } else {
                                try {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date fromDate = dateFormat.parse(from_date);
                                    Date toDate = dateFormat.parse(to_date);
                                    Date dCurrentDate = dateFormat.parse(dateFormat.format(new Date()));
                                    Log.e("TIME", String.valueOf(System.currentTimeMillis())+"      "+String.valueOf(fromDate.getTime()) );
                                    if ((dCurrentDate.equals(fromDate) || dCurrentDate.after(fromDate)) && (dCurrentDate.before(toDate) || dCurrentDate.equals(toDate))) {
                                        product_new_Price.setText(getString(R.string.kd)+" " + featured_product.getJSONObject(finalPosition[0]).getString("discount_price"));
                                        product_old_Price.setText(getString(R.string.kd)+" " + featured_product.getJSONObject(finalPosition[0]).getString("price"));
                                        float final_pres = Math.round(((price - discount_price) / price) * 100);
                                        prctg_offer_tv.setText(final_pres + getString(R.string.per_off));
                                        product_old_Price.setVisibility(View.VISIBLE);
                                        offer_lay.setVisibility(View.VISIBLE);
                                    } else {
                                        product_new_Price.setText(getString(R.string.kd)+" " + featured_product.getJSONObject(finalPosition[0]).getString("price"));
                                        product_old_Price.setVisibility(View.GONE);
                                        offer_lay.setVisibility(View.GONE);
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    Log.e("DateException1", e.getMessage());
                                }
                            }
                        }
                        if(featured_product.getJSONObject((finalPosition[0])).getString("default_image").equals("null") ||featured_product.getJSONObject((finalPosition[0])).getString("default_image").equals(null) || featured_product.getJSONObject((finalPosition[0])).getString("default_image").equals("")){
                            String pro_img = "";
                            Picasso.get().load(PRODUCT_IMAGE_URL + pro_img).error(R.drawable.applogo).placeholder(R.drawable.applogo).into(Prod_image1);
                        }else {
                            String pro_img = featured_product.getJSONObject((finalPosition[0])).getJSONObject("default_image").getString("image");
                            Picasso.get().load(PRODUCT_IMAGE_URL + pro_img).error(R.drawable.applogo).placeholder(R.drawable.applogo).into(Prod_image1);
                        }

                        view_layout.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(HomeActivity.this, ProductDetailsActivity.class);
                                intent.putExtra("id", id);
                                startActivity(intent);
                            }
                        });


                        like_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getBoolean("is_logged_in", false)) {
                                    WishListAddRemoveClass.addwishList(HomeActivity.this, id);
                                } else {
                                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_login_first), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }
                            }
                        });


                        RelativeLayout view_layout2 = view.findViewById(R.id.view_layout2);
                        if (featured_product.length() > (finalPosition[0] + 1)) {
                            TextView title_tv2 = view.findViewById(R.id.title_tv2);
                            TextView prctg_offer_tv2 = view.findViewById(R.id.prctg_offer_tv2);
                            TextView product_new_Price2 = view.findViewById(R.id.product_new_Price2);
                            TextView product_old_Price2 = view.findViewById(R.id.product_old_Price2);
                            ImageView Prod_image2 = view.findViewById(R.id.prod_image);
                            ImageView like_img2 = view.findViewById(R.id.like_img2);
                            LinearLayout offer_lay2 = view.findViewById(R.id.offer_lay2);
                            title_tv2.setText(featured_product.getJSONObject((finalPosition[0] + 1)).getJSONObject("product_by_language").getString("title"));
                            final String id2 = featured_product.getJSONObject((finalPosition[0] + 1)).getString("id");
                            float price2 = Float.parseFloat(featured_product.getJSONObject((finalPosition[0] + 1)).getString("price"));
                            float discount_price2 = Float.parseFloat(featured_product.getJSONObject((finalPosition[0] + 1)).getString("discount_price"));
                            String from_date2 = featured_product.getJSONObject((finalPosition[0] + 1)).getString("from_date");
                            String to_date2 = featured_product.getJSONObject((finalPosition[0] + 1)).getString("to_date");

                            if (discount_price2 == 0) {
                                product_new_Price2.setText(getString(R.string.kd)+" " + featured_product.getJSONObject((finalPosition[0] + 1)).getString("price"));
                                product_old_Price2.setVisibility(View.GONE);
                                offer_lay2.setVisibility(View.GONE);
                            } else {
                                if (from_date2.equals("null") || from_date2.equals(null) || from_date2.equals("")) {
                                    product_new_Price2.setText(getString(R.string.kd)+" " + featured_product.getJSONObject((finalPosition[0] + 1)).getString("price"));
                                    product_old_Price2.setVisibility(View.GONE);
                                    offer_lay2.setVisibility(View.GONE);
                                } else {
                                    try {
                                        SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd");
                                        Date fromDate2 = dateFormat2.parse(from_date2);
                                        Date toDate2 = dateFormat2.parse(to_date2);
                                        Date dCurrentDate2 = dateFormat2.parse(dateFormat2.format(new Date()));
                                        Log.e("TIME", String.valueOf(System.currentTimeMillis())+"      "+String.valueOf(fromDate2.getTime()) );
                                        if ((dCurrentDate2.equals(fromDate2) || dCurrentDate2.after(fromDate2)) && (dCurrentDate2.before(toDate2) || dCurrentDate2.equals(toDate2))) {
                                            product_new_Price2.setText(getString(R.string.kd)+" " + featured_product.getJSONObject((finalPosition[0] + 1)).getString("discount_price"));
                                            product_old_Price2.setText(getString(R.string.kd)+" " + featured_product.getJSONObject((finalPosition[0] + 1)).getString("price"));
                                            float final_pres2 = Math.round(((price2 - discount_price2) / price2) * 100);
                                            prctg_offer_tv2.setText(final_pres2 + getString(R.string.per_off));
                                            product_old_Price2.setVisibility(View.VISIBLE);
                                            offer_lay2.setVisibility(View.VISIBLE);
                                        } else {
                                            product_new_Price2.setText(getString(R.string.kd)+" " + featured_product.getJSONObject((finalPosition[0] + 1)).getString("price"));
                                            product_old_Price2.setVisibility(View.GONE);
                                            offer_lay2.setVisibility(View.GONE);
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                        Log.e("DateException2", e.getMessage());
                                    }
                                }
                            }
                            if(featured_product.getJSONObject((finalPosition[0] + 1)).getString("default_image").equals("null") ||featured_product.getJSONObject((finalPosition[0] + 1)).getString("default_image").equals(null) || featured_product.getJSONObject((finalPosition[0] + 1)).getString("default_image").equals("")){
                                String pro_img2 = "";
                                Picasso.get().load(PRODUCT_IMAGE_URL + pro_img2).error(R.drawable.applogo).placeholder(R.drawable.applogo).into(Prod_image2);
                            }else {
                                String pro_img2 = featured_product.getJSONObject((finalPosition[0] + 1)).getJSONObject("default_image").getString("image");
                                Picasso.get().load(PRODUCT_IMAGE_URL + pro_img2).error(R.drawable.applogo).placeholder(R.drawable.applogo).into(Prod_image2);
                            }

                            view_layout2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        Intent intent = new Intent(HomeActivity.this, ProductDetailsActivity.class);
                                        intent.putExtra("id", id2);
                                        startActivity(intent);
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Log.e("JSONException2", e.toString());
                                    }
                                }
                            });
                            like_img2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getBoolean("is_logged_in", false)) {
                                        WishListAddRemoveClass.addwishList(HomeActivity.this, id2);
                                    } else {
                                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_login_first), Snackbar.LENGTH_SHORT);
                                        snackbar.show();
                                    }
                                }
                            });
                        } else {
                            view_layout2.setVisibility(View.INVISIBLE);
                        }
                        finalPosition[0] = finalPosition[0] + 2;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("FeacherError", e.getMessage());
                    }
                }
            });
            carouselView.show();
        }


    }

    private void newProductList(JSONArray new_product) {
        try {
            ArrayList<HashMap<String, String>> newProductArrList = new ArrayList<>();
            for (int i = 0; i < new_product.length(); i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(ID, new_product.getJSONObject(i).getString("id"));
                hashMap.put(FROM_DATE, new_product.getJSONObject(i).getString("from_date"));
                hashMap.put(TO_DATE, new_product.getJSONObject(i).getString("to_date"));
                hashMap.put(PRICE, new_product.getJSONObject(i).getString("price"));
                hashMap.put(DISCOUNT_PRICE, new_product.getJSONObject(i).getString("discount_price"));
                if (!new_product.getJSONObject(i).getString("default_image").equals("null")) {
                    hashMap.put(IMAGE, new_product.getJSONObject(i).getJSONObject("default_image").getString("image"));
                }
                hashMap.put(TITLE, new_product.getJSONObject(i).getJSONObject("product_by_language").getString("title"));
                newProductArrList.add(hashMap);
            }
            HomeNewProdAdapter homeNewProdAdapter = new HomeNewProdAdapter(HomeActivity.this, newProductArrList);
            recy_view_new_prod.setAdapter(homeNewProdAdapter);
            recy_view_new_prod.setFocusable(false);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("newProductList", e.toString());
        }
    }

    private void bestDealsList(JSONArray best_deals) {
        try {
            ArrayList<HashMap<String, String>> bestDealsArrList = new ArrayList<>();
            for (int i = 0; i < best_deals.length(); i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(ID, best_deals.getJSONObject(i).getString("id"));
                hashMap.put(FROM_DATE, best_deals.getJSONObject(i).getString("from_date"));
                hashMap.put(TO_DATE, best_deals.getJSONObject(i).getString("to_date"));
                hashMap.put(PRICE, best_deals.getJSONObject(i).getString("price"));
                hashMap.put(DISCOUNT_PRICE, best_deals.getJSONObject(i).getString("discount_price"));
                if (!best_deals.getJSONObject(i).getString("default_image").equals("null")) {
                    hashMap.put(IMAGE, best_deals.getJSONObject(i).getJSONObject("default_image").getString("image"));
                }
                hashMap.put(TITLE, best_deals.getJSONObject(i).getJSONObject("product_by_language").getString("title"));
                bestDealsArrList.add(hashMap);
            }
            HomeBestDealsAdapter homeBestDealsAdapter = new HomeBestDealsAdapter(HomeActivity.this, bestDealsArrList);
            recy_view_best_prod.setAdapter(homeBestDealsAdapter);
            recy_view_best_prod.setFocusable(false);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("bestDealsList", e.toString());
        }

    }

    private void topSellers(JSONArray top_sellers) {
        try {
            ArrayList<HashMap<String, String>> topSellersArrList = new ArrayList<>();
            for (int i = 0; i < top_sellers.length(); i++) {
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(ID, top_sellers.getJSONObject(i).getString("id"));
                hashMap.put(NAME, top_sellers.getJSONObject(i).getString("fname")+" "+top_sellers.getJSONObject(i).getString("lname"));
                String image = top_sellers.getJSONObject(i).getString("image");
                if (!image.equals("null") && !image.equals(null) && !image.equals("")) {
                    hashMap.put(IMAGE, image);
                }
                topSellersArrList.add(hashMap);
            }
            HomeTopSellAdapter homeTopSellAdapter = new HomeTopSellAdapter(HomeActivity.this, topSellersArrList);
            recy_view_top_sell.setAdapter(homeTopSellAdapter);
            recy_view_top_sell.setFocusable(false);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("bestDealsList", e.toString());
        }

    }

    private void category_json(JSONArray category_json) {
        try {
            for (int i = 0; i < category_json.length(); i++) {
                JSONObject catObj = category_json.getJSONObject(i);
                String slug = catObj.getString("slug");
                String picture = catObj.getString("picture");
                String title1 = catObj.getJSONArray("details").getJSONObject(0).getString("title");
                String title2 = catObj.getJSONArray("details").getJSONObject(1).getString("title");
                tagArrayListId_1.add(new MethodClass.StringWithTag(title1, slug));
                tagArrayListId_2.add(new MethodClass.StringWithTag(title2, slug));
                ArrayList<HashMap<String, String>> subCatArrayListID_1 = new ArrayList<>();
                ArrayList<HashMap<String, String>> subCatArrayListID_2 = new ArrayList<>();
                for (int j = 0; j < catObj.getJSONArray("sub_categories").length(); j++) {
                    JSONObject sub_categoriesOBJ = catObj.getJSONArray("sub_categories").getJSONObject(j);
                    String sub_slug = sub_categoriesOBJ.getString("slug");
                    String sub_title1 = sub_categoriesOBJ.getJSONArray("details").getJSONObject(0).getString("title");
                    String sub_title2 = sub_categoriesOBJ.getJSONArray("details").getJSONObject(1).getString("title");
                    tagArrayListId_1.add(new MethodClass.StringWithTag(sub_title1, sub_slug));
                    tagArrayListId_2.add(new MethodClass.StringWithTag(sub_title2, sub_slug));
                    ////////////////////////////////////////////////////////////////////////
                    HashMap<String, String> subCatHashMap1 = new HashMap<>();
                    subCatHashMap1.put(SLUG, sub_slug);
                    subCatHashMap1.put(TITLE, sub_title1);
                    subCatArrayListID_1.add(subCatHashMap1);
                    HashMap<String, String> subCatHashMap2 = new HashMap<>();
                    subCatHashMap2.put(SLUG, sub_slug);
                    subCatHashMap2.put(TITLE, sub_title2);
                    subCatArrayListID_2.add(subCatHashMap2);
                    ////////////////////////////////////////////////////////////////////////
                }
                //////////////////////////////////////////////////////////
                HashMap<String, Object> hashMap1 = new HashMap<>();
                hashMap1.put(SLUG, slug);
                hashMap1.put(TITLE, title1);
                hashMap1.put(IMAGE, picture);
                hashMap1.put(SUB_CAT_ARRLIST, subCatArrayListID_1);
                catArrayListID_1.add(hashMap1);
                HashMap<String, Object> hashMap2 = new HashMap<>();
                hashMap2.put(SLUG, slug);
                Log.e("SLUG", slug );
                hashMap2.put(TITLE, title2);
                hashMap2.put(IMAGE, picture);
                hashMap2.put(SUB_CAT_ARRLIST, subCatArrayListID_2);
                catArrayListID_2.add(hashMap2);
                //////////////////////////////////////////////////////////
            }
            if (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("LANG", "").equals("en")) {
                HomeCatAdapter homeCatAdapter = new HomeCatAdapter(HomeActivity.this, catArrayListID_1);
                recy_view_category.setAdapter(homeCatAdapter);
                recy_view_category.setFocusable(false);
            } else {
                HomeCatAdapter homeCatAdapter = new HomeCatAdapter(HomeActivity.this, catArrayListID_2);
                recy_view_category.setAdapter(homeCatAdapter);
                recy_view_category.setFocusable(false);
            }
            if (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("LANG", "").equals("en")) {
                ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(HomeActivity.this, android.R.layout.simple_dropdown_item_1line, tagArrayListId_1);
                search_auto_text.setAdapter(adapter);
                search_auto_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                        String cat_slug = "0";
                        String cat_sub_slug = "0";
                        String key = (String) stringWithTag.tag;
                        for (int i = 0; i < catArrayListID_1.size(); i++) {
                            HashMap<String, Object> hashMap = catArrayListID_1.get(i);
                            if (String.valueOf(hashMap.get(SLUG)).equals(key)) {
                                cat_slug = key;
                            }
                        }
                        if (cat_slug.equals("0")) {
                            cat_sub_slug = key;
                        }
                        Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                        intent.putExtra("cat_slug", cat_slug);
                        intent.putExtra("sub_cat_slug", cat_sub_slug);
                        intent.putExtra("brand", "0");
                        intent.putExtra("variant", "0");
                        intent.putExtra("other_variant", "0");
                        intent.putExtra("price", "0");
                        intent.putExtra("order_by", "0");
                        intent.putExtra("product", "");
                        startActivity(intent);
                    }
                });

            } else {
                ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(HomeActivity.this, android.R.layout.simple_dropdown_item_1line, tagArrayListId_2);
                search_auto_text.setAdapter(adapter);
                search_auto_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                        String cat_slug = "0";
                        String cat_sub_slug = "0";
                        String key = (String) stringWithTag.tag;
                        for (int i = 0; i < catArrayListID_1.size(); i++) {
                            HashMap<String, Object> hashMap = catArrayListID_1.get(i);
                            if (String.valueOf(hashMap.get(SLUG)).equals(key)) {
                                cat_slug = key;
                            }
                        }
                        if (cat_slug.equals("0")) {
                            cat_sub_slug = key;
                        }
                        Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                        intent.putExtra("cat_slug", cat_slug);
                        intent.putExtra("sub_cat_slug", cat_sub_slug);
                        intent.putExtra("brand", "0");
                        intent.putExtra("variant", "0");
                        intent.putExtra("other_variant", "0");
                        intent.putExtra("price", "0");
                        intent.putExtra("order_by", "0");
                        intent.putExtra("product", "");
                        startActivity(intent);
                    }
                });
            }

            search_auto_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    boolean handled = false;
                    if (actionId == EditorInfo.IME_ACTION_SEND) {
                        Intent intent = new Intent(HomeActivity.this, SearchActivity.class);
                        intent.putExtra("cat_slug", "");
                        intent.putExtra("sub_cat_slug", "");
                        intent.putExtra("brand", "0");
                        intent.putExtra("variant", "0");
                        intent.putExtra("other_variant", "0");
                        intent.putExtra("price", "0");
                        intent.putExtra("order_by", "0");
                        intent.putExtra("product", search_auto_text.getText().toString());
                        startActivity(intent);
                        handled = true;
                    }
                    return handled;
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("category_json", e.toString());
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isFirst) {
            getCartCount();
        } else {
            isFirst = true;
        }
       /* new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        },1000);*/
        MethodClass.bottomMenu(HomeActivity.this);
        MethodClass.setMenuColor(HomeActivity.this, "home");
    }

    public void profile(View view) {
        if (!PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getBoolean("is_logged_in", false)) {
            Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        }

    }

    public void changeLanguage(View view) {
        final Dialog dialog = new Dialog(HomeActivity.this);
        dialog.setContentView(R.layout.chooselauguage_popup);
        LinearLayout arabic_lay = dialog.findViewById(R.id.arabic_lay);
        LinearLayout english_lay = dialog.findViewById(R.id.english_lay);
        arabic_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).edit().putString("LANG", "ar").commit();
                recreate();
                dialog.dismiss();
            }
        });
        english_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).edit().putString("LANG", "en").commit();
                recreate();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void changeLanguage(final String lang) {
        if (!isNetworkConnected(HomeActivity.this)) {
            MethodClass.network_error_alert(HomeActivity.this);
            return;
        }
        MethodClass.showProgressDialog(HomeActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "change-lang";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("lang", lang);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(HomeActivity.this);
                Log.e("respUser", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(HomeActivity.this, response);
                    if (resultResponse != null) {
                        PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).edit().putString("LANG", lang).commit();
                        recreate();
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(HomeActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(HomeActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(HomeActivity.this);
                } else {
                    MethodClass.error_alert(HomeActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(HomeActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void getCartCount() {
        if (!isNetworkConnected(HomeActivity.this)) {
            MethodClass.network_error_alert(HomeActivity.this);
            return;
        }
        // MethodClass.showProgressDialog(HomeActivity.this);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String server_url = "";
        if (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getBoolean("is_logged_in", false)) {
            server_url = getString(R.string.SERVER_URL) + "home";
        } else {
            server_url = getString(R.string.SERVER_URL) + "home/" + android_id;
        }
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                // MethodClass.hideProgressDialog(HomeActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(HomeActivity.this, response);
                    if (resultResponse != null) {
                        if (resultResponse.has("cart_item") && !resultResponse.getString("cart_item").equals("null") && !resultResponse.getString("cart_item").equals(null)) {
                            JSONObject cart_item = resultResponse.getJSONObject("cart_item");
                            int total_item = cart_item.getInt("total_item");
                            if (total_item > 0) {
                                cart_tv.setText(String.valueOf(total_item));
                                cart_tv.setVisibility(View.VISIBLE);
                            } else {
                                cart_tv.setVisibility(View.GONE);
                            }
                        } else {
                            cart_tv.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(HomeActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                //  MethodClass.hideProgressDialog(HomeActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(HomeActivity.this);
                } else {
                    MethodClass.error_alert(HomeActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(HomeActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(HomeActivity.this).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(HomeActivity.this).addToRequestQueue(jsonObjectRequest);
    }


}
