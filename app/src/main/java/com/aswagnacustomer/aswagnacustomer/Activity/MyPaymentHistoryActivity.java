package com.aswagnacustomer.aswagnacustomer.Activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.MyRewardPointAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.PaymentHistoryAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.AMOUNT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_AMOUNT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_RECEIVED;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_USED;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TRANSACTION_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class MyPaymentHistoryActivity extends AppCompatActivity {

    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_my_payment_history);
        recy_view = findViewById(R.id.recy_view);
        customer_reward_points();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(MyPaymentHistoryActivity.this);
        MethodClass.setMenuColor(MyPaymentHistoryActivity.this, "no");
    }

    public void back(View view) {
        super.onBackPressed();
    }

    private void customer_reward_points() {
        if (!isNetworkConnected(MyPaymentHistoryActivity.this)) {
            MethodClass.network_error_alert(MyPaymentHistoryActivity.this);
            return;
        }
        MethodClass.showProgressDialog(MyPaymentHistoryActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "payment-history";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(MyPaymentHistoryActivity.this);
                Log.e("payment-historyREs", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(MyPaymentHistoryActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray payments = resultResponse.getJSONArray("payments");
                        arrayList = new ArrayList<>();
                        for (int i = 0; i < payments.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            if(!payments.getJSONObject(i).getString("order_master_tab").equals("null")){
                                hashMap.put(ORDER_NO, payments.getJSONObject(i).getJSONObject("order_master_tab").getString("order_no"));
                            }else{
                                hashMap.put(ORDER_NO, "-");
                            }

                            hashMap.put(CREATED_AT, payments.getJSONObject(i).getString("created_at"));
                            if(!payments.getJSONObject(i).getString("txn_id").equals("null")){
                                hashMap.put(TRANSACTION_NO, payments.getJSONObject(i).getString("txn_id"));
                            }else{
                                hashMap.put(TRANSACTION_NO, "-");
                            }

                            hashMap.put(AMOUNT, payments.getJSONObject(i).getString("amount"));
                            hashMap.put(PAYMENT_METHODE, payments.getJSONObject(i).getString("payment_mode"));
                            arrayList.add(hashMap);
                        }
                        PaymentHistoryAdapter  historyAdapter = new PaymentHistoryAdapter(MyPaymentHistoryActivity.this, arrayList);
                        recy_view.setAdapter(historyAdapter);
                        recy_view.setFocusable(false);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(MyPaymentHistoryActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(MyPaymentHistoryActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(MyPaymentHistoryActivity.this);
                } else {
                    MethodClass.error_alert(MyPaymentHistoryActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(MyPaymentHistoryActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MyPaymentHistoryActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(MyPaymentHistoryActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
