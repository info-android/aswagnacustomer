package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class AddAddressActivity extends AppCompatActivity {

    private EditText shi_first_name_et, shi_last_name_et, shi_phone_et, shi_email_et, shi_city_et, shi_block_et, shi_street_et, shi_building_et, shi_more_address_et, shi_postal_code_et;
    private Spinner shi_spin_country;
    private AutoCompleteTextView shi_city_autoTextw;
    EditText shi_location_placeText;
    private Button shi_save_btn;
    private String shi_country_id = "", shi_country_str = "", shi_city_id = "", shi_city_str = "";
    public ArrayList<MethodClass.StringWithTag> countriesArrayList;
    public ArrayList<MethodClass.StringWithTag> cityArrayList;
    public String shi_lat = "", shi_long = "";
    public CheckBox is_default;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_add_address);
        shi_first_name_et = findViewById(R.id.shi_first_name_et);
        shi_last_name_et = findViewById(R.id.shi_last_name_et);
        shi_phone_et = findViewById(R.id.shi_phone_et);
        shi_email_et = findViewById(R.id.shi_email_et);
        shi_city_et = findViewById(R.id.shi_city_et);
        shi_block_et = findViewById(R.id.shi_block_et);
        shi_street_et = findViewById(R.id.shi_street_et);
        shi_building_et = findViewById(R.id.shi_building_et);
        shi_more_address_et = findViewById(R.id.shi_more_address_et);
        shi_postal_code_et = findViewById(R.id.shi_postal_code_et);
        shi_spin_country = findViewById(R.id.shi_spin_country);
        shi_city_autoTextw = findViewById(R.id.shi_city_autoTextw);
        shi_save_btn = findViewById(R.id.shi_save_btn);
        shi_location_placeText = findViewById(R.id.shi_location_placeText);
        is_default = findViewById(R.id.is_default);

//        shi_location_placeText.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
//            @Override
//            public void onPlaceSelected(final Place place) {
//                Log.e("placebdec", place.description);
//                shi_location_placeText.getDetailsFor(place, new DetailsCallback() {
//                    @Override
//                    public void onSuccess(PlaceDetails details) {
//                        shi_lat = String.valueOf(details.geometry.location.lat);
//                        shi_long = String.valueOf(details.geometry.location.lng);
//                        Log.e("loglat", shi_lat + "////" + shi_long);
//                    }
//
//                    @Override
//                    public void onFailure(Throwable failure) {
//                    }
//
//                    public void onComplete() {
//                    }
//                });
//            }
//        });


        shi_save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fname, lname, phone, email, country, city, block, street, building, more_add, postal_code, location;
                fname = shi_first_name_et.getText().toString().trim();
                lname = shi_last_name_et.getText().toString().trim();
                phone = shi_phone_et.getText().toString().trim();
                email = shi_email_et.getText().toString().trim();
                country = shi_country_str;
                if (shi_country_id.equals("134")) {
                    city = shi_city_id;
                } else {
                    city = shi_city_et.getText().toString().trim();
                }
                block = shi_block_et.getText().toString().trim();
                street = shi_street_et.getText().toString().trim();
                building = shi_building_et.getText().toString().trim();
                more_add = shi_more_address_et.getText().toString().trim();
                if (fname.length() == 0) {
                    shi_first_name_et.setError(getResources().getString(R.string.please_enter_first_name));
                    shi_first_name_et.requestFocus();
                    return;
                }
                if (lname.length() == 0) {
                    shi_last_name_et.setError(getResources().getString(R.string.please_enter_last_name));
                    shi_last_name_et.requestFocus();
                    return;
                }
                if (phone.length() == 0) {
                    shi_phone_et.setError(getResources().getString(R.string.please_enter_mobile_no));
                    shi_phone_et.requestFocus();
                    return;
                }
                if (email.length() == 0) {
                    shi_email_et.setError(getResources().getString(R.string.please_enter_email_address));
                    shi_email_et.requestFocus();
                    return;
                }
                if (!MethodClass.emailValidator(email)) {
                    shi_email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
                    shi_email_et.requestFocus();
                    return;
                }
                if (country.equals("")) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_country), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
                if (shi_country_id.equals("134")) {
                    if (city.equals("")) {
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_city), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                    if (block.length() == 0) {
                        shi_block_et.setError(getResources().getString(R.string.please_enter_block));
                        shi_block_et.requestFocus();
                        return;
                    }
                    if (building.length() == 0) {
                        shi_building_et.setError(getResources().getString(R.string.please_enter_building));
                        shi_building_et.requestFocus();
                        return;
                    }
                } else {
                    if (city.length() == 0) {
                        shi_city_et.setError(getResources().getString(R.string.please_enter_city));
                        shi_city_et.requestFocus();
                        return;
                    }
                }
                if (street.length() == 0) {
                    shi_street_et.setError(getResources().getString(R.string.please_enter_street));
                    shi_street_et.requestFocus();
                    return;
                }

                postAddress();
            }
        });
        get_all_country();

    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(AddAddressActivity.this);
        MethodClass.setMenuColor(AddAddressActivity.this, "no");
    }


    public void get_all_country() {
        if (!isNetworkConnected(AddAddressActivity.this)) {
            MethodClass.network_error_alert(AddAddressActivity.this);
            return;
        }
        MethodClass.showProgressDialog(AddAddressActivity.this);
        String server_url = "";
        server_url = getString(R.string.SERVER_URL) + "get-all-country";
        Log.e("server_url", server_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AddAddressActivity.this);
                Log.e("Responce::", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(AddAddressActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray countries = resultResponse.getJSONArray("countries");
                        countriesArrayList = new ArrayList<>();
                        for (int i = 0; i < countries.length(); i++) {
                            JSONObject object = countries.getJSONObject(i);
                            String country_id = object.getJSONObject("country_details_bylanguage").getString("country_id");
                            String name = object.getJSONObject("country_details_bylanguage").getString("name");
                            MethodClass.StringWithTag stringWithTag = new MethodClass.StringWithTag(name, country_id);
                            countriesArrayList.add(stringWithTag);
                        }
                        ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(AddAddressActivity.this, android.R.layout.simple_dropdown_item_1line, countriesArrayList);
                        shi_spin_country.setAdapter(adapter);
                        shi_spin_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                                shi_country_id = (String) stringWithTag.tag;
                                shi_country_str = stringWithTag.string;

                                if (shi_country_id.equals("134")) {
                                    shi_postal_code_et.setVisibility(View.GONE);
                                    shi_city_et.setVisibility(View.GONE);
                                    shi_city_autoTextw.setVisibility(View.VISIBLE);
                                    shi_block_et.setHint(getResources().getString(R.string.block));
                                    shi_building_et.setHint(getResources().getString(R.string.building));
                                    get_all_city();
                                } else {
                                    shi_postal_code_et.setVisibility(View.VISIBLE);
                                    shi_city_et.setVisibility(View.VISIBLE);
                                    shi_city_autoTextw.setVisibility(View.GONE);
                                    shi_block_et.setHint(getResources().getString(R.string.block_optional));
                                    shi_building_et.setHint(getResources().getString(R.string.building_optional));
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(AddAddressActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(AddAddressActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AddAddressActivity.this);
                } else {
                    MethodClass.error_alert(AddAddressActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AddAddressActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddAddressActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AddAddressActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void get_all_city() {
        if (!isNetworkConnected(AddAddressActivity.this)) {
            MethodClass.network_error_alert(AddAddressActivity.this);
            return;
        }
        String server_url = "";
        server_url = getString(R.string.SERVER_URL) + "get-all-city";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Responce::", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(AddAddressActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray cities = resultResponse.getJSONArray("cities");
                        cityArrayList = new ArrayList<>();
                        for (int i = 0; i < cities.length(); i++) {
                            JSONObject object = cities.getJSONObject(i);
                            String country_id = object.getJSONObject("city_details_by_language").getString("city_id");
                            String name = object.getJSONObject("city_details_by_language").getString("name");
                            MethodClass.StringWithTag stringWithTag = new MethodClass.StringWithTag(name, country_id);
                            cityArrayList.add(stringWithTag);
                        }
                        ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(AddAddressActivity.this, android.R.layout.simple_dropdown_item_1line, cityArrayList);
                        shi_city_autoTextw.setThreshold(1);
                        shi_city_autoTextw.setAdapter(adapter);
                        shi_city_autoTextw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                               Log.e("stringWithTag",stringWithTag.string+"=="+stringWithTag.tag);
                               shi_city_id= String.valueOf(stringWithTag.tag);
                               shi_city_str=stringWithTag.string;
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSONExceptioncity", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AddAddressActivity.this);
                } else {
                    MethodClass.error_alert(AddAddressActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AddAddressActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddAddressActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AddAddressActivity.this).addToRequestQueue(jsonObjectRequest);
    }


    public void postAddress() {
        if (!isNetworkConnected(AddAddressActivity.this)) {
            MethodClass.network_error_alert(AddAddressActivity.this);
            return;
        }
        MethodClass.showProgressDialog(AddAddressActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "add-address-book";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("shipping_fname", shi_first_name_et.getText().toString().trim());
        hashMap.put("shipping_lname", shi_last_name_et.getText().toString().trim());
        hashMap.put("email", shi_email_et.getText().toString().trim());
        hashMap.put("phone", shi_phone_et.getText().toString().trim());
        hashMap.put("country", shi_country_id);
        if (shi_country_id.equals("134")) {
            hashMap.put("city", shi_city_str);
            hashMap.put("city_id", shi_city_id);
        } else {
            hashMap.put("city", shi_city_et.getText().toString().trim());
            hashMap.put("postal_code", shi_postal_code_et.getText().toString().trim());
        }
        hashMap.put("street", shi_street_et.getText().toString().trim());
        hashMap.put("block", shi_block_et.getText().toString().trim());
        hashMap.put("building", shi_building_et.getText().toString().trim());
        hashMap.put("more_address", shi_more_address_et.getText().toString().trim());
        hashMap.put("location", shi_location_placeText.getText().toString().trim());
        hashMap.put("lat", shi_lat);
        hashMap.put("lng", shi_long);
        hashMap.put("is_default", is_default.isChecked() ? "Y" : "N");
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AddAddressActivity.this);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(AddAddressActivity.this, response);
                    SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(AddAddressActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                    sweetAlertDialog.setTitleText(resultResponse.getJSONObject("success").getString("message"));
                    sweetAlertDialog.setContentText(resultResponse.getJSONObject("success").getString("meaning"));
                    sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    });
                    sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            Intent intent = new Intent(AddAddressActivity.this, AddressBookActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    sweetAlertDialog.show();
                } catch (Exception e) {
                    MethodClass.error_alert(AddAddressActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(AddAddressActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AddAddressActivity.this);
                } else {
                    MethodClass.error_alert(AddAddressActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AddAddressActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddAddressActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AddAddressActivity.this).addToRequestQueue(jsonObjectRequest);
    }


}
