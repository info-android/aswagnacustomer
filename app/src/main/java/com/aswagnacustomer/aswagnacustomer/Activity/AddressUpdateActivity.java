package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class AddressUpdateActivity extends AppCompatActivity {

    private EditText shi_first_name_et, shi_last_name_et, shi_phone_et, shi_email_et, shi_city_et, shi_block_et, shi_street_et, shi_building_et, shi_more_address_et, shi_postal_code_et;
    private Spinner shi_spin_country;
    private AutoCompleteTextView shi_city_autoText;
    EditText shi_location_placeText;
    private Button shi_save_btn;
    private String shi_country_id = "", shi_country_str = "", shi_city_id = "", shi_city_str = "";
    public ArrayList<MethodClass.StringWithTag> countriesArrayList;
    public ArrayList<MethodClass.StringWithTag> cityArrayList;
    public String shi_lat = "", shi_long = "";
    public CheckBox is_default;
    private String id="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_address_update);
        shi_first_name_et = findViewById(R.id.shi_first_name_et);
        shi_last_name_et = findViewById(R.id.shi_last_name_et);
        shi_phone_et = findViewById(R.id.shi_phone_et);
        shi_email_et = findViewById(R.id.shi_email_et);
        shi_city_et = findViewById(R.id.shi_city_et);
        shi_block_et = findViewById(R.id.shi_block_et);
        shi_street_et = findViewById(R.id.shi_street_et);
        shi_building_et = findViewById(R.id.shi_building_et);
        shi_more_address_et = findViewById(R.id.shi_more_address_et);
        shi_postal_code_et = findViewById(R.id.shi_postal_code_et);
        shi_spin_country = findViewById(R.id.shi_spin_country);
        shi_city_autoText = findViewById(R.id.shi_city_autoText);
        shi_save_btn = findViewById(R.id.shi_save_btn);
        shi_location_placeText = findViewById(R.id.shi_location_placeText);
        is_default = findViewById(R.id.is_default);
        id= getIntent().getStringExtra("id");
//        shi_location_placeText.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
//            @Override
//            public void onPlaceSelected(final Place place) {
//                Log.e("placebdec", place.description);
//                shi_location_placeText.getDetailsFor(place, new DetailsCallback() {
//                    @Override
//                    public void onSuccess(PlaceDetails details) {
//                        shi_lat = String.valueOf(details.geometry.location.lat);
//                        shi_long = String.valueOf(details.geometry.location.lng);
//                        Log.e("loglat", shi_lat + "////" + shi_long);
//                    }
//
//                    @Override
//                    public void onFailure(Throwable failure) {
//                    }
//
//                    public void onComplete() {
//                    }
//                });
//            }
//        });


        shi_save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fname, lname, phone, email, country, city, block, street, building, more_add, postal_code, location;
                fname = shi_first_name_et.getText().toString().trim();
                lname = shi_last_name_et.getText().toString().trim();
                phone = shi_phone_et.getText().toString().trim();
                email = shi_email_et.getText().toString().trim();
                country = shi_country_str;
                if (shi_country_id.equals("134")) {
                    city = shi_city_id;
                } else {
                    city = shi_city_et.getText().toString().trim();
                }
                block = shi_block_et.getText().toString().trim();
                street = shi_street_et.getText().toString().trim();
                building = shi_building_et.getText().toString().trim();
                more_add = shi_more_address_et.getText().toString().trim();
                if (fname.length() == 0) {
                    shi_first_name_et.setError(getResources().getString(R.string.please_enter_first_name));
                    shi_first_name_et.requestFocus();
                    return;
                }
                if (lname.length() == 0) {
                    shi_last_name_et.setError(getResources().getString(R.string.please_enter_last_name));
                    shi_last_name_et.requestFocus();
                    return;
                }
                if (phone.length() == 0) {
                    shi_phone_et.setError(getResources().getString(R.string.please_enter_mobile_no));
                    shi_phone_et.requestFocus();
                    return;
                }
                if (email.length() == 0) {
                    shi_email_et.setError(getResources().getString(R.string.please_enter_email_address));
                    shi_email_et.requestFocus();
                    return;
                }
                if (!MethodClass.emailValidator(email)) {
                    shi_email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
                    shi_email_et.requestFocus();
                    return;
                }
                if (country.equals("")) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_country), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    return;
                }
                if (shi_country_id.equals("134")) {
                    if (city.equals("")) {
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_city), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                    if (block.length() == 0) {
                        shi_block_et.setError(getResources().getString(R.string.please_enter_block));
                        shi_block_et.requestFocus();
                        return;
                    }
                    if (building.length() == 0) {
                        shi_building_et.setError(getResources().getString(R.string.please_enter_building));
                        shi_building_et.requestFocus();
                        return;
                    }
                } else {
                    if (city.length() == 0) {
                        shi_city_et.setError(getResources().getString(R.string.please_enter_city));
                        shi_city_et.requestFocus();
                        return;
                    }
                }
                if (street.length() == 0) {
                    shi_street_et.setError(getResources().getString(R.string.please_enter_street));
                    shi_street_et.requestFocus();
                    return;
                }

                postAddress();
            }
        });
        get_all_country();
    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(AddressUpdateActivity.this);
        MethodClass.setMenuColor(AddressUpdateActivity.this, "no");
    }


    public void get_all_country() {
        if (!isNetworkConnected(AddressUpdateActivity.this)) {
            MethodClass.network_error_alert(AddressUpdateActivity.this);
            return;
        }
        MethodClass.showProgressDialog(AddressUpdateActivity.this);
        String server_url = "";
        server_url = getString(R.string.SERVER_URL) + "get-all-country";
        Log.e("server_url", server_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AddressUpdateActivity.this);
                Log.e("Responce::", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(AddressUpdateActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray countries = resultResponse.getJSONArray("countries");
                        countriesArrayList = new ArrayList<>();
                        for (int i = 0; i < countries.length(); i++) {
                            JSONObject object = countries.getJSONObject(i);
                            String country_id = object.getJSONObject("country_details_bylanguage").getString("country_id");
                            String name = object.getJSONObject("country_details_bylanguage").getString("name");
                            MethodClass.StringWithTag stringWithTag = new MethodClass.StringWithTag(name, country_id);
                            countriesArrayList.add(stringWithTag);
                        }
                        ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(AddressUpdateActivity.this, android.R.layout.simple_dropdown_item_1line, countriesArrayList);
                        shi_spin_country.setAdapter(adapter);
                        shi_spin_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                                shi_country_id = (String) stringWithTag.tag;
                                shi_country_str = stringWithTag.string;

                                if (shi_country_id.equals("134")) {
                                    shi_postal_code_et.setVisibility(View.GONE);
                                    shi_city_et.setVisibility(View.GONE);
                                    shi_city_autoText.setVisibility(View.VISIBLE);
                                    shi_block_et.setHint(getResources().getString(R.string.block));
                                    shi_building_et.setHint(getResources().getString(R.string.building));
                                    get_all_city();
                                } else {
                                    shi_postal_code_et.setVisibility(View.VISIBLE);
                                    shi_city_et.setVisibility(View.VISIBLE);
                                    shi_city_autoText.setVisibility(View.GONE);
                                    shi_block_et.setHint(getResources().getString(R.string.block_optional));
                                    shi_building_et.setHint(getResources().getString(R.string.building_optional));
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        get_address();
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(AddressUpdateActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(AddressUpdateActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AddressUpdateActivity.this);
                } else {
                    MethodClass.error_alert(AddressUpdateActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AddressUpdateActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddressUpdateActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AddressUpdateActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void get_all_city() {
        if (!isNetworkConnected(AddressUpdateActivity.this)) {
            MethodClass.network_error_alert(AddressUpdateActivity.this);
            return;
        }
        String server_url = "";
        server_url = getString(R.string.SERVER_URL) + "get-all-city";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("city_name", shi_city_autoText.getText().toString().trim());
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Responce::", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(AddressUpdateActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray cities = resultResponse.getJSONArray("cities");
                        cityArrayList = new ArrayList<>();
                        for (int i = 0; i < cities.length(); i++) {
                            JSONObject object = cities.getJSONObject(i);
                            String country_id = object.getJSONObject("city_details_by_language").getString("city_id");
                            String name = object.getJSONObject("city_details_by_language").getString("name");
                            MethodClass.StringWithTag stringWithTag = new MethodClass.StringWithTag(name, country_id);
                            cityArrayList.add(stringWithTag);
                        }
                        ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(AddressUpdateActivity.this, android.R.layout.simple_dropdown_item_1line, cityArrayList);
                        shi_city_autoText.setAdapter(adapter);
                        shi_city_autoText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                               Log.e("stringWithTag",stringWithTag.string+"=="+stringWithTag.tag);
                               shi_city_id= String.valueOf(stringWithTag.tag);
                               shi_city_str=stringWithTag.string;
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("JSONExceptioncity", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AddressUpdateActivity.this);
                } else {
                    MethodClass.error_alert(AddressUpdateActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AddressUpdateActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddressUpdateActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AddressUpdateActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void get_address() {
        if (!isNetworkConnected(AddressUpdateActivity.this)) {
            MethodClass.network_error_alert(AddressUpdateActivity.this);
            return;
        }
        MethodClass.showProgressDialog(AddressUpdateActivity.this);
        String server_url = "";
        server_url = getString(R.string.SERVER_URL) + "edit-address-book/"+id;
        Log.e("server_url", server_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AddressUpdateActivity.this);
                Log.e("Responce::", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(AddressUpdateActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject user_address_book = resultResponse.getJSONObject("user_address_book");

                        shi_first_name_et.setText(user_address_book.getString("shipping_fname"));
                        shi_last_name_et.setText(user_address_book.getString("shipping_lname"));
                        shi_email_et.setText(user_address_book.getString("email"));
                        shi_phone_et.setText(user_address_book.getString("phone"));
                       
                        shi_street_et.setText(user_address_book.getString("street"));

                        if(!user_address_book.getString("building").equals("null") && !user_address_book.getString("building").equals(null) && !user_address_book.getString("building").equals("")){
                            shi_building_et.setText(user_address_book.getString("building"));
                        }else{
                            shi_building_et.setText("");
                        }

                        if(!user_address_book.getString("block").equals("null") && !user_address_book.getString("block").equals(null) && !user_address_book.getString("block").equals("")){
                            shi_block_et.setText(user_address_book.getString("block"));
                        }else{
                            shi_block_et.setText("");
                        }
                        if(!user_address_book.getString("more_address").equals("null") && !user_address_book.getString("more_address").equals(null) && !user_address_book.getString("more_address").equals("")){
                            shi_more_address_et.setText(user_address_book.getString("more_address"));
                        }else{
                            shi_more_address_et.setText("");
                        }

                        shi_location_placeText.setText(user_address_book.getString("location"));
                        is_default.setChecked(user_address_book.getString("is_default").equals("Y"));
                        String postal_code=user_address_book.getString("postal_code");
                        if (!postal_code.equals("") && !postal_code.equals("null") && !postal_code.equals(null)){
                            shi_postal_code_et.setText(postal_code);
                        }

                        shi_country_id=user_address_book.getString("country");
                        if (!shi_country_id.equals("") && !shi_country_id.equals("null") && !shi_country_id.equals(null)) {
                            for (int i = 0; i < countriesArrayList.size(); i++) {
                                MethodClass.StringWithTag stringWithTag =countriesArrayList.get(i);
                                if (shi_country_id.equals(String.valueOf(stringWithTag.tag))){
                                    shi_spin_country.setSelection(i);
                                }
                            }
                            if (shi_country_id.equals("134")){
                                shi_city_autoText.setText(user_address_book.getString("city"));
                                shi_city_id=user_address_book.getString("city_id");
                                shi_city_str=user_address_book.getString("city");
                                shi_city_autoText.setVisibility(View.VISIBLE);
                                shi_city_et.setVisibility(View.GONE);
                            }else {
                                shi_city_et.setText(user_address_book.getString("city"));
                                shi_city_autoText.setVisibility(View.GONE);
                                shi_city_et.setVisibility(View.VISIBLE);
                            }
                        }

                    }
                } catch (Exception e) {
                    MethodClass.error_alert(AddressUpdateActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(AddressUpdateActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AddressUpdateActivity.this);
                } else {
                    MethodClass.error_alert(AddressUpdateActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AddressUpdateActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddressUpdateActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AddressUpdateActivity.this).addToRequestQueue(jsonObjectRequest);
    }


    public void postAddress() {
        if (!isNetworkConnected(AddressUpdateActivity.this)) {
            MethodClass.network_error_alert(AddressUpdateActivity.this);
            return;
        }
        MethodClass.showProgressDialog(AddressUpdateActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "update-address-book";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("shipping_fname", shi_first_name_et.getText().toString().trim());
        hashMap.put("shipping_lname", shi_last_name_et.getText().toString().trim());
        hashMap.put("email", shi_email_et.getText().toString().trim());
        hashMap.put("phone", shi_phone_et.getText().toString().trim());
        hashMap.put("country", shi_country_id);
        if (shi_country_id.equals("134")) {
            hashMap.put("city", shi_city_str);
            hashMap.put("city_id", shi_city_id);
        } else {
            hashMap.put("city", shi_city_et.getText().toString().trim());
            hashMap.put("postal_code", shi_postal_code_et.getText().toString().trim());
        }
        hashMap.put("street", shi_street_et.getText().toString().trim());
        hashMap.put("block", shi_block_et.getText().toString().trim());
        hashMap.put("building", shi_building_et.getText().toString().trim());
        hashMap.put("more_address", shi_more_address_et.getText().toString().trim());
        hashMap.put("location", shi_location_placeText.getText().toString().trim());
        hashMap.put("lat", shi_lat);
        hashMap.put("lng", shi_long);
        hashMap.put("is_default", is_default.isChecked() ? "Y" : "N");
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AddressUpdateActivity.this);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(AddressUpdateActivity.this, response);
                    SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(AddressUpdateActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                    sweetAlertDialog.setTitleText(resultResponse.getJSONObject("success").getString("message"));
                    sweetAlertDialog.setContentText(resultResponse.getJSONObject("success").getString("meaning"));
                    sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    });
                    sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            Intent intent = new Intent(AddressUpdateActivity.this, AddressBookActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    });
                    sweetAlertDialog.show();
                } catch (Exception e) {
                    MethodClass.error_alert(AddressUpdateActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(AddressUpdateActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AddressUpdateActivity.this);
                } else {
                    MethodClass.error_alert(AddressUpdateActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AddressUpdateActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddressUpdateActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AddressUpdateActivity.this).addToRequestQueue(jsonObjectRequest);
    }


}
