package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.PlaceOrderAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_DETAILS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SELLER_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SELLER_NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;


public class PlaceOrderActivity extends AppCompatActivity {
    private RecyclerView recy_view;
    private TextView shi_name_tv, shi_email_tv, shi_phone_tv, shi_address_tv;
    private ImageView shi_edit_img;
    private TextView bil_name_tv, bil_email_tv, bil_phone_tv, bil_address_tv;
    private ImageView bil_edit_img;
    private TextView payment_method_tv, item_tv, total_price_tv, subtotal_price_tv, loyalty_point_descount_tv, total_descount_tv, shi_charge_tv;
    private TextView rid_point_tv,coupon_point_descount_tv;
    private Button confirm_button;
    private RadioButton reed_yes_rb;
    private String order_id = "";
    private LinearLayout reedm_lay, loya_dec_lay,coupon_dec_lay;
    private String loyalty_point = "0.0";
    private double finalPayAmount = 0.0;
    private double balance = 0.0;
    private double discount_price = 0.000;
    private String payment_method = "";
    private NestedScrollView scrollView;
    private EditText couponText;
    private TextView coupTxt;
    private Button couponApply;
    double subTotal = 0;
    double ship_charge = 0;
    double disc = 0;
    double coup_disc = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_place_order);

        recy_view = findViewById(R.id.recy_view);
        couponText = findViewById(R.id.couponText);
        coupTxt = findViewById(R.id.coupTxt);
        coupon_point_descount_tv = findViewById(R.id.coupon_point_descount_tv);
        coupon_dec_lay = findViewById(R.id.coupon_dec_lay);
        couponApply = findViewById(R.id.couponApply);
        shi_name_tv = findViewById(R.id.shi_name_tv);
        shi_email_tv = findViewById(R.id.shi_email_tv);
        shi_phone_tv = findViewById(R.id.shi_phone_tv);
        shi_address_tv = findViewById(R.id.shi_address_tv);
        shi_edit_img = findViewById(R.id.shi_edit_img);
        bil_name_tv = findViewById(R.id.bil_name_tv);
        bil_email_tv = findViewById(R.id.bil_email_tv);
        bil_phone_tv = findViewById(R.id.bil_phone_tv);
        bil_address_tv = findViewById(R.id.bil_address_tv);
        bil_edit_img = findViewById(R.id.bil_edit_img);
        payment_method_tv = findViewById(R.id.payment_method_tv);
        item_tv = findViewById(R.id.item_tv);
        total_price_tv = findViewById(R.id.total_price_tv);
        subtotal_price_tv = findViewById(R.id.subtotal_price_tv);
        loyalty_point_descount_tv = findViewById(R.id.loyalty_point_descount_tv);
        total_descount_tv = findViewById(R.id.total_descount_tv);
        shi_charge_tv = findViewById(R.id.shi_charge_tv);
        rid_point_tv = findViewById(R.id.rid_point_tv);
        reed_yes_rb = findViewById(R.id.reed_yes_rb);
        confirm_button = findViewById(R.id.confirm_button);
        reedm_lay = findViewById(R.id.reedm_lay);
        loya_dec_lay = findViewById(R.id.loya_dec_lay);
        scrollView = findViewById(R.id.scrollView);
        order_id = getIntent().getStringExtra("id");
        get_order();

        confirm_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                place_order();
            }
        });

        couponApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                applyCoupon();
            }
        });

        couponText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {

               if(coupon_point_descount_tv.getText().toString().length() >0){
                   if(s.length() == 0){
                       coupon_dec_lay.setVisibility(View.GONE);
                       coupTxt.setVisibility(View.GONE);
                       couponApply.setVisibility(View.VISIBLE);
                       couponApply.setEnabled(true);
                       couponApply.setText(getString(R.string.apply));
                       disc = disc-coup_disc ;
                       coup_disc = 0;
                       get_order();
                   }
               }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });

    }

    public void applyCoupon(){
        if(couponText.getText().toString().trim().length() == 0){
            couponText.setError(getString(R.string.enterCOu));
            couponText.requestFocus();
            return;
        }
        MethodClass.showProgressDialog(PlaceOrderActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "apply-coupon";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        hashMap.put("order_id", order_id);
        hashMap.put("coupon_code", couponText.getText().toString().trim());
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(PlaceOrderActivity.this);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(PlaceOrderActivity.this, response);
                    if(resultResponse.has("discount_for") ){
                        coupon_point_descount_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(resultResponse.getString("coupon_discount"))));
                        couponApply.setEnabled(false);
                        couponApply.setText(getString(R.string.applied));
                        coupon_dec_lay.setVisibility(View.VISIBLE);
                        new SweetAlertDialog(PlaceOrderActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getString(R.string.couApplied))
                                .setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        }).show();

                        if(resultResponse.getString("discount_for").equals("SHI")){

                            coupTxt.setText("Get "+getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(resultResponse.getString("coupon_discount")))+" on shipping charge");
                            coup_disc = Double.parseDouble(resultResponse.getString("coupon_discount"));
                            disc = disc+coup_disc ;
                            total_descount_tv.setText(getResources().getString(R.string.kd)+" " +String.format(Locale.ENGLISH,"%.3f", disc));
                            double tot_pri = subTotal + ship_charge - disc;
                            total_price_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", tot_pri));
                            coupTxt.setVisibility(View.VISIBLE);
                        }else {
                            coupTxt.setText("Get "+getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(resultResponse.getString("coupon_discount")))+" on subtotal!");

                            coupTxt.setVisibility(View.VISIBLE);
                            coup_disc = Double.parseDouble(resultResponse.getString("coupon_discount"));
                            disc = disc+coup_disc ;
                            total_descount_tv.setText(getResources().getString(R.string.kd)+" " +String.format(Locale.ENGLISH,"%.3f", disc));
                            double tot_pri = subTotal + ship_charge - disc;
                            total_price_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", tot_pri));
                        }

                    }

                } catch (Exception e) {
                    MethodClass.error_alert(PlaceOrderActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(PlaceOrderActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(PlaceOrderActivity.this);
                } else {
                    MethodClass.error_alert(PlaceOrderActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(PlaceOrderActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getString("token", ""));
                } else {
                    headers.put("Authorization", "Bearer " + CheckOutActivity.token);
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(PlaceOrderActivity.this).addToRequestQueue(jsonObjectRequest);
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(PlaceOrderActivity.this);
        MethodClass.setMenuColor(PlaceOrderActivity.this, "no");
    }

    public void back(View view) {
        super.onBackPressed();
    }
    //http://aswagna.co/dev/api/get-order-data/804

    public void get_order() {
        if (!isNetworkConnected(PlaceOrderActivity.this)) {
            MethodClass.network_error_alert(PlaceOrderActivity.this);
            return;
        }
        MethodClass.showProgressDialog(PlaceOrderActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "get-order-data/" + order_id;
        Log.e("order", server_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(PlaceOrderActivity.this);
                Log.e("addToCartRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(PlaceOrderActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject order = resultResponse.getJSONObject("order");
                        item_tv.setText(resultResponse.getString("total_item") + " " + getResources().getString(R.string.items));
                        payment_method = order.getString("payment_method");
                        if (payment_method.equals("C")) {
                            payment_method_tv.setText(getResources().getString(R.string.cash_on_delivery));
                        } else {
                            payment_method_tv.setText(getResources().getString(R.string.online));
                        }

                        subtotal_price_tv.setText(getResources().getString(R.string.kd)+" " + order.getString("subtotal"));
                        total_descount_tv.setText(getResources().getString(R.string.kd)+" " + order.getString("total_discount"));
                        shi_charge_tv.setText(getResources().getString(R.string.kd)+" " + order.getString("shipping_price"));
                        total_price_tv.setText(getResources().getString(R.string.kd)+" " + order.getString("order_total"));
                        subTotal = Double.valueOf(order.getString("subtotal"));
                        ship_charge = Double.valueOf(order.getString("shipping_price"));
                        disc = Double.valueOf(order.getString("total_discount"));
                        if (PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getBoolean("is_logged_in", false)) {
                            Log.e("mmmmmmmmmmmmm", "total_itemtotal_itemtotal_item");
                            reedm_lay.setVisibility(View.VISIBLE);

                            String loyalty_balance = order.getJSONObject("customer_details").getString("loyalty_balance");
                            PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).edit().putString("loyalty_balance", loyalty_balance).commit();

                            rid_point_tv.setText(getResources().getString(R.string.you_have) + " " + String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(loyalty_balance)) + " " + getResources().getString(R.string.point_available));

                            Log.e("loydfghj", loyalty_balance);
                            balance = Double.valueOf(loyalty_balance);
                            Log.e("loy", loyalty_balance);
                            JSONObject settings = resultResponse.getJSONObject("settings");
                            double one_point_to_kwd = Double.valueOf(settings.getString("one_point_to_kwd"));
                            double one_kwd_to_point = Double.valueOf(settings.getString("one_kwd_to_point"));
                            double min_point = Double.valueOf(settings.getString("min_point"));
                            double max_discount = Double.valueOf(settings.getString("max_discount"));
                            double kwd_balance = balance * one_point_to_kwd;

                            if (max_discount < kwd_balance) {
                                discount_price = max_discount;
                            } else {
                                discount_price = kwd_balance;
                            }


                            total_descount_tv.setText(getResources().getString(R.string.kd)+" " + order.getString("total_discount"));




                            if (min_point <= balance) {
                                loya_dec_lay.setVisibility(View.VISIBLE);
                                double tot_pri = subTotal + ship_charge - discount_price - disc;
                                if(tot_pri <0){
                                    tot_pri = 0.000;
                                    balance = (subTotal+ship_charge-disc)*100;
                                    loyalty_point_descount_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", (subTotal+ship_charge-disc)));
                                }else{
                                    balance = discount_price*100;
                                    tot_pri = tot_pri;
                                    loyalty_point_descount_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", discount_price));
                                }
                                finalPayAmount = tot_pri;
                                loyalty_point = String.format(Locale.ENGLISH,"%.3f", balance);
                                total_descount_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", (discount_price+disc)));
                                total_price_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", tot_pri));


                                reed_yes_rb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                                    @Override
                                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                        if (isChecked) {
                                            final double finalDiscount_price = discount_price +disc;
                                            loya_dec_lay.setVisibility(View.VISIBLE);
                                            double tot_pri = subTotal + ship_charge - finalDiscount_price;
                                            if(tot_pri <0){
                                                tot_pri = 0.000;
                                                balance = (subTotal+ship_charge-disc)*100;
                                            }else{
                                                balance  = discount_price*100;
                                                tot_pri = tot_pri;
                                            }
                                            finalPayAmount = tot_pri;
                                            loyalty_point = String.format(Locale.ENGLISH,"%.3f", balance);
                                            total_descount_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", finalDiscount_price));
                                            total_price_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", tot_pri));
                                        } else {
                                            loya_dec_lay.setVisibility(View.GONE);
                                            double tot_pri = subTotal + ship_charge - disc;
                                            finalPayAmount = tot_pri;
                                            loyalty_point = "0.0000";
                                            total_descount_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", disc));
                                            total_price_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", tot_pri));
                                        }
                                    }
                                });
                            }else {
                                loya_dec_lay.setVisibility(View.GONE);
                                double tot_pri = subTotal + ship_charge - disc;
                                finalPayAmount = tot_pri;
                                loyalty_point = "0.0000";
                                total_descount_tv.setText(getResources().getString(R.string.kd)+" " + order.getString("total_discount"));
                                total_price_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", tot_pri));
                            }
                        } else {
                            reedm_lay.setVisibility(View.GONE);
                            loya_dec_lay.setVisibility(View.GONE);
                            double tot_pri = subTotal + ship_charge - disc;
                            finalPayAmount = tot_pri;
                            loyalty_point = "0.0000";
                            total_descount_tv.setText(getResources().getString(R.string.kd)+" " + order.getString("total_discount"));
                            total_price_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f", tot_pri));
                        }

                        ///////////////////////////////////////////////////////////////

                        String shipping_fname = order.getString("shipping_fname");
                        String shipping_lname = order.getString("shipping_lname");
                        if (shipping_lname.equals("") || shipping_lname.equals(null) || shipping_lname.equals("null")) {
                            shipping_lname = "";
                        }
                        String shipping_name = shipping_fname + " " + shipping_lname;
                        shi_name_tv.setText(shipping_name);
                        shi_email_tv.setText(order.getString("shipping_email"));
                        shi_phone_tv.setText(order.getString("shipping_phone"));

                        String shipping_city = order.getString("shipping_city");
                        String shipping_street = order.getString("shipping_street");
                        String shipping_block = order.getString("shipping_block");
                        if (shipping_block.equals("") || shipping_block.equals(null) || shipping_block.equals("null")) {
                            shipping_block = "";
                        }
                        String shipping_building = order.getString("shipping_building");
                        if (shipping_building.equals("") || shipping_building.equals(null) || shipping_building.equals("null")) {
                            shipping_building = "";
                        }

                        String country = order.getJSONObject("get_country").getString("name");
                         /*String postal_code = order.getString("postal_code");
                        if (postal_code.equals("") || postal_code.equals(null) || postal_code.equals("null")){
                            postal_code="";
                        }*/

                        String shipping_more_address = order.getString("shipping_more_address");
                        if (shipping_more_address.equals("") || shipping_more_address.equals(null) || shipping_more_address.equals("null")) {
                            shipping_more_address = "";
                        }
                        String address = shipping_city + ", " + shipping_street + ", " + shipping_block + ", " + shipping_building + ", " + shipping_more_address + ", " + country;
                        shi_address_tv.setText(address);

                        String billing_fname = order.getString("billing_fname");
                        String billing_lname = order.getString("billing_lname");
                        if (billing_lname.equals("") || billing_lname.equals(null) || billing_lname.equals("null")) {
                            billing_lname = "";
                        }
                        String billing_name = billing_fname + " " + billing_lname;
                        PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).edit().putString("billing_name",billing_name).commit();
                        bil_name_tv.setText(billing_name);
                        bil_email_tv.setText(order.getString("billing_email"));
                        PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).edit().putString("emailss",order.getString("billing_email")).commit();
                        bil_phone_tv.setText(order.getString("billing_phone"));
                        PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).edit().putString("mobss",order.getString("billing_phone")).commit();
                        String billing_city = order.getString("billing_city");
                        String billing_street = order.getString("billing_street");
                        String billing_block = order.getString("billing_block");
                        if (billing_block.equals("") || billing_block.equals(null) || billing_block.equals("null")) {
                            billing_block = "";
                        }
                        String billing_building = order.getString("billing_building");
                        if (billing_building.equals("") || billing_building.equals(null) || billing_building.equals("null")) {
                            billing_building = "";
                        }

                        //String billing_country = order.getJSONObject("get_country").getString("name");
                       /* String billpostal_code = order.getString("postal_code");
                        if (billpostal_code.equals("") || billpostal_code.equals(null) || billpostal_code.equals("null")){
                            billpostal_code="";
                        }
*/
                        String billing_more_address = order.getString("billing_more_address");
                        if (billing_more_address.equals("") || billing_more_address.equals(null) || billing_more_address.equals("null")) {
                            billing_more_address = "";
                        }
                        String bill_address = billing_city + ", " + billing_street + ", " + billing_block + ", " + billing_building + ", " + billing_more_address;
                        bil_address_tv.setText(bill_address);

                        JSONArray sellers = resultResponse.getJSONArray("sellers");
                        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
                        for (int i = 0; i < sellers.length(); i++) {
                            JSONObject jsonObject = sellers.getJSONObject(i);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID, jsonObject.getString("id"));
                            hashMap.put(SELLER_ID, jsonObject.getString("seller_id"));
                            JSONObject order_merchant = jsonObject.getJSONObject("order_merchant");
                            String fname = order_merchant.getString("fname");
                            String lname = order_merchant.getString("lname");
                            String image = order_merchant.getString("image");
                            if (lname.equals("") || lname.equals(null) || lname.equals("null")) {
                                lname = "";
                            }
                            hashMap.put(SELLER_NAME, fname + " " + lname);
                            //hashMap.put(IMAGE,image);
                            hashMap.put(ORDER_DETAILS, jsonObject.getString("order_details"));
                            arrayList.add(hashMap);
                        }
                        PlaceOrderAdapter placeOrderAdapter = new PlaceOrderAdapter(PlaceOrderActivity.this, arrayList);
                        recy_view.setAdapter(placeOrderAdapter);
                        recy_view.setFocusable(false);
                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (Exception e) {
                    MethodClass.error_alert(PlaceOrderActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(PlaceOrderActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(PlaceOrderActivity.this);
                } else {
                    MethodClass.error_alert(PlaceOrderActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(PlaceOrderActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getString("token", ""));
                } else {
                    headers.put("Authorization", "Bearer " + CheckOutActivity.token);
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(PlaceOrderActivity.this).addToRequestQueue(jsonObjectRequest);
    }


    public void place_order() {
        if (!isNetworkConnected(PlaceOrderActivity.this)) {
            MethodClass.network_error_alert(PlaceOrderActivity.this);
            return;
        }
        MethodClass.showProgressDialog(PlaceOrderActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "place-order";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if (!PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getBoolean("is_logged_in", false)) {
            hashMap.put("device_id", android_id);
        }
        hashMap.put("order_id", order_id);
        hashMap.put("loyalty_point", loyalty_point);
        hashMap.put("coupon_code", couponText.getText().toString().trim());
        hashMap.put("reward_point", reed_yes_rb.isChecked() ? "Y" : "N");
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(PlaceOrderActivity.this);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(PlaceOrderActivity.this, response);
                    if(resultResponse.has("status") && (resultResponse.getString("status").equals("OUT_OF_STOCK"))){
                        new SweetAlertDialog(PlaceOrderActivity.this, SweetAlertDialog.WARNING_TYPE)
                                .setTitleText("Out of stock")
                                .setContentText(resultResponse.getString("message"))
                                .setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                Intent intent = new Intent(PlaceOrderActivity.this, ShoppingCartActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }
                        }).show();
                    }else {
                        String id = resultResponse.getJSONObject("order").getString("id");
                        String order_no = resultResponse.getJSONObject("order").getString("order_no");
                        String pay_amount = resultResponse.getJSONObject("order").getString("order_total");
                        if (payment_method.equals("C")) {
                            new SweetAlertDialog(PlaceOrderActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Order Placed!")
                                    .setContentText("Thank you for your purchase! Your order has now been received, and you will shortly receive a confirmation email containing details of your order. If you have any questions, please contact us and we will be happy to assist you.Your Order Number : "+order_no)
                                    .setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    if (PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getBoolean("is_logged_in", false)) {
                                        Intent intent = new Intent(PlaceOrderActivity.this, OrderHistoryActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    } else {
                                        Intent intent = new Intent(PlaceOrderActivity.this, HomeActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                    }
                                }
                            }).show();

                        } else {
                            Intent intent = new Intent(PlaceOrderActivity.this, PayNowActivity.class);
                            intent.putExtra("payable_amount", pay_amount);
                            intent.putExtra("id", id);
                            intent.putExtra("order_no", order_no);
                            startActivity(intent);
                        }
                    }

                } catch (Exception e) {
                    MethodClass.error_alert(PlaceOrderActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(PlaceOrderActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(PlaceOrderActivity.this);
                } else {
                    MethodClass.error_alert(PlaceOrderActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(PlaceOrderActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(PlaceOrderActivity.this).getString("token", ""));
                } else {
                    headers.put("Authorization", "Bearer " + CheckOutActivity.token);
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(PlaceOrderActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
