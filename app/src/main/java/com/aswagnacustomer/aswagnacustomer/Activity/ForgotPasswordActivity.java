package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText email_et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());//here initialize Facebook Sdk
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_forgot_password);
        email_et = findViewById(R.id.email_et);

    }

    public void sendOTP(View view) {
        String get_email = "";
        get_email = email_et.getText().toString().trim();

        if (get_email.length() == 0) {
            email_et.setError(getResources().getString(R.string.please_enter_email_address));
            email_et.requestFocus();
            return;
        }

        if (!isNetworkConnected(ForgotPasswordActivity.this)) {
            MethodClass.network_error_alert(ForgotPasswordActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ForgotPasswordActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "forgot-pass-user";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("email", get_email);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        final String finalGet_email = get_email;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ForgotPasswordActivity.this);
                Log.e("respLogin", response.toString());
                try {
                    JSONObject resultResponce = MethodClass.get_result_from_webservice(ForgotPasswordActivity.this, response);
                    if (resultResponce != null) {
                        new SweetAlertDialog(ForgotPasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(resultResponce.getString("message"))
                                .setContentText(resultResponce.getString("meaning"))
                                .setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                try {
                                    String code = resultResponce.getString("vcode");
                                    Intent intent = new Intent(ForgotPasswordActivity.this, VerifyActivity.class);
                                    intent.putExtra("email", finalGet_email);
                                    intent.putExtra("code", code);
                                    intent.putExtra("type", "F");
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                            }
                        }).show();
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ForgotPasswordActivity.this);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(ForgotPasswordActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ForgotPasswordActivity.this);
                } else {
                    MethodClass.error_alert(ForgotPasswordActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(ForgotPasswordActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ForgotPasswordActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ForgotPasswordActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
