package com.aswagnacustomer.aswagnacustomer.Activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Adapter.FAQAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ANS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.QUESTION;

public class FAQActivity extends AppCompatActivity {

    private RecyclerView recy_view;
    private ArrayList<HashMap<String,String>> arrayList ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_faq);
        recy_view=findViewById(R.id.recy_view);
        topList();
    }

    private void topList(){
        arrayList=new ArrayList<>();
        String data=getIntent().getStringExtra("data");
        try {
            JSONArray jsonArray=new JSONArray(data);
            for (int i = 0; i < jsonArray.length(); i++) {
                HashMap<String,String>  hashMap=new HashMap<>();
                hashMap.put(QUESTION,jsonArray.getJSONObject(i).getJSONObject("faq_details_by_language").getString("faq_ques"));
                hashMap.put(ANS,jsonArray.getJSONObject(i).getJSONObject("faq_details_by_language").getString("faq_answer"));
                arrayList.add(hashMap);
            }
            FAQAdapter orderHistoryAdapter=new FAQAdapter(FAQActivity.this,arrayList);
            recy_view.setAdapter(orderHistoryAdapter);
            recy_view.setFocusable(false);
        }catch (JSONException e){
            Log.e("JSONException",e.toString());
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(FAQActivity.this);
        MethodClass.setMenuColor(FAQActivity.this,"no");
    }

    public void back(View view) {
        super.onBackPressed();
    }
}
