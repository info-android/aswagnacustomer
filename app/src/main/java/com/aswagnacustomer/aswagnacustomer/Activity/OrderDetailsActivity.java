package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.OrderDetailsAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COMMENTS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CURR;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CURRENCY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_DETAILS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IDS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_IDSS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_VARIANT_IDS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.RATINGS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.REVIEW_DONE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SELLER_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SELLER_NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.STATUS;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class OrderDetailsActivity extends AppCompatActivity {

    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> arrayList;
    private TextView order_no_tv, order_on_date_tv, item_no_tv, payment_method_tv, total_order_tv, status_tv;
    private TextView shipping_name_tv, shipping_gmail_tv, shipping_phone_no_tv, shipping_full_address_tv;
    private TextView billing_name_tv, billing_email_tv, billing_phone_tv, billing_full_address_tv;
    String id = "";

    String is_review_done;
    private NestedScrollView scrollView;
    private Button submitReview;
    private String total_item;
    private LinearLayout coupon_dec_lay;
    private TextView coupon_order_tv;

    private TextView loyalty_rec_order_tv,loyalty_used_order_tv,loyalty_point_descount_tv,total_dis_tv,subtotal_order_tv,ship_order_tv;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_order_details);
        recy_view = findViewById(R.id.recy_view);
        order_no_tv = findViewById(R.id.order_no_tv);
        order_on_date_tv = findViewById(R.id.order_on_date_tv);
        item_no_tv = findViewById(R.id.item_no_tv);
        payment_method_tv = findViewById(R.id.payment_method_tv);
        total_order_tv = findViewById(R.id.total_order_tv);
        shipping_name_tv = findViewById(R.id.shipping_name_tv);
        shipping_gmail_tv = findViewById(R.id.shipping_gmail_tv);
        shipping_phone_no_tv = findViewById(R.id.shipping_phone_no_tv);
        shipping_full_address_tv = findViewById(R.id.shipping_full_address_tv);
        billing_name_tv = findViewById(R.id.billing_name_tv);
        billing_email_tv = findViewById(R.id.billing_email_tv);
        billing_phone_tv = findViewById(R.id.billing_phone_tv);
        billing_full_address_tv = findViewById(R.id.billing_full_address_tv);
        status_tv = findViewById(R.id.status_tv);
        scrollView = findViewById(R.id.scrollView);
        submitReview = findViewById(R.id.submitReview);
        coupon_dec_lay = findViewById(R.id.coupon_dec_lay);
        coupon_order_tv = findViewById(R.id.coupon_order_tv);

        loyalty_rec_order_tv = findViewById(R.id.loyalty_rec_order_tv);
        loyalty_used_order_tv = findViewById(R.id.loyalty_used_order_tv);
        loyalty_point_descount_tv = findViewById(R.id.loyalty_point_descount_tv);
        total_dis_tv = findViewById(R.id.total_dis_tv);
        subtotal_order_tv = findViewById(R.id.subtotal_order_tv);
        ship_order_tv = findViewById(R.id.ship_order_tv);

        id = getIntent().getStringExtra("id");
        order_historyDetails();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(OrderDetailsActivity.this);
        MethodClass.setMenuColor(OrderDetailsActivity.this, "no");
    }

    public void back(View view) {
        super.onBackPressed();
    }

    private void order_historyDetails() {
        if (!isNetworkConnected(OrderDetailsActivity.this)) {
            MethodClass.network_error_alert(OrderDetailsActivity.this);
            return;
        }
        MethodClass.showProgressDialog(OrderDetailsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "order-history-details/" + id;
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(OrderDetailsActivity.this);
                Log.e("order_historyDetails", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(OrderDetailsActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject order = resultResponse.getJSONObject("order");
                        order_no_tv.setText(order.getString("order_no"));
                        order_on_date_tv.setText(order.getString("created_at"));
                        item_no_tv.setText(resultResponse.getString("no_of_items"));
                        total_item = resultResponse.getString("no_of_items");
                        if(!order.getString("coupon_code").equals(null) && !order.getString("coupon_code").equals("null")){
                            coupon_dec_lay.setVisibility(View.VISIBLE);
                            coupon_order_tv.setText(CURRENCY+" "+String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(order.getString("coupon_discount")))+" ("+order.getString("coupon_code")+")");
                        }

                        RATINGS = new String[Integer.parseInt(total_item)];
                        COMMENTS = new String[Integer.parseInt(total_item)];
                        PRODUCT_IDS = new int[Integer.parseInt(total_item)];
                        PRODUCT_VARIANT_IDS = new String[Integer.parseInt(total_item)];
                        PRODUCT_IDSS = new String[Integer.parseInt(total_item)];

                        String payment_method = order.getString("payment_method");
                        if (payment_method.equals("O")) {
                            payment_method_tv.setText(getResources().getString(R.string.online));
                        } else if (payment_method.equals("C")) {
                            payment_method_tv.setText(getResources().getString(R.string.cash_on_delivery));
                        }

                        String status = order.getString("status");
                        if (status.equals("N")) {
                            status_tv.setText(getResources().getString(R.string.new_order));
                        } else if (status.equals("OA")) {
                            status_tv.setText(getResources().getString(R.string.accepted));
                        } else if (status.equals("DA")) {
                            status_tv.setText(getResources().getString(R.string.assigned));
                        } else if (status.equals("RP")) {
                            status_tv.setText(getResources().getString(R.string.ready_to_pick_up));
                        } else if (status.equals("OP")) {
                            status_tv.setText(getResources().getString(R.string.picked_up));
                        } else if (status.equals("OD")) {
                            status_tv.setText(getResources().getString(R.string.delivered));
                        } else if (status.equals("OC")) {
                            status_tv.setText(getResources().getString(R.string.cancelled));
                        } else if (status.equals("I")) {
                            status_tv.setText(getResources().getString(R.string.incomplete));
                        } else if (status.equals("F")) {
                            status_tv.setText(getResources().getString(R.string.payment_failed));
                        }else if (status.equals("PP")) {
                            status_tv.setText(getResources().getString(R.string.driver_assign));
                        }else if (status.equals("PC")) {
                            status_tv.setText(getResources().getString(R.string.driver_assign));
                        }
                        total_order_tv.setText(CURRENCY+" "+String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(order.getString("order_total"))));
                        loyalty_rec_order_tv.setText(order.getString("loyalty_point_received"));
                        loyalty_used_order_tv.setText(order.getString("loyalty_point_used"));
                        loyalty_point_descount_tv.setText(": "+CURRENCY+" "+String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(order.getString("loyalty_amount"))));
                        total_dis_tv.setText(": "+CURRENCY+" "+String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(order.getString("total_discount"))));
                        subtotal_order_tv.setText(": "+CURRENCY+" "+String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(order.getString("subtotal"))));
                        ship_order_tv.setText(": "+CURRENCY+" "+String.format(Locale.ENGLISH,"%.3f", Double.parseDouble(order.getString("shipping_price"))));


                        is_review_done = order.getString("is_review_done");


                        if(status.equals("OD") && is_review_done.equals("N")){
                            submitReview.setVisibility(View.VISIBLE);
                            submitReview.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    postRev();
                                }
                            });
                        }else {
                            submitReview.setVisibility(View.GONE);
                        }

                        ///////////////////////////////////////////////////////////////

                        String shipping_fname = order.getString("shipping_fname");
                        String shipping_lname = order.getString("shipping_lname");
                        if (shipping_lname.equals("") || shipping_lname.equals(null) || shipping_lname.equals("null")){
                            shipping_lname="";
                        }
                        String shipping_name = shipping_fname +" "+shipping_lname;
                        shipping_name_tv.setText(shipping_name);
                        shipping_gmail_tv.setText(order.getString("shipping_email"));
                        shipping_phone_no_tv.setText(order.getString("shipping_phone"));
                        String shipping_city = order.getString("shipping_city");
                        String shipping_street = order.getString("shipping_street");
                        String shipping_block = order.getString("shipping_block");
                        if (shipping_block.equals("") || shipping_block.equals(null) || shipping_block.equals("null")) {
                            shipping_block = "";
                        }
                        String shipping_building = order.getString("shipping_building");
                        if (shipping_building.equals("") || shipping_building.equals(null) || shipping_building.equals("null")) {
                            shipping_building = "";
                        }

                        String country = order.getJSONObject("get_country").getString("name");
                         /*String postal_code = order.getString("postal_code");
                        if (postal_code.equals("") || postal_code.equals(null) || postal_code.equals("null")){
                            postal_code="";
                        }*/

                        String shipping_more_address = order.getString("shipping_more_address");
                        if (shipping_more_address.equals("") || shipping_more_address.equals(null) || shipping_more_address.equals("null")) {
                            shipping_more_address = "";
                        }
                        String address = shipping_city + ", " + shipping_street + ", " + shipping_block + ", " + shipping_building + ", " + shipping_more_address + ", " + country;
                        shipping_full_address_tv.setText(address);


                        String billing_fname = order.getString("billing_fname");
                        String billing_lname = order.getString("billing_lname");
                        if (billing_lname.equals("") || billing_lname.equals(null) || billing_lname.equals("null")){
                            billing_lname="";
                        }
                        String billing_name = billing_fname+" "+billing_lname;
                        billing_name_tv.setText(billing_name);

                        billing_email_tv.setText(order.getString("billing_email"));
                        billing_phone_tv.setText(order.getString("billing_phone"));

                        String billing_city = order.getString("billing_city");
                        String billing_street = order.getString("billing_street");
                        String billing_block = order.getString("billing_block");
                        if (billing_block.equals("") || billing_block.equals(null) || billing_block.equals("null")) {
                            billing_block = "";
                        }
                        String billing_building = order.getString("billing_building");
                        if (billing_building.equals("") || billing_building.equals(null) || billing_building.equals("null")) {
                            billing_building = "";
                        }

                        //String billing_country = order.getJSONObject("get_country").getString("name");
                       /* String billpostal_code = order.getString("postal_code");
                        if (billpostal_code.equals("") || billpostal_code.equals(null) || billpostal_code.equals("null")){
                            billpostal_code="";
                        }
*/
                        String billing_more_address = order.getString("billing_more_address");
                        if (billing_more_address.equals("") || billing_more_address.equals(null) || billing_more_address.equals("null")) {
                            billing_more_address = "";
                        }
                        String bill_address = billing_city + ", " + billing_street + ", " + billing_block + ", " + billing_building + ", " + billing_more_address;
                        billing_full_address_tv.setText(bill_address);

                        JSONArray seller = resultResponse.getJSONArray("seller");
                        arrayList = new ArrayList<>();
                        for (int i = 0; i < seller.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID, seller.getJSONObject(i).getString("id"));
                            hashMap.put(SELLER_ID, seller.getJSONObject(i).getString("seller_id"));
                            String fname = seller.getJSONObject(i).getJSONObject("order_merchant").getString("fname");
                            String lname = seller.getJSONObject(i).getJSONObject("order_merchant").getString("lname");
                            String image = seller.getJSONObject(i).getJSONObject("order_merchant").getString("image");
                            if (lname.equals("") || lname.equals(null) || lname.equals("null")){
                                lname = "";
                            }
                            hashMap.put(SELLER_NAME, fname + " " + lname);
                            hashMap.put(IMAGE, image);
                            hashMap.put(ORDER_DETAILS, seller.getJSONObject(i).getString("order_details"));
                            hashMap.put(REVIEW_DONE, is_review_done);
                            hashMap.put(STATUS, status);
                            arrayList.add(hashMap);
                        }
                        OrderDetailsAdapter orderDetailsAdapter = new OrderDetailsAdapter(OrderDetailsActivity.this, arrayList);
                        recy_view.setAdapter(orderDetailsAdapter);
                        recy_view.setFocusable(false);
                        scrollView.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(OrderDetailsActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(OrderDetailsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(OrderDetailsActivity.this);
                } else {
                    MethodClass.error_alert(OrderDetailsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(OrderDetailsActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(OrderDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void postRev(){
        if(Integer.parseInt(total_item)>0){
            for (int i = 0; i <Integer.parseInt(total_item) ; i++) {
                Log.e("Comment", COMMENTS[i] );
                if(RATINGS[i].isEmpty()){
                    Toast.makeText(OrderDetailsActivity.this, getString(R.string.enterRating), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(COMMENTS[i].isEmpty()){
                    Toast.makeText(OrderDetailsActivity.this, getString(R.string.enterCom), Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        }
        if (!isNetworkConnected(OrderDetailsActivity.this)) {
            MethodClass.network_error_alert(OrderDetailsActivity.this);
            return;
        }
        MethodClass.showProgressDialog(OrderDetailsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "post-review";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        hashMap.put("order_master_id", id);
        hashMap.put("total", total_item);
        hashMap.put("reward_point", TextUtils.join(",",RATINGS));
        hashMap.put("comment", TextUtils.join(",",COMMENTS));
        hashMap.put("product_id", TextUtils.join(",",PRODUCT_IDSS));
        hashMap.put("product_variant_id", TextUtils.join(",",PRODUCT_VARIANT_IDS));
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(OrderDetailsActivity.this);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(OrderDetailsActivity.this, response);
                   if(resultResponse !=null){
                       new SweetAlertDialog(OrderDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                               .setTitleText(getString(R.string.reviewDone))
                               .setContentText(getString(R.string.reviewCom))
                               .setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                           @Override
                           public void onClick(SweetAlertDialog sDialog) {
                               sDialog.dismissWithAnimation();
                                order_historyDetails();
                           }
                       }).show();
                   }

                } catch (Exception e) {
                    MethodClass.error_alert(OrderDetailsActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(OrderDetailsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(OrderDetailsActivity.this);
                } else {
                    MethodClass.error_alert(OrderDetailsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(OrderDetailsActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(OrderDetailsActivity.this).getString("token", ""));
                } else {
                    headers.put("Authorization", "Bearer " + CheckOutActivity.token);
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(OrderDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }
}
