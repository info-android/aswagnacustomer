package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.MarchandProductAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.MarchandProfileCatAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.MarchandReviewAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.ProductDetailsAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.skyhope.showmoretextview.ShowMoreTextView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COMMENT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FNAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FROM_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LNAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.RATING;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SElLER_COVER_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SElLER_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TO_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class MarchandProfileActivity extends AppCompatActivity {
    private RecyclerView category_recyclerView;
    private ArrayList<HashMap<String, String>> arrayList;
    private ShowMoreTextView about_comp_tv;
    private CircleImageView seller_pic;
    private TextView compny_name_tv, address_tv, total_review_tv, since_date_tv, order_process_tv, no_of_product_tv;
    private String id = "";
    private RatingBar rating;
    private LinearLayout about_com_lin_text, review_lin_text, product_seller_lin_text;
    private LinearLayout about_com_lay, review_lay, seller_product_lay;
    private RecyclerView product_sell_recy, review_sell_recy;

    private LinearLayout product_lin, review_lin, about_lin;
    private TextView product_tv, review_tv, about_tv;
    private LinearLayout share_lin,main_lay,orderLay;
    private ImageView coverPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_merchand_profile);
        category_recyclerView = findViewById(R.id.category_recyclerView);
        compny_name_tv = findViewById(R.id.compny_name_tv);
        address_tv = findViewById(R.id.address_tv);
        total_review_tv = findViewById(R.id.total_review_tv);
        rating = findViewById(R.id.rating);
        since_date_tv = findViewById(R.id.since_date_tv);
        order_process_tv = findViewById(R.id.order_process_tv);
        no_of_product_tv = findViewById(R.id.no_of_product_tv);
        seller_pic = findViewById(R.id.seller_pic);
        product_sell_recy = findViewById(R.id.product_sell_recy);
        review_sell_recy = findViewById(R.id.review_sell_recy);

        product_lin = findViewById(R.id.product_lin);
        review_lin = findViewById(R.id.review_lin);
        about_lin = findViewById(R.id.about_lin);
        product_tv = findViewById(R.id.product_tv);
        review_tv = findViewById(R.id.review_tv);
        about_tv = findViewById(R.id.about_tv);
        share_lin = findViewById(R.id.share_lin);
        main_lay = findViewById(R.id.main_lay);
        orderLay = findViewById(R.id.orderLay);
        coverPhoto = findViewById(R.id.coverPhoto);

        id = getIntent().getStringExtra("id");

        about_comp_tv = findViewById(R.id.about_comp_tv);
        about_comp_tv.setShowingLine(4);
        about_comp_tv.addShowMoreText("Read More");
        about_comp_tv.addShowLessText("Read Less");
        about_comp_tv.setShowMoreColor(getResources().getColor(R.color.red)); // or other color
        about_comp_tv.setShowLessTextColor(getResources().getColor(R.color.red)); // or other color

        about_com_lin_text = findViewById(R.id.about_com_lin_text);
        review_lin_text = findViewById(R.id.review_lin_text);
        product_seller_lin_text = findViewById(R.id.product_seller_lin_text);
        about_com_lay = findViewById(R.id.about_com_lay);
        review_lay = findViewById(R.id.review_lay);
        seller_product_lay = findViewById(R.id.seller_product_lay);
        about_com_lin_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product_lin.setBackgroundColor(getResources().getColor(R.color.textMidGreyColor));
                review_lin.setBackgroundColor(getResources().getColor(R.color.textMidGreyColor));
                about_lin.setBackgroundColor(getResources().getColor(R.color.red));

                product_tv.setTextColor(getResources().getColor(R.color.textMidGreyColor));
                review_tv.setTextColor(getResources().getColor(R.color.textMidGreyColor));
                about_tv.setTextColor(getResources().getColor(R.color.red));

                about_com_lay.setVisibility(View.VISIBLE);
                review_lay.setVisibility(View.GONE);
                seller_product_lay.setVisibility(View.GONE);
            }
        });
        review_lin_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                product_lin.setBackgroundColor(getResources().getColor(R.color.textMidGreyColor));
                review_lin.setBackgroundColor(getResources().getColor(R.color.red));
                about_lin.setBackgroundColor(getResources().getColor(R.color.textMidGreyColor));

                product_tv.setTextColor(getResources().getColor(R.color.textMidGreyColor));
                review_tv.setTextColor(getResources().getColor(R.color.red));
                about_tv.setTextColor(getResources().getColor(R.color.textMidGreyColor));
                about_com_lay.setVisibility(View.GONE);
                review_lay.setVisibility(View.VISIBLE);
                seller_product_lay.setVisibility(View.GONE);
            }
        });
        product_seller_lin_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                product_lin.setBackgroundColor(getResources().getColor(R.color.red));
                review_lin.setBackgroundColor(getResources().getColor(R.color.textMidGreyColor));
                about_lin.setBackgroundColor(getResources().getColor(R.color.textMidGreyColor));

                product_tv.setTextColor(getResources().getColor(R.color.red));
                review_tv.setTextColor(getResources().getColor(R.color.textMidGreyColor));
                about_tv.setTextColor(getResources().getColor(R.color.textMidGreyColor));
                about_com_lay.setVisibility(View.GONE);
                review_lay.setVisibility(View.GONE);
                seller_product_lay.setVisibility(View.VISIBLE);
            }
        });

        productDetails();



    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(MarchandProfileActivity.this);
        MethodClass.setMenuColor(MarchandProfileActivity.this,"other");
    }


    private void productDetails() {
        if (!isNetworkConnected(MarchandProfileActivity.this)) {
            MethodClass.network_error_alert(MarchandProfileActivity.this);
            return;
        }
        MethodClass.showProgressDialog(MarchandProfileActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "merchant-details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("seller_id", id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("merchant-details: ", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("merchant-detailsrespo", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(MarchandProfileActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject merchant = resultResponse.getJSONObject("merchant");
                        JSONObject merchant_details = merchant.getJSONObject("merchant_details");

                        String slugss = merchant_details.getString("slug");

                        share_lin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                //sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, "Check out our andorid app: https://www.aswagna.co/api/share?content="+getIntent().getStringExtra("id")+"&type=M"+ "\n\n" +"Checkout our Ios app: https://www.aswagna.co/merchant-profile/"+slugss);
                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                            }
                        });


                        String image = merchant_details.getString("image");
                        String cover_pic = merchant_details.getString("cover_pic");
                        Picasso.get().load(SElLER_IMAGE_URL + image).error(R.drawable.person_icon).placeholder(R.drawable.person_icon).into(seller_pic);
                        Picasso.get().load(SElLER_COVER_URL + cover_pic).error(R.drawable.applogo).placeholder(R.drawable.applogo).into(coverPhoto);
                        String company_name = merchant_details.getString("company_name");
                        compny_name_tv.setText(company_name);
                        String city = merchant_details.getString("city");
                        String Country_name = merchant_details.getJSONObject("country").getString("name");
                        if (city.equals("null") || city.equals("")){
                            address_tv.setText(Country_name);
                        }else {
                            address_tv.setText(Country_name);
                        }
                        String rate = merchant_details.getString("rate");
                        String total_no_review = merchant_details.getString("total_no_review");
                        String show_order_processed = merchant_details.getString("show_order_processed");

                        if(show_order_processed.equals("Y")){
                            String order_processed = merchant.getString("order_processed");
                            order_process_tv.setText(order_processed);
                            orderLay.setVisibility(View.VISIBLE);
                        }else {
                            orderLay.setVisibility(View.GONE);
                        }
                        rating.setRating(Float.parseFloat(rate));
                        total_review_tv.setText("(" + total_no_review + " " + getResources().getString(R.string.reviews) + ")");
                        if (!merchant_details.getString("merchant_company_details_by_language").equals("null") && !merchant_details.getString("merchant_company_details_by_language").equals(null)) {
                            String aboutDescription = merchant_details.getJSONObject("merchant_company_details_by_language").getString("description");
                            about_comp_tv.setText(Html.fromHtml(aboutDescription));
                        }
                        String created_at = merchant_details.getString("created_at");
                        since_date_tv.setText(created_at);

                        String total_products = merchant.getString("total_products");
                        no_of_product_tv.setText(total_products);

                        String categories = merchant.getString("categories");
                        if (!categories.equals("") && !categories.equals("null")) {
                            JSONArray categoriesArray = new JSONArray(categories);
                            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
                            for (int i = 0; i < categoriesArray.length(); i++) {
                                String id = categoriesArray.getJSONObject(i).getJSONObject("category").getString("id");
                                String slug = categoriesArray.getJSONObject(i).getJSONObject("category").getString("slug");
                                String picture = categoriesArray.getJSONObject(i).getJSONObject("category").getString("picture");
                                String title = categoriesArray.getJSONObject(i).getJSONObject("category").getJSONObject("category_by_language").getString("title");
                                HashMap<String, String> subCatHashMap1 = new HashMap<>();
                                subCatHashMap1.put(ID, id);
                                subCatHashMap1.put(SLUG, slug);
                                subCatHashMap1.put(TITLE, title);
                                subCatHashMap1.put(IMAGE, picture);
                                arrayList.add(subCatHashMap1);
                            }
                            MarchandProfileCatAdapter catAdapter = new MarchandProfileCatAdapter(MarchandProfileActivity.this, arrayList);
                            category_recyclerView.setAdapter(catAdapter);
                            category_recyclerView.setFocusable(false);
                        }

                        //////////////////////////////////////////////////////////////

                        JSONArray products = merchant.getJSONArray("merchant_products");
                        ArrayList<HashMap<String, String>> productArrayList = new ArrayList<>();
                        for (int i = 0; i < products.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID, products.getJSONObject(i).getString("id"));
                            hashMap.put(PRICE, products.getJSONObject(i).getString("price"));
                            hashMap.put(FROM_DATE, products.getJSONObject(i).getString("from_date"));
                            hashMap.put(TO_DATE, products.getJSONObject(i).getString("to_date"));
                            hashMap.put(DISCOUNT_PRICE, products.getJSONObject(i).getString("discount_price"));
                            if (!products.getJSONObject(i).getString("default_image").equals("null")) {
                                hashMap.put(IMAGE, products.getJSONObject(i).getJSONObject("default_image").getString("image"));
                            }
                            hashMap.put(TITLE, products.getJSONObject(i).getJSONObject("product_by_language").getString("title"));
                            productArrayList.add(hashMap);
                        }

                        MarchandProductAdapter productAdapter = new MarchandProductAdapter(MarchandProfileActivity.this, productArrayList);
                        productAdapter.setHasStableIds(true);
                        product_sell_recy.setAdapter(productAdapter);
                        product_sell_recy.setFocusable(false);

                        //////////////////////////////////////////////////////////////////////////////

                        JSONArray reviews = merchant.getJSONArray("reviews");
                        ArrayList<HashMap<String, String>> reviewsArrayList = new ArrayList<>();
                        for (int i = 0; i < reviews.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID, reviews.getJSONObject(i).getString("id"));
                            hashMap.put(RATING, reviews.getJSONObject(i).getString("rate"));
                            hashMap.put(DATE, reviews.getJSONObject(i).getString("review_date"));
                            hashMap.put(COMMENT, reviews.getJSONObject(i).getString("comment"));
                            hashMap.put(FNAME, reviews.getJSONObject(i).getJSONObject("order_master").getJSONObject("get_user_details").getString("fname"));
                            hashMap.put(LNAME, reviews.getJSONObject(i).getJSONObject("order_master").getJSONObject("get_user_details").getString("lname"));
                            hashMap.put(IMAGE, reviews.getJSONObject(i).getJSONObject("order_master").getJSONObject("get_user_details").getString("image"));
                            reviewsArrayList.add(hashMap);
                        }
                        MarchandReviewAdapter reviewAdapter = new MarchandReviewAdapter(MarchandProfileActivity.this, reviewsArrayList);
                        review_sell_recy.setAdapter(reviewAdapter);
                        review_sell_recy.setFocusable(false);


                    }
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            MethodClass.hideProgressDialog(MarchandProfileActivity.this);
                            main_lay.setVisibility(View.VISIBLE);

                        }},5000);

                } catch (JSONException e) {
                    MethodClass.hideProgressDialog(MarchandProfileActivity.this);
                    MethodClass.error_alert(MarchandProfileActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(MarchandProfileActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(MarchandProfileActivity.this);
                } else {
                    MethodClass.error_alert(MarchandProfileActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization",MethodClass.check_locale_lang(MarchandProfileActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MarchandProfileActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(MarchandProfileActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
