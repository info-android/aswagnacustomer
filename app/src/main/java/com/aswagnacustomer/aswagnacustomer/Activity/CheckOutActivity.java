package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.CheckOutAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.CheckoutBillingAddressAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.CheckoutshippingAddressAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ADDRESS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COUNTRY_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.EMAIL_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IS_DEFAULT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PHONE_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCTS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.USER_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class CheckOutActivity extends AppCompatActivity {
    private RecyclerView recy_view;
    private TextView total_price_tv, total_items_tv;
    private LinearLayout personal_tab_lin, shipping_tab_lin, billing_tab_lin;
    private TextView personal_tab_text, shipping_tab_text, billing_tab_text;
    private LinearLayout personal_tab_line, shipping_tab_line, billing_tab_line;
    private CardView personal_Layout, shipping_layout, billing_layout;
    private LinearLayout shipping_linear, billing_linear;
    private RecyclerView shiping_address_recy, billing_address_recy;
    private EditText personal_first_name_et, personal_last_name_et, personal_email_et;
    private Button personal_save_btn;
    //////////////////////////////////////////////////////////////////
    private RadioButton shi_create_new_address, shi_user_saved_address,yes,no;
    private RadioGroup shi_radio;
    private RadioGroup add_radio;
    private EditText shi_first_name_et, shi_last_name_et, shi_phone_et, shi_email_et, shi_city_et, shi_block_et, shi_street_et, shi_building_et, shi_more_address_et, shi_postal_code_et;
    private Spinner shi_spin_country, shi_city_spin;
    EditText shi_location_placeText;
    private Button shi_save_btn;
    private String shi_country_id = "", shi_country_str = "", shi_city_id = "", shi_city_str = "";
    //////////////////////////////////////////////////////////////////
    private RadioButton bil_create_new_address, bil_user_saved_address;
    private RadioGroup bil_radio;
    private EditText bil_first_name_et, bil_last_name_et, bil_phone_et, bil_email_et, bil_city_et, bil_block_et, bil_street_et, bil_building_et, bil_more_address_et, bil_postal_code_et;
    private Spinner bil_spin_country, bil_city_spin;
    private String bil_country_id = "", bil_country_str = "", bil_city_id = "", bil_city_str = "";
    //////////////////////////////////////////////////////////////////
    private RadioButton cashOnDelevery_rb, online_rb;
    private CheckBox same_add_checkbox;
    String type = "";
    String save_Add = "Y";
    public String shi_sel_add_id = "", bil_sel_add_id = "";
    public String sel_coun_id = "";
    public String shi_lat = "", shi_long = "";
    private Button continue_place_order_btn;
    private boolean per_checkData = false, shi_checkData = false, bil_checkData = false;

    public static String order_id = "",token="";
    private AutoCompleteTextView search_edt,bill_search_edt;

    public ArrayList<MethodClass.StringWithTag2> countriesArrayList;
    public ArrayList<MethodClass.StringWithTag> cityArrayList;

    private EditText bil_country_code,shi_country_code;
    private NestedScrollView scrollView;
    private Boolean onLi = false,CoD = false;

    private String fname,lname,mob,email;
    private LinearLayout saveAdd;
    String merchantOutsideKuwait = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_checkout);

        fname = PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getString("user_fname", "");
        lname = PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getString("user_lname", "");
        mob = PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getString("mobile", "");
        email = PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getString("email", "");

        order_id = "";
        recy_view = findViewById(R.id.recy_view);
        total_price_tv = findViewById(R.id.total_price_tv);
        total_items_tv = findViewById(R.id.total_items_tv);
        search_edt = findViewById(R.id.search_edt);
        bill_search_edt = findViewById(R.id.bill_search_edt);
        cashOnDelevery_rb = findViewById(R.id.cashOnDelevery_rb);
        online_rb = findViewById(R.id.online_rb);
        same_add_checkbox = findViewById(R.id.same_add_checkbox);
        continue_place_order_btn = findViewById(R.id.continue_place_order_btn);
        saveAdd = findViewById(R.id.saveAdd);
        //////////////////////////////////////////////////
        personal_tab_lin = findViewById(R.id.personal_tab_lin);
        shipping_tab_lin = findViewById(R.id.shipping_tab_lin);
        billing_tab_lin = findViewById(R.id.billing_tab_lin);

        personal_tab_text = findViewById(R.id.personal_tab_text);
        shipping_tab_text = findViewById(R.id.shipping_tab_text);
        billing_tab_text = findViewById(R.id.billing_tab_text);

        personal_tab_line = findViewById(R.id.personal_tab_line);
        shipping_tab_line = findViewById(R.id.shipping_tab_line);
        billing_tab_line = findViewById(R.id.billing_tab_line);
        //////////////////////////////////////////////////
        personal_Layout = findViewById(R.id.personal_Layout);
        shipping_layout = findViewById(R.id.shipping_layout);
        billing_layout = findViewById(R.id.billing_layout);

        shipping_linear = findViewById(R.id.shipping_linear);
        billing_linear = findViewById(R.id.billing_linear);

        shiping_address_recy = findViewById(R.id.shiping_address_recy);
        billing_address_recy = findViewById(R.id.billing_address_recy);
        //////////////////////////////////////////////////
        personal_first_name_et = findViewById(R.id.personal_first_name_et);
        personal_last_name_et = findViewById(R.id.personal_last_name_et);
        personal_email_et = findViewById(R.id.personal_email_et);
        personal_save_btn = findViewById(R.id.personal_save_btn);
        //////////////////////////////////////////////////
        shi_create_new_address = findViewById(R.id.shi_create_new_address);
        shi_user_saved_address = findViewById(R.id.shi_user_saved_address);
        shi_radio = findViewById(R.id.shi_radio);
        add_radio = findViewById(R.id.add_radio);
        yes = findViewById(R.id.yes);
        no = findViewById(R.id.no);
        shi_first_name_et = findViewById(R.id.shi_first_name_et);
        shi_last_name_et = findViewById(R.id.shi_last_name_et);
        shi_phone_et = findViewById(R.id.shi_phone_et);
        shi_email_et = findViewById(R.id.shi_email_et);
        shi_city_et = findViewById(R.id.shi_city_et);
        shi_block_et = findViewById(R.id.shi_block_et);
        shi_street_et = findViewById(R.id.shi_street_et);
        shi_building_et = findViewById(R.id.shi_building_et);
        shi_more_address_et = findViewById(R.id.shi_more_address_et);
        shi_postal_code_et = findViewById(R.id.shi_postal_code_et);
        shi_spin_country = findViewById(R.id.shi_spin_country);
        bil_country_code = findViewById(R.id.bil_country_code);
        shi_country_code = findViewById(R.id.shi_country_code);
        shi_city_spin = findViewById(R.id.shi_city_spin);
        shi_save_btn = findViewById(R.id.shi_save_btn);
        shi_location_placeText = findViewById(R.id.shi_location_placeText);
        //////////////////////////////////////////////////
        bil_create_new_address = findViewById(R.id.bil_create_new_address);
        bil_user_saved_address = findViewById(R.id.bil_user_saved_address);
        bil_radio = findViewById(R.id.bil_radio);
        bil_first_name_et = findViewById(R.id.bil_first_name_et);
        bil_last_name_et = findViewById(R.id.bil_last_name_et);
        bil_phone_et = findViewById(R.id.bil_phone_et);
        bil_email_et = findViewById(R.id.bil_email_et);
        bil_city_et = findViewById(R.id.bil_city_et);
        bil_block_et = findViewById(R.id.bil_block_et);
        bil_street_et = findViewById(R.id.bil_street_et);
        bil_building_et = findViewById(R.id.bil_building_et);
        bil_more_address_et = findViewById(R.id.bil_more_address_et);
        bil_postal_code_et = findViewById(R.id.bil_postal_code_et);
        bil_spin_country = findViewById(R.id.bil_spin_country);
        bil_city_spin = findViewById(R.id.bil_city_spin);
        scrollView = findViewById(R.id.scrollView);
        //////////////////////////////////////////////////
        type = getIntent().getStringExtra("type");
        if (type.equals("guest")) {
            per_checkData = false;
            shi_radio.setVisibility(View.GONE);
            bil_radio.setVisibility(View.GONE);
            personal_tab_lin.setVisibility(View.VISIBLE);

            shipping_tab_lin.setVisibility(View.VISIBLE);
            billing_tab_lin.setVisibility(View.GONE);
            personal_Layout.setVisibility(View.VISIBLE);
            shipping_layout.setVisibility(View.GONE);
            billing_layout.setVisibility(View.GONE);
            saveAdd.setVisibility(View.GONE);
            same_add_checkbox.setVisibility(View.GONE);
        } else {
            per_checkData = true;
            shi_radio.setVisibility(View.VISIBLE);
            bil_radio.setVisibility(View.VISIBLE);
            personal_tab_lin.setVisibility(View.GONE);
            shipping_tab_lin.setVisibility(View.VISIBLE);
            shipping_tab_text.setTextColor(getResources().getColor(R.color.red));
            shipping_tab_line.setBackgroundColor(getResources().getColor(R.color.red));
            billing_tab_lin.setVisibility(View.GONE);
            personal_Layout.setVisibility(View.GONE);
            shipping_layout.setVisibility(View.VISIBLE);
            shi_save_btn.setVisibility(View.GONE);
            continue_place_order_btn.setVisibility(View.VISIBLE);
            saveAdd.setVisibility(View.VISIBLE);
            same_add_checkbox.setVisibility(View.VISIBLE);
            billing_layout.setVisibility(View.GONE);
            bil_first_name_et.setText(fname);
            bil_last_name_et.setText(lname);
            bil_phone_et.setText(mob);
            bil_email_et.setText(email);
            shi_first_name_et.setText(fname);
            shi_last_name_et.setText(lname);
            shi_phone_et.setText(mob);
            shi_email_et.setText(email);
        }

        personal_save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fname, lname, email;
                fname = personal_first_name_et.getText().toString().trim();
                lname = personal_last_name_et.getText().toString().trim();
                email = personal_email_et.getText().toString().trim();
                if (fname.length() == 0) {
                    personal_first_name_et.setError(getResources().getString(R.string.please_enter_first_name));
                    personal_first_name_et.requestFocus();
                    return;
                }
                if (lname.length() == 0) {
                    personal_last_name_et.setError(getResources().getString(R.string.please_enter_last_name));
                    personal_last_name_et.requestFocus();
                    return;
                }
                if (email.length() == 0) {
                    personal_email_et.setError(getResources().getString(R.string.please_enter_email_address));
                    personal_email_et.requestFocus();
                    return;
                }
                if (!MethodClass.emailValidator(email)) {
                    personal_email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
                    personal_email_et.requestFocus();
                    return;
                }
                per_checkData = true;
                personal_Layout.setVisibility(View.GONE);
                shipping_layout.setVisibility(View.VISIBLE);
                billing_layout.setVisibility(View.GONE);
                saveAdd.setVisibility(View.GONE);
                same_add_checkbox.setVisibility(View.VISIBLE);
                shi_save_btn.setVisibility(View.GONE);
                continue_place_order_btn.setVisibility(View.VISIBLE);
                bil_email_et.setText(email);
                shi_email_et.setText(email);
                bil_first_name_et.setText(fname);
                bil_last_name_et.setText(lname);
                shi_first_name_et.setText(fname);
                shi_last_name_et.setText(lname);
                personal_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                personal_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
                shipping_tab_text.setTextColor(getResources().getColor(R.color.red));
                shipping_tab_line.setBackgroundColor(getResources().getColor(R.color.red));
                billing_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                billing_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
            }
        });

        add_radio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        Log.d("chk", "id" + checkedId);

                        if (checkedId == R.id.yes) {
                            //some code
                            save_Add = "Y";
                        } else if (checkedId == R.id.no) {
                            //some code
                            save_Add = "N";
                        }

                    }

                });

        shi_save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (shi_create_new_address.isChecked()) {
                    String fname, lname, phone, email, country, city, block, street, building, more_add, postal_code, location;
                    fname = shi_first_name_et.getText().toString().trim();
                    lname = shi_last_name_et.getText().toString().trim();
                    phone = shi_phone_et.getText().toString().trim();
                    email = shi_email_et.getText().toString().trim();
                    country = shi_country_str;
                    if (shi_country_id.equals("134")) {
                        city = shi_city_id;
                    } else {
                        city = shi_city_et.getText().toString().trim();
                    }
                    block = shi_block_et.getText().toString().trim();
                    street = shi_street_et.getText().toString().trim();
                    building = shi_building_et.getText().toString().trim();
                    more_add = shi_more_address_et.getText().toString().trim();
                    if (fname.length() == 0) {
                        shi_first_name_et.setError(getResources().getString(R.string.please_enter_first_name));
                        shi_first_name_et.requestFocus();
                        return;
                    }
                    if (lname.length() == 0) {
                        shi_last_name_et.setError(getResources().getString(R.string.please_enter_last_name));
                        shi_last_name_et.requestFocus();
                        return;
                    }

                    if (email.length() == 0) {
                        shi_email_et.setError(getResources().getString(R.string.please_enter_email_address));
                        shi_email_et.requestFocus();
                        return;
                    }
                    if (!MethodClass.emailValidator(email)) {
                        shi_email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
                        shi_email_et.requestFocus();
                        return;
                    }
                    if (country.equals("")) {
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_country), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                    if (phone.length() == 0) {
                        shi_phone_et.setError(getResources().getString(R.string.please_enter_mobile_no));
                        shi_phone_et.requestFocus();
                        return;
                    }
                    if (shi_country_id.equals("134")) {
                        if (city.equals("")) {
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_city), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        if (block.length() == 0) {
                            shi_block_et.setError(getResources().getString(R.string.please_enter_block));
                            shi_block_et.requestFocus();
                            return;
                        }
                        if (building.length() == 0) {
                            shi_building_et.setError(getResources().getString(R.string.please_enter_building));
                            shi_building_et.requestFocus();
                            return;
                        }
                    } else {
                        if (city.length() == 0) {
                            shi_city_et.setError(getResources().getString(R.string.please_enter_city));
                            shi_city_et.requestFocus();
                            return;
                        }
                    }
                    if (street.length() == 0) {
                        shi_street_et.setError(getResources().getString(R.string.please_enter_street));
                        shi_street_et.requestFocus();
                        return;
                    }
                } else {
                    if (shi_sel_add_id.equals("")) {
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_address), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        return;
                    }
                }

                shi_checkData = true;
                personal_Layout.setVisibility(View.GONE);
                shipping_layout.setVisibility(View.GONE);
                billing_layout.setVisibility(View.VISIBLE);
                continue_place_order_btn.setVisibility(View.VISIBLE);
                personal_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                personal_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
                shipping_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                shipping_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
                billing_tab_text.setTextColor(getResources().getColor(R.color.red));
                billing_tab_line.setBackgroundColor(getResources().getColor(R.color.red));
            }
        });

        personal_tab_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continue_place_order_btn.setVisibility(View.GONE);
                per_checkData = false;
                personal_tab_lin.setVisibility(View.VISIBLE);
                shipping_tab_lin.setVisibility(View.VISIBLE);
                billing_tab_lin.setVisibility(View.VISIBLE);
                personal_Layout.setVisibility(View.VISIBLE);
                shi_save_btn.setVisibility(View.GONE);
                shipping_layout.setVisibility(View.GONE);
                billing_layout.setVisibility(View.GONE);
                personal_tab_text.setTextColor(getResources().getColor(R.color.red));
                personal_tab_line.setBackgroundColor(getResources().getColor(R.color.red));
                shipping_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                shipping_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
                billing_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                billing_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
            }
        });

        shipping_tab_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!per_checkData) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_save_continue), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return;
                }
                shi_checkData = false;
                if (type.equals("guest")) {
                    personal_tab_lin.setVisibility(View.VISIBLE);
                } else {
                    personal_tab_lin.setVisibility(View.GONE);
                }
                shipping_tab_lin.setVisibility(View.VISIBLE);
                billing_tab_lin.setVisibility(View.VISIBLE);
                personal_Layout.setVisibility(View.GONE);
                shipping_layout.setVisibility(View.VISIBLE);
                billing_layout.setVisibility(View.GONE);
                if(same_add_checkbox.isChecked()){
                    continue_place_order_btn.setVisibility(View.VISIBLE);
                }

                personal_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                personal_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
                shipping_tab_text.setTextColor(getResources().getColor(R.color.red));
                shipping_tab_line.setBackgroundColor(getResources().getColor(R.color.red));
                billing_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                billing_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
            }
        });

        billing_tab_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!bil_checkData) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_save_continue), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return;
                }
                if (!shi_checkData) {
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_save_continue), Snackbar.LENGTH_LONG);
                    snackbar.show();
                    return;
                }
                if (type.equals("guest")) {
                    personal_tab_lin.setVisibility(View.VISIBLE);
                } else {
                    personal_tab_lin.setVisibility(View.GONE);
                }
                shipping_tab_lin.setVisibility(View.VISIBLE);
                billing_tab_lin.setVisibility(View.VISIBLE);
                personal_Layout.setVisibility(View.GONE);
                shipping_layout.setVisibility(View.GONE);
                continue_place_order_btn.setVisibility(View.VISIBLE);
                billing_layout.setVisibility(View.VISIBLE);
                personal_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                personal_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
                shipping_tab_text.setTextColor(getResources().getColor(R.color.textLightGreyColor));
                shipping_tab_line.setBackgroundColor(getResources().getColor(R.color.textLightGreyColor));
                billing_tab_text.setTextColor(getResources().getColor(R.color.red));
                billing_tab_line.setBackgroundColor(getResources().getColor(R.color.red));
            }
        });

        bil_create_new_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    billing_linear.setVisibility(View.VISIBLE);
                    billing_address_recy.setVisibility(View.GONE);
                    saveAdd.setVisibility(View.VISIBLE);
                } else {
                    billing_linear.setVisibility(View.GONE);
                    billing_address_recy.setVisibility(View.VISIBLE);
                    saveAdd.setVisibility(View.GONE);
                }
            }
        });

        shi_create_new_address.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    shipping_linear.setVisibility(View.VISIBLE);
                    shiping_address_recy.setVisibility(View.GONE);
                    saveAdd.setVisibility(View.VISIBLE);
                    if(!shi_country_id.equals("134") && merchantOutsideKuwait.equals("Y")){
                        cashOnDelevery_rb.setVisibility(View.GONE);
                        online_rb.setVisibility(View.VISIBLE);
                        online_rb.setChecked(true);
                    }else if(!shi_country_id.equals("134") && merchantOutsideKuwait.equals("N")){
                        cashOnDelevery_rb.setVisibility(View.GONE);
                        online_rb.setVisibility(View.VISIBLE);
                        online_rb.setChecked(true);
                    }else if(shi_country_id.equals("134") && merchantOutsideKuwait.equals("Y")){
                        cashOnDelevery_rb.setVisibility(View.GONE);
                        online_rb.setVisibility(View.VISIBLE);
                        online_rb.setChecked(true);
                    }else {
                        if(CoD){
                            cashOnDelevery_rb.setVisibility(View.VISIBLE);
                            cashOnDelevery_rb.setChecked(true);
                        }else {
                            cashOnDelevery_rb.setVisibility(View.GONE);
                            online_rb.setChecked(true);
                        }
                        if(onLi){
                            online_rb.setVisibility(View.VISIBLE);
                            online_rb.setChecked(true);
                        }else {
                            online_rb.setVisibility(View.GONE);
                            cashOnDelevery_rb.setChecked(true);
                        }

                    }
                } else {
                    shipping_linear.setVisibility(View.GONE);
                    shiping_address_recy.setVisibility(View.VISIBLE);
                    saveAdd.setVisibility(View.GONE);
                    setPayment();
                }
            }
        });

//        shi_location_placeText.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
//            @Override
//            public void onPlaceSelected(final Place place) {
//                Log.e("placebdec", place.description);
//                shi_location_placeText.getDetailsFor(place, new DetailsCallback() {
//                    @Override
//                    public void onSuccess(PlaceDetails details) {
//                        shi_lat = String.valueOf(details.geometry.location.lat);
//                        shi_long = String.valueOf(details.geometry.location.lng);
//                        Log.e("loglat", shi_lat + "////" + shi_long);
//                    }
//
//                    @Override
//                    public void onFailure(Throwable failure) {
//                    }
//
//                    public void onComplete() {
//                    }
//                });
//            }
//        });

        continue_place_order_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(same_add_checkbox.isChecked()){
                    if (shi_create_new_address.isChecked()) {
                        String fname, lname, phone, email, country, city, block, street, building, more_add, postal_code, location;
                        fname = shi_first_name_et.getText().toString().trim();
                        lname = shi_last_name_et.getText().toString().trim();
                        phone = shi_phone_et.getText().toString().trim();
                        email = shi_email_et.getText().toString().trim();
                        country = shi_country_str;
                        if (shi_country_id.equals("134")) {
                            city = shi_city_id;
                        } else {
                            city = shi_city_et.getText().toString().trim();
                        }
                        block = shi_block_et.getText().toString().trim();
                        street = shi_street_et.getText().toString().trim();
                        building = shi_building_et.getText().toString().trim();
                        more_add = shi_more_address_et.getText().toString().trim();
                        if (fname.length() == 0) {
                            shi_first_name_et.setError(getResources().getString(R.string.please_enter_first_name));
                            shi_first_name_et.requestFocus();
                            return;
                        }
                        if (lname.length() == 0) {
                            shi_last_name_et.setError(getResources().getString(R.string.please_enter_last_name));
                            shi_last_name_et.requestFocus();
                            return;
                        }

                        if (email.length() == 0) {
                            shi_email_et.setError(getResources().getString(R.string.please_enter_email_address));
                            shi_email_et.requestFocus();
                            return;
                        }
                        if (!MethodClass.emailValidator(email)) {
                            shi_email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
                            shi_email_et.requestFocus();
                            return;
                        }
                        if (country.equals("")) {
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_country), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        if (phone.length() == 0) {
                            shi_phone_et.setError(getResources().getString(R.string.please_enter_mobile_no));
                            shi_phone_et.requestFocus();
                            return;
                        }
                        if (shi_country_id.equals("134")) {
                            if (city.equals("")) {
                                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_city), Snackbar.LENGTH_SHORT);
                                snackbar.show();
                                return;
                            }
                            if (block.length() == 0) {
                                shi_block_et.setError(getResources().getString(R.string.please_enter_block));
                                shi_block_et.requestFocus();
                                return;
                            }
                            if (building.length() == 0) {
                                shi_building_et.setError(getResources().getString(R.string.please_enter_building));
                                shi_building_et.requestFocus();
                                return;
                            }
                        } else {
                            if (city.length() == 0) {
                                shi_city_et.setError(getResources().getString(R.string.please_enter_city));
                                shi_city_et.requestFocus();
                                return;
                            }
                        }
                        if (street.length() == 0) {
                            shi_street_et.setError(getResources().getString(R.string.please_enter_street));
                            shi_street_et.requestFocus();
                            return;
                        }
                    } else {
                        if (shi_sel_add_id.equals("")) {
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_address), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                    }
                    checkoutDataPost();
                }else {
                    if (bil_create_new_address.isChecked()) {
                        String fname, lname, phone, email, country, city, block, street, building, more_add, postal_code, location;
                        fname = bil_first_name_et.getText().toString().trim();
                        lname = bil_last_name_et.getText().toString().trim();
                        phone = bil_phone_et.getText().toString().trim();
                        email = bil_email_et.getText().toString().trim();
                        country = bil_country_str;
                        if (bil_country_id.equals("134")) {
                            city = bil_city_id;
                        } else {
                            city = bil_city_et.getText().toString().trim();
                        }
                        block = bil_block_et.getText().toString().trim();
                        street = bil_street_et.getText().toString().trim();
                        building = bil_building_et.getText().toString().trim();
                        more_add = bil_more_address_et.getText().toString().trim();
                        if (fname.length() == 0) {
                            bil_first_name_et.setError(getResources().getString(R.string.please_enter_first_name));
                            bil_first_name_et.requestFocus();
                            return;
                        }
                        if (lname.length() == 0) {
                            bil_last_name_et.setError(getResources().getString(R.string.please_enter_last_name));
                            bil_last_name_et.requestFocus();
                            return;
                        }

                        if (email.length() == 0) {
                            bil_email_et.setError(getResources().getString(R.string.please_enter_email_address));
                            bil_email_et.requestFocus();
                            return;
                        }
                        if (!MethodClass.emailValidator(email)) {
                            bil_email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
                            bil_email_et.requestFocus();
                            return;
                        }
                        if (country.equals("")) {
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_country), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                        if (phone.length() == 0) {
                            bil_phone_et.setError(getResources().getString(R.string.please_enter_mobile_no));
                            bil_phone_et.requestFocus();
                            return;
                        }
                        if (bil_country_id.equals("134")) {
                            if (city.equals("")) {
                                Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_city), Snackbar.LENGTH_SHORT);
                                snackbar.show();
                                return;
                            }
                            if (block.length() == 0) {
                                bil_block_et.setError(getResources().getString(R.string.please_enter_block));
                                bil_block_et.requestFocus();
                                return;
                            }
                            if (building.length() == 0) {
                                bil_building_et.setError(getResources().getString(R.string.please_enter_building));
                                bil_building_et.requestFocus();
                                return;
                            }
                        } else {
                            if (city.length() == 0) {
                                bil_city_et.setError(getResources().getString(R.string.please_enter_city));
                                bil_city_et.requestFocus();
                                return;
                            }
                        }
                        if (street.length() == 0) {
                            bil_street_et.setError(getResources().getString(R.string.please_enter_street));
                            bil_street_et.requestFocus();
                            return;
                        }
                    } else {
                        if (bil_sel_add_id.equals("")) {
                            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_select_address), Snackbar.LENGTH_SHORT);
                            snackbar.show();
                            return;
                        }
                    }

                    checkoutDataPost();
                }

            }
        });

        same_add_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    billing_tab_lin.setVisibility(View.GONE);
                    billing_layout.setVisibility(View.GONE);
                    shipping_layout.setVisibility(View.VISIBLE);
                    shi_save_btn.setVisibility(View.GONE);
                    continue_place_order_btn.setVisibility(View.VISIBLE);

                    bil_first_name_et.setText(shi_first_name_et.getText().toString().trim());
                    bil_last_name_et.setText(shi_last_name_et.getText().toString().trim());
                    bil_phone_et.setText(shi_phone_et.getText().toString().trim());
                    bil_email_et.setText(shi_email_et.getText().toString().trim());
                    bil_city_et.setText(shi_city_et.getText().toString().trim());
                    bil_block_et.setText(shi_block_et.getText().toString().trim());
                    bil_street_et.setText(shi_street_et.getText().toString().trim());
                    bil_building_et.setText(shi_building_et.getText().toString().trim());
                    bil_more_address_et.setText(shi_more_address_et.getText().toString().trim());
                    bil_postal_code_et.setText(shi_postal_code_et.getText().toString().trim());

                    if (!shi_country_id.equals("") && !shi_country_id.equals("null") && !shi_country_id.equals(null)) {
                        for (int i = 0; i < countriesArrayList.size(); i++) {
                            MethodClass.StringWithTag2 stringWithTag =countriesArrayList.get(i);
                            if (shi_country_id.equals(String.valueOf(stringWithTag.tag))){
                                bil_spin_country.setSelection(i);
                            }
                        }
                        if (shi_country_id.equals("134")){
                            bill_search_edt.setText(shi_city_str);
                            bil_city_str = shi_city_str;
                            bil_city_id = shi_city_id;
                        }
                    }

                }else {
                    billing_tab_lin.setVisibility(View.VISIBLE);
                    shi_save_btn.setVisibility(View.VISIBLE);
                    continue_place_order_btn.setVisibility(View.GONE);
                }
            }
        });

        get_cart();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(CheckOutActivity.this);
        MethodClass.setMenuColor(CheckOutActivity.this, "no");
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void get_cart() {
        if (!isNetworkConnected(CheckOutActivity.this)) {
            MethodClass.network_error_alert(CheckOutActivity.this);
            return;
        }
        MethodClass.showProgressDialog(CheckOutActivity.this);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String server_url = "";
        if (PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getBoolean("is_logged_in", false)) {
            server_url = getString(R.string.SERVER_URL) + "checkout";
        } else {
            server_url = getString(R.string.SERVER_URL) + "checkout/" + android_id;
        }
        Log.e("get-cart", server_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(CheckOutActivity.this);
                Log.e("addToCartRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(CheckOutActivity.this, response);
                    if (resultResponse != null) {
                        if (resultResponse.getString("cart").equals(null) || resultResponse.getString("cart").equals("null") || resultResponse.getString("cart").equals("")){
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(CheckOutActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    Intent intent = new Intent(CheckOutActivity.this, HomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                            sweetAlertDialog.show();
                            return;
                        }
                        JSONObject cart = resultResponse.getJSONObject("cart");
                        merchantOutsideKuwait = resultResponse.getString("merchantOutsideKuwait");
                        String id = cart.getString("id");
                        String total_item = cart.getString("total_item");
                        String total_qty = cart.getString("total_qty");
                        String total = cart.getString("total");
                        total_price_tv.setText(getResources().getString(R.string.total_with_colon) + total);
                        total_items_tv.setText(total_item +" "+ getResources().getString(R.string.items));
                        JSONArray sellers = resultResponse.getJSONArray("sellers");
                        ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
                        for (int i = 0; i < sellers.length(); i++) {
                            JSONObject object = sellers.getJSONObject(i);

                            HashMap<String, String> hashMap = new HashMap<>();
                            JSONObject seller_info = object.getJSONObject("seller_info");
                            if(seller_info.getString("payment_mode").equals("A")){
                                onLi = true;
                                CoD = true;
                            }else if(seller_info.getString("payment_mode").equals("O")){
                                onLi = true;
                            }else if(seller_info.getString("payment_mode").equals("C")){
                                CoD = true;
                            }
                            hashMap.put(ID, object.getString("id"));

                            hashMap.put(NAME, seller_info.getString("fname") + " " + seller_info.getString("lname"));

                            hashMap.put(PRODUCTS, seller_info.getString("products"));
                            arrayList.add(hashMap);
                        }
                        CheckOutAdapter shoppigCartAdapter = new CheckOutAdapter(CheckOutActivity.this, arrayList);
                        recy_view.setAdapter(shoppigCartAdapter);
                        recy_view.setFocusable(false);
                        ///////////////////////////////////////////////////////////////////////
                        if (!type.equals("guest")) {
                            JSONArray addresses = resultResponse.getJSONArray("addresses");
                            ArrayList<HashMap<String, String>> addressArrayList = new ArrayList<>();
                            for (int i = 0; i < addresses.length(); i++) {
                                HashMap<String, String> hashMap = new HashMap<>();

                                hashMap.put(ID, addresses.getJSONObject(i).getString("id"));
                                hashMap.put(USER_ID, addresses.getJSONObject(i).getString("user_id"));

                                String shipping_fname = addresses.getJSONObject(i).getString("shipping_fname");
                                String shipping_lname = addresses.getJSONObject(i).getString("shipping_lname");
                                if (shipping_lname.equals("") || shipping_lname.equals(null) || shipping_lname.equals("null")) {
                                    shipping_lname = "";
                                }
                                String name = shipping_fname + " " + shipping_lname;
                                String city = addresses.getJSONObject(i).getString("city");
                                String street = addresses.getJSONObject(i).getString("street");
                                String block = addresses.getJSONObject(i).getString("block");
                                if (block.equals("") || block.equals(null) || block.equals("null")) {
                                    block = "";
                                }
                                String building = addresses.getJSONObject(i).getString("building");
                                if (building.equals("") || building.equals(null) || building.equals("null")) {
                                    building = "";
                                }
                                String more_address = addresses.getJSONObject(i).getString("more_address");
                                if (more_address.equals("") || more_address.equals(null) || more_address.equals("null")) {
                                    more_address = "";
                                }
                                String country = addresses.getJSONObject(i).getString("country");
                                String country_name = addresses.getJSONObject(i).getJSONObject("get_country").getString("name");
                                String postal_code = addresses.getJSONObject(i).getString("postal_code");
                                if (postal_code.equals("") || postal_code.equals(null) || postal_code.equals("null")) {
                                    postal_code = "";
                                }
                                String phone = addresses.getJSONObject(i).getString("phone");
                                String email = addresses.getJSONObject(i).getString("email");
                                String is_default = addresses.getJSONObject(i).getString("is_default");
                                String address = building + ", " + block + ", " + street + ", " + city + ", " + more_address + ", " + country_name + ", " + postal_code;

                                hashMap.put(NAME, name);
                                hashMap.put(ADDRESS, address);
                                hashMap.put(IS_DEFAULT, is_default);
                                hashMap.put(PHONE_NO, phone);
                                hashMap.put(EMAIL_ID, email);
                                hashMap.put(COUNTRY_ID, country);
                                addressArrayList.add(hashMap);
                            }

                            CheckoutshippingAddressAdapter checkoutshippingAddressAdapter = new CheckoutshippingAddressAdapter(CheckOutActivity.this, addressArrayList);
                            shiping_address_recy.setAdapter(checkoutshippingAddressAdapter);
                            shiping_address_recy.setFocusable(false);
                            CheckoutBillingAddressAdapter checkoutBillingAddressAdapter = new CheckoutBillingAddressAdapter(CheckOutActivity.this, addressArrayList);
                            billing_address_recy.setAdapter(checkoutBillingAddressAdapter);
                            billing_address_recy.setFocusable(false);
                        }

                        ////////////////////////////////////////////////////////////////////////
                        JSONArray countries = resultResponse.getJSONArray("countries");
                        Log.e("country", countries.toString() );
                        countriesArrayList = new ArrayList<>();
                        for (int i = 0; i < countries.length(); i++) {
                            JSONObject object = countries.getJSONObject(i);
                            String country_id = object.getJSONObject("country_details_bylanguage").getString("country_id");
                            String name = object.getJSONObject("country_details_bylanguage").getString("name");
                            String country_code = object.getJSONObject("country_details_bylanguage").getString("country_code");
                            MethodClass.StringWithTag2 stringWithTag = new MethodClass.StringWithTag2(name,country_code, country_id);
                            countriesArrayList.add(stringWithTag);
                        }
                        ArrayAdapter<MethodClass.StringWithTag2> adapter = new ArrayAdapter<MethodClass.StringWithTag2>(CheckOutActivity.this, android.R.layout.simple_dropdown_item_1line, countriesArrayList);
                        shi_spin_country.setAdapter(adapter);
                        shi_spin_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                MethodClass.StringWithTag2 stringWithTag = (MethodClass.StringWithTag2) parent.getItemAtPosition(position);
                                shi_country_id = (String) stringWithTag.tag;
                                shi_country_str = stringWithTag.string;
                                shi_country_code.setText(stringWithTag.string2);
                                if (shi_country_id.equals("134")) {
                                    shi_postal_code_et.setVisibility(View.GONE);
                                    shi_city_et.setVisibility(View.GONE);
                                    shi_city_spin.setVisibility(View.GONE);
                                    search_edt.setVisibility(View.VISIBLE);
                                    shi_block_et.setHint(getResources().getString(R.string.block));
                                    shi_building_et.setHint(getResources().getString(R.string.building));
                                    if(onLi){
                                        online_rb.setVisibility(View.VISIBLE);
                                    }else {
                                        online_rb.setVisibility(View.GONE);
                                        cashOnDelevery_rb.setChecked(true);
                                    }
                                    if(CoD){
                                        cashOnDelevery_rb.setVisibility(View.VISIBLE);
                                    }else {
                                        cashOnDelevery_rb.setVisibility(View.GONE);
                                        online_rb.setChecked(true);
                                    }
                                    try {
                                        setShiCity(resultResponse.getJSONArray("cities"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Log.e("JSONExceptionCity", e.toString());
                                    }
                                } else {
                                    shi_postal_code_et.setVisibility(View.VISIBLE);
                                    shi_city_et.setVisibility(View.VISIBLE);
                                    shi_city_spin.setVisibility(View.GONE);
                                    search_edt.setVisibility(View.GONE);
                                    shi_block_et.setHint(getResources().getString(R.string.block_optional));
                                    shi_building_et.setHint(getResources().getString(R.string.building_optional));
                                    cashOnDelevery_rb.setVisibility(View.GONE);
                                    online_rb.setChecked(true);
                                    online_rb.setVisibility(View.VISIBLE);
                                }
                                if(!shi_country_id.equals("134") && merchantOutsideKuwait.equals("Y")){
                                    cashOnDelevery_rb.setVisibility(View.GONE);
                                    online_rb.setVisibility(View.VISIBLE);
                                    online_rb.setChecked(true);
                                }else if(!shi_country_id.equals("134") && merchantOutsideKuwait.equals("N")){
                                    cashOnDelevery_rb.setVisibility(View.GONE);
                                    online_rb.setVisibility(View.VISIBLE);
                                    online_rb.setChecked(true);
                                }else if(shi_country_id.equals("134") && merchantOutsideKuwait.equals("Y")){
                                    cashOnDelevery_rb.setVisibility(View.GONE);
                                    online_rb.setVisibility(View.VISIBLE);
                                    online_rb.setChecked(true);
                                }else {
                                    if(CoD){
                                        cashOnDelevery_rb.setVisibility(View.VISIBLE);
                                        cashOnDelevery_rb.setChecked(true);
                                    }else {
                                        cashOnDelevery_rb.setVisibility(View.GONE);
                                        online_rb.setChecked(true);
                                    }
                                    if(onLi){
                                        online_rb.setVisibility(View.VISIBLE);
                                        online_rb.setChecked(true);
                                    }else {
                                        online_rb.setVisibility(View.GONE);
                                        cashOnDelevery_rb.setChecked(true);
                                    }

                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        ArrayAdapter<MethodClass.StringWithTag2> bilAddAdapter = new ArrayAdapter<MethodClass.StringWithTag2>(CheckOutActivity.this, android.R.layout.simple_dropdown_item_1line, countriesArrayList);
                        bil_spin_country.setAdapter(bilAddAdapter);
                        bil_spin_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                MethodClass.StringWithTag2 stringWithTag = (MethodClass.StringWithTag2) parent.getItemAtPosition(position);
                                bil_country_id = (String) stringWithTag.tag;
                                bil_country_str = stringWithTag.string;
                                bil_country_code.setText(stringWithTag.string2);
                                if (bil_country_id.equals("134")) {
                                    bil_postal_code_et.setVisibility(View.GONE);
                                    bil_city_et.setVisibility(View.GONE);
                                    bil_city_spin.setVisibility(View.GONE);
                                    bill_search_edt.setVisibility(View.VISIBLE);
                                    bil_block_et.setHint(getResources().getString(R.string.block));
                                    bil_building_et.setHint(getResources().getString(R.string.building));
                                    try {
                                        setBilCity(resultResponse.getJSONArray("cities"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        Log.e("JSONExceptionCity", e.toString());
                                    }
                                } else {
                                    bil_postal_code_et.setVisibility(View.VISIBLE);
                                    bil_city_et.setVisibility(View.VISIBLE);
                                    bil_city_spin.setVisibility(View.GONE);
                                    bill_search_edt.setVisibility(View.GONE);
                                    bil_block_et.setHint(getResources().getString(R.string.block_optional));
                                    bil_building_et.setHint(getResources().getString(R.string.building_optional));
                                }

                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });
                        scrollView.setVisibility(View.VISIBLE);

                    }
                } catch (Exception e) {
                    MethodClass.error_alert(CheckOutActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(CheckOutActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(CheckOutActivity.this);
                } else {
                    MethodClass.error_alert(CheckOutActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(CheckOutActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(CheckOutActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void setShiCity(JSONArray cities) {
        try {
            cityArrayList = new ArrayList<>();
            for (int i = 0; i < cities.length(); i++) {
                JSONObject object = cities.getJSONObject(i);
                String country_id = object.getJSONObject("city_details_by_language").getString("city_id");
                String name = object.getJSONObject("city_details_by_language").getString("name");
                MethodClass.StringWithTag stringWithTag = new MethodClass.StringWithTag(name, country_id);
                cityArrayList.add(stringWithTag);
            }
            ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(CheckOutActivity.this, android.R.layout.simple_dropdown_item_1line, cityArrayList);
            shi_city_spin.setAdapter(adapter);
            search_edt.setAdapter(adapter);
//            shi_city_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//                @Override
//                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                    MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
//                    shi_city_id = (String) stringWithTag.tag;
//                    shi_city_str = (String) stringWithTag.string;
//                }
//
//                @Override
//                public void onNothingSelected(AdapterView<?> parent) {
//                }
//            });
            search_edt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                    shi_city_id = (String) stringWithTag.tag;
                    shi_city_str = (String) stringWithTag.string;
                    Log.e("CITY ID", shi_city_id);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONExceptioncity", e.toString());
        }
    }

    public void setBilCity(JSONArray cities) {
        try {
            ArrayList<MethodClass.StringWithTag> cityArrayList = new ArrayList<>();
            for (int i = 0; i < cities.length(); i++) {
                JSONObject object = cities.getJSONObject(i);
                String country_id = object.getJSONObject("city_details_by_language").getString("city_id");
                String name = object.getJSONObject("city_details_by_language").getString("name");
                MethodClass.StringWithTag stringWithTag = new MethodClass.StringWithTag(name, country_id);
                cityArrayList.add(stringWithTag);
            }
            ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(CheckOutActivity.this, android.R.layout.simple_dropdown_item_1line, cityArrayList);
            bil_city_spin.setAdapter(adapter);
            bill_search_edt.setAdapter(adapter);
            bil_city_spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                    bil_city_id = (String) stringWithTag.tag;
                    bil_city_str = (String) stringWithTag.string;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
            bill_search_edt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                    bil_city_id = (String) stringWithTag.tag;
                    bil_city_str = (String) stringWithTag.string;
                    Log.e("CITY ID", shi_city_id);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONExceptioncity", e.toString());
        }
    }

    public void checkoutDataPost() {
        if (!isNetworkConnected(CheckOutActivity.this)) {
            MethodClass.network_error_alert(CheckOutActivity.this);
            return;
        }
        MethodClass.showProgressDialog(CheckOutActivity.this);
        final String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String server_url = getString(R.string.SERVER_URL) + "checkout";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        if (!order_id.equals("")) {
            hashMap.put("order_id", order_id);
        }
        if (!PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getBoolean("is_logged_in", false)) {
            hashMap.put("device_id", android_id);
        }
        if (type.equals("guest")) {
            hashMap.put("fname", personal_first_name_et.getText().toString().trim());
            hashMap.put("lname", personal_last_name_et.getText().toString().trim());
            hashMap.put("email", personal_email_et.getText().toString().trim());
        }
        hashMap.put("payment_method", cashOnDelevery_rb.isChecked() ? "C" : "O");
        if(!type.equals("guest")){
            hashMap.put("save_address", yes.isChecked() ? "Y" : "N");
        }

        if (shi_create_new_address.isChecked()) {
            hashMap.put("shipping_fname", shi_first_name_et.getText().toString().trim());
            hashMap.put("shipping_lname", shi_last_name_et.getText().toString().trim());
            hashMap.put("shipping_email", shi_email_et.getText().toString().trim());
            hashMap.put("shipping_phone", shi_phone_et.getText().toString().trim());
            hashMap.put("shipping_country", shi_country_id);
            if (shi_country_id.equals("134")) {
                hashMap.put("shipping_city", shi_city_str);
                hashMap.put("shipping_city_id", shi_city_id);
            } else {
                hashMap.put("shipping_city", shi_city_et.getText().toString().trim());
                hashMap.put("shipping_postal_code", shi_postal_code_et.getText().toString().trim());
            }
            hashMap.put("shipping_street", shi_street_et.getText().toString().trim());
            hashMap.put("shipping_block", shi_block_et.getText().toString().trim());
            hashMap.put("shipping_building", shi_building_et.getText().toString().trim());
            hashMap.put("shipping_more_address", shi_more_address_et.getText().toString().trim());
            hashMap.put("location", shi_location_placeText.getText().toString().trim());
            hashMap.put("lat", shi_lat);
            hashMap.put("lng", shi_long);
        } else {
            hashMap.put("shipping_address_id", shi_sel_add_id);
        }

        if(same_add_checkbox.isChecked()){
            if(shi_create_new_address.isChecked()){
                hashMap.put("billing_fname", shi_first_name_et.getText().toString().trim());
                hashMap.put("billing_lname", shi_last_name_et.getText().toString().trim());
                hashMap.put("billing_email", shi_email_et.getText().toString().trim());
                hashMap.put("billing_phone", shi_phone_et.getText().toString().trim());
                hashMap.put("billing_country", shi_country_id);
                if (shi_country_id.equals("134")) {
                    hashMap.put("billing_city", shi_city_str);
                    hashMap.put("billing_city_id", shi_city_id);
                } else {
                    hashMap.put("billing_city", shi_city_et.getText().toString().trim());
                    hashMap.put("billing_postal_code", shi_postal_code_et.getText().toString().trim());
                }
                hashMap.put("billing_street", shi_street_et.getText().toString().trim());
                hashMap.put("billing_block", shi_block_et.getText().toString().trim());
                hashMap.put("billing_building", shi_building_et.getText().toString().trim());
                hashMap.put("billing_more_address", shi_more_address_et.getText().toString().trim());
            } else {
                hashMap.put("billing_address_id", shi_sel_add_id);
            }
        }else {
            if (bil_create_new_address.isChecked()) {
                hashMap.put("billing_fname", bil_first_name_et.getText().toString().trim());
                hashMap.put("billing_lname", bil_last_name_et.getText().toString().trim());
                hashMap.put("billing_email", bil_email_et.getText().toString().trim());
                hashMap.put("billing_phone", bil_phone_et.getText().toString().trim());
                hashMap.put("billing_country", bil_country_id);
                if (bil_country_id.equals("134")) {
                    hashMap.put("billing_city", bil_city_str);
                    hashMap.put("billing_city_id", bil_city_id);
                } else {
                    hashMap.put("billing_city", bil_city_et.getText().toString().trim());
                    hashMap.put("billing_postal_code", bil_postal_code_et.getText().toString().trim());
                }
                hashMap.put("billing_street", bil_street_et.getText().toString().trim());
                hashMap.put("billing_block", bil_block_et.getText().toString().trim());
                hashMap.put("billing_building", bil_building_et.getText().toString().trim());
                hashMap.put("billing_more_address", bil_more_address_et.getText().toString().trim());
            } else {
                hashMap.put("billing_address_id", bil_sel_add_id);
            }
        }

        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(CheckOutActivity.this);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(CheckOutActivity.this, response);
                    if(resultResponse !=null){
                        if(resultResponse.has("out_of_stock")){
                            new SweetAlertDialog(CheckOutActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText("Out of stock")
                                    .setContentText(resultResponse.getString("out_of_stock"))
                                    .setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    Intent intent = new Intent(CheckOutActivity.this, ShoppingCartActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            }).show();
                        }else {
                            order_id = resultResponse.getJSONObject("order").getString("id");
                            if (!PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getBoolean("is_logged_in", false)) {
                                token = resultResponse.getString("token");
                            }
                            Intent intent = new Intent(CheckOutActivity.this, PlaceOrderActivity.class);
                            intent.putExtra("id", resultResponse.getJSONObject("order").getString("id"));
                            startActivity(intent);
                        }

                    }

                } catch (Exception e) {
                    MethodClass.error_alert(CheckOutActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(CheckOutActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(CheckOutActivity.this);
                } else {
                    MethodClass.error_alert(CheckOutActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(CheckOutActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(CheckOutActivity.this).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(CheckOutActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void setPayment(){

        if(!sel_coun_id.equals("134") && merchantOutsideKuwait.equals("Y")){
            cashOnDelevery_rb.setVisibility(View.GONE);
            online_rb.setVisibility(View.VISIBLE);
            online_rb.setChecked(true);
        }else if(!sel_coun_id.equals("134") && merchantOutsideKuwait.equals("N")){
            cashOnDelevery_rb.setVisibility(View.GONE);
            online_rb.setVisibility(View.VISIBLE);
            online_rb.setChecked(true);
        }else if(sel_coun_id.equals("134") && merchantOutsideKuwait.equals("Y")){
            cashOnDelevery_rb.setVisibility(View.GONE);
            online_rb.setVisibility(View.VISIBLE);
            online_rb.setChecked(true);
        }else {
            cashOnDelevery_rb.setVisibility(View.VISIBLE);
            online_rb.setVisibility(View.VISIBLE);
            cashOnDelevery_rb.setChecked(true);
        }
    }

}
