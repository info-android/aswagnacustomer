package com.aswagnacustomer.aswagnacustomer.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.BuildConfig;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SHARED_PREF;

public class SplashActivity extends AppCompatActivity {

    private String android_id;
    private String id = "";
    private String type = "";
    Dialog dialog;
    private Integer versionCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_splash);
        versionCode = BuildConfig.VERSION_CODE;
        String versionName = BuildConfig.VERSION_NAME;
        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Uri data = this.getIntent().getData();


        if (data != null && data.isHierarchical()) {
            Uri uri = Uri.parse(data.toString());
            id = uri.getQueryParameter("content");
            type = uri.getQueryParameter("type");
        }


        sendToken();
    }
    public void sendToken(){
        SharedPreferences pref = getSharedPreferences(SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String server_url = getString(R.string.SERVER_URL) + "send-notify";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("device_id", android_id);
        params.put("reg_id", regId);
        params.put("type", "A");
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("Login",jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("respLogin", response.toString());
                try {
                    JSONObject resultResponce = MethodClass.get_result_from_webservice(SplashActivity.this, response);
                    if (resultResponce != null) {


                        JSONObject app_settings = resultResponce.getJSONObject("app_settings");
                        if(app_settings.getString("android_version_check").equals("Y")){
                            if(Double.parseDouble(app_settings.getString("android_version"))>Double.parseDouble(String.valueOf(versionCode))){
                                final AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
                                builder.setTitle(getString(R.string.new_version));
                                builder.setMessage(app_settings.getString("message_for_android"));
                                builder.setPositiveButton(getString(R.string.Update), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                                                ("market://details?id=com.aswagnacustomer.aswagnacustomer")));
                                        dialog.dismiss();
                                    }
                                });

                                builder.setCancelable(false);
                                dialog = builder.show();
                            }else {
                                // if (status.equals("A") || status.equals("R"))
                                if (!PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("LANG", "").equals(null)) {
                                    PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).edit().putString("LANG", "en").commit();
                                }
                                if(type.equals("M")){
                                    Intent intent = new Intent(SplashActivity.this, MarchandProfileActivity.class);
                                    intent.putExtra("id",id);
                                    startActivity(intent);
                                    finish();
                                }else if(type.equals("P")){
                                    Intent intent = new Intent(SplashActivity.this, ProductDetailsActivity.class);
                                    intent.putExtra("id",id);
                                    startActivity(intent);
                                    finish();
                                }else {
                                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }else {
                            // if (status.equals("A") || status.equals("R"))
                            if (!PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("LANG", "").equals(null)) {
                                PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).edit().putString("LANG", "en").commit();
                            }
                            if(type.equals("M")){
                                Intent intent = new Intent(SplashActivity.this, MarchandProfileActivity.class);
                                intent.putExtra("id",id);
                                startActivity(intent);
                                finish();
                            }else if(type.equals("P")){
                                Intent intent = new Intent(SplashActivity.this, ProductDetailsActivity.class);
                                intent.putExtra("id",id);
                                startActivity(intent);
                                finish();
                            }else {
                                Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }
                        }



                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ERROR", String.valueOf(error.networkResponse.statusCode));
                if(error.networkResponse.statusCode == 400){
                    Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getAll();
                    for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {
                        if (!prefToReset.getKey().equals("LANG")) {
                            PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).edit().remove(prefToReset.getKey()).commit();
                        }
                    }

                    Intent intent = new Intent(SplashActivity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }else if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(SplashActivity.this);
                }else {
                    MethodClass.error_alert(SplashActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(SplashActivity.this));

                if (PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SplashActivity.this).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(SplashActivity.this).addToRequestQueue(jsonObjectRequest);
    }
}
