package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.ProductDeatilsVariantValueAdapter1;
import com.aswagnacustomer.aswagnacustomer.Adapter.ProductDeatilsVariantValueAdapter2;
import com.aswagnacustomer.aswagnacustomer.Adapter.ProductDeatilsVariantValueAdapter3;
import com.aswagnacustomer.aswagnacustomer.Adapter.ProductDetailsAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.ProductDetailsFeaturAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.SliderAdapterExample;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.Helper.WishListAddRemoveClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.smarteist.autoimageslider.SliderView;
import com.squareup.picasso.Picasso;
import com.willy.ratingbar.ScaleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.COLOR;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FROM_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SElLER_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SHARED_PREF;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TO_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class ProductDetailsActivity extends AppCompatActivity {

    private RecyclerView recy_view_ral_prod;
    private RecyclerView feature_recy;
    private LinearLayout feature_line;
    private LinearLayout savelayout;
    SliderView imageSlider;
    private TextView product_title_tv, newPrice_tv, oldPrice_tv, product_desc_tv;
    private String product_id = "";
    private TextView seller_name, feature_text;
    private CircleImageView seller_pic;
    private TextView vew_pofile_seller;
    private TextView save;
    private RatingBar mer_rating;
    RelativeLayout main_lay;
    LinearLayout share_lin;
    private TextView plus_count_tv, minus_count_tv, count_tv;
    private Button addTOCartBtn;
    private int countF = 1;
    private float priceF = 0;
    private float OldpriceF = 0;
    private ImageView like_img;
    private ImageView hint;

    private LinearLayout variant_lay1, variant_lay2, variant_lay3;
    private TextView variant_tv1, variant_tv2, variant_tv3,pCode;
    public RecyclerView vari_valu_recyc1, vari_valu_recyc2, vari_valu_recyc3;

    public static ArrayList<String> productVariantValueArrayID;
    public static ArrayList<ArrayList<String>> COMBINATION_ARRAY;
    public static ArrayList<String> STOCK_ARRAY;
    public static int All_VARIANT_SIZE;
    public static ArrayList<String> SELECTED_COMMBINATION;
    public boolean iscallActivity;

    ArrayList<HashMap<String, String>> variaArrayList1,variaArrayList2,variaArrayList3;

    private TextView outofstock;
    private NestedScrollView scrollView;
    private ScaleRatingBar simpleRatingBar;

    private LinearLayout notiLayout;
    private EditText notiEmail;
    private Button notiApply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_product_details);

        iscallActivity=true;
        recy_view_ral_prod = findViewById(R.id.recy_view_ral_prod);
        main_lay = findViewById(R.id.main_lay);
        hint = findViewById(R.id.hint);
        main_lay.setVisibility(View.GONE);
        imageSlider = (SliderView) findViewById(R.id.imageSlider);
        product_title_tv = findViewById(R.id.product_title_tv);
        notiLayout = findViewById(R.id.notiLayout);
        notiEmail = findViewById(R.id.notiEmail);
        notiApply = findViewById(R.id.notiApply);

        newPrice_tv = findViewById(R.id.newPrice_tv);
        oldPrice_tv = findViewById(R.id.oldPrice_tv);
        product_desc_tv = findViewById(R.id.product_desc_tv);
        seller_name = findViewById(R.id.seller_name);
        feature_text = findViewById(R.id.feature_text);
        feature_line = findViewById(R.id.feature_line);
        like_img = findViewById(R.id.like_img);
        seller_pic = findViewById(R.id.seller_pic);
        mer_rating = findViewById(R.id.mer_rating);
        scrollView = findViewById(R.id.scrollView);
        pCode = findViewById(R.id.pCode);
        vew_pofile_seller = findViewById(R.id.vew_pofile_seller);
        feature_recy = (RecyclerView) findViewById(R.id.feature_recy);
        share_lin = (LinearLayout) findViewById(R.id.share_lin);
        plus_count_tv = findViewById(R.id.plus_count_tv);
        minus_count_tv = findViewById(R.id.minus_count_tv);
        count_tv = findViewById(R.id.count_tv);
        addTOCartBtn = findViewById(R.id.addTOCartBtn);
        savelayout = findViewById(R.id.savelayout);
        save = findViewById(R.id.save);

        variant_lay1 = findViewById(R.id.variant_lay1);
        variant_lay2 = findViewById(R.id.variant_lay2);
        variant_lay3 = findViewById(R.id.variant_lay3);

        variant_tv1 = findViewById(R.id.variant_tv1);
        variant_tv2 = findViewById(R.id.variant_tv2);
        variant_tv3 = findViewById(R.id.variant_tv3);

        vari_valu_recyc1 = findViewById(R.id.vari_valu_recyc1);
        vari_valu_recyc2 = findViewById(R.id.vari_valu_recyc2);
        vari_valu_recyc3 = findViewById(R.id.vari_valu_recyc3);

        outofstock = findViewById(R.id.outofstock);
        simpleRatingBar = findViewById(R.id.simpleRatingBar);

        product_id = getIntent().getStringExtra("id");




        plus_count_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countF = countF + 1;
                if(oldPrice_tv.getVisibility() == View.VISIBLE){
                    countPriceCal2();
                }else {
                    countPriceCal();
                }

            }
        });
        minus_count_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (countF != 1) {
                    countF = countF - 1;
                    if(oldPrice_tv.getVisibility() == View.VISIBLE){
                        countPriceCal2();
                    }else {
                        countPriceCal();
                    }
                }
            }
        });

        addTOCartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCart();
            }
        });
        notiApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToNoti();
            }
        });
    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(ProductDetailsActivity.this);
        MethodClass.setMenuColor(ProductDetailsActivity.this, "no");
        productVariantValueArrayID = new ArrayList<>();
        COMBINATION_ARRAY = new ArrayList<ArrayList<String>>();
        STOCK_ARRAY = new ArrayList<String>();
        All_VARIANT_SIZE = 0;
        SELECTED_COMMBINATION=new ArrayList<>();
        productDetails();
    }


    private void productDetails() {
        main_lay.setVisibility(View.GONE);
        scrollView.setVisibility(View.GONE);
        if (!isNetworkConnected(ProductDetailsActivity.this)) {
            MethodClass.network_error_alert(ProductDetailsActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ProductDetailsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "product-details";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("product_id", product_id);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("productDetails: ", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("res productDetails", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject product = resultResponse.getJSONObject("product");
                        JSONObject product_details = product.getJSONObject("product_details");

                        String slugss = product_details.getString("slug");
                        share_lin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                //sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID);
                                sendIntent.putExtra(Intent.EXTRA_TEXT, "Checkout our android app: https://www.aswagna.co/api/share?content="+getIntent().getStringExtra("id")+"&type=P"+ "\n\n" +"Checkout our Ios app: https://www.aswagna.co/product/"+slugss);
                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                            }
                        });
                        String product_code = product_details.getString("product_code");
                        pCode.setText(product_code);
                        simpleRatingBar.setRating(Float.parseFloat(product_details.getString("avg_review")));
                        JSONObject product_by_language = product.getJSONObject("product_details").getJSONObject("product_by_language");
                        String product_id = product.getJSONObject("product_details").getString("id");
                        String stock = product.getJSONObject("product_details").getString("stock");
                        if(!stock.equals("0")){
                            showcart();
                        }else {
                            hidecart();
                        }
                        like_img.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in", false)) {
                                    WishListAddRemoveClass.addwishList(ProductDetailsActivity.this,product_id);
                                } else {
                                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_login_first), Snackbar.LENGTH_SHORT);
                                    snackbar.show();
                                }
                            }
                        });
                        String title = product_by_language.getString("title");
                        String description = product_by_language.getString("description");
                        product_title_tv.setText(title);
                        product_desc_tv.setText(Html.fromHtml(description));
                        product_desc_tv.setMovementMethod(LinkMovementMethod.getInstance());
                        float price = Float.parseFloat(product_details.getString("price"));
                        float discount_price = Float.parseFloat(product_details.getString("discount_price"));
                        String from_date = product_details.getString("from_date");
                        String to_date = product_details.getString("to_date");
                        if (discount_price == 0) {
                            newPrice_tv.setText(getString(R.string.kd)+" " + price);
                            priceF = price;
                            countPriceCal();
                            oldPrice_tv.setVisibility(View.GONE);
                        } else {
                            if (from_date.equals("null") || from_date.equals(null) || from_date.equals("")) {
                                newPrice_tv.setText(getString(R.string.kd)+" " + price);
                                priceF = price;
                                countPriceCal();
                                oldPrice_tv.setVisibility(View.GONE);
                            } else {
                                try {
                                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                    Date fromDate = dateFormat.parse(from_date);
                                    Date toDate = dateFormat.parse(to_date);


//                                    Date dateC = Calendar.getInstance().getTime();
//                                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//                                    String formattedDate = df.format(dateC);
//                                    Date currentDate = dateFormat.parse(formattedDate);
                                    Date dCurrentDate = dateFormat.parse(dateFormat.format(new Date()));
                                    Log.e("TIME", String.valueOf(dCurrentDate)+"      "+String.valueOf(fromDate) );
                                    if ((dCurrentDate.equals(fromDate) || dCurrentDate.after(fromDate)) && (dCurrentDate.before(toDate) || dCurrentDate.equals(toDate))) {
                                        priceF = discount_price;
                                        OldpriceF = price;
                                        countPriceCal2();
                                        oldPrice_tv.setVisibility(View.VISIBLE);
                                        savelayout.setVisibility(View.VISIBLE);


                                    } else {
                                        newPrice_tv.setText(getString(R.string.kd)+" " + price);
                                        priceF = price;
                                        countPriceCal();
                                        oldPrice_tv.setVisibility(View.GONE);
                                    }
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                            }
                            //newPrice_tv.setText(getString(R.string.kd)+" " + discount_price);

                        }

                        JSONArray imagesArray = product_details.getJSONArray("images");
                        if(imagesArray.length()>0){
                            Log.e("TOTAL", String.valueOf(imagesArray.length()));
                            ArrayList<HashMap<String, String>> image_arrayList = new ArrayList<>();
                            for (int i = 0; i < imagesArray.length(); i++) {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put(ID, imagesArray.getJSONObject(i).getString("product_id"));
                                hashMap.put(IMAGE, imagesArray.getJSONObject(i).getString("image"));
                                image_arrayList.add(hashMap);
                            }
                            SliderAdapterExample adapter = new SliderAdapterExample(ProductDetailsActivity.this, image_arrayList);
                            imageSlider.setSliderAdapter(adapter);
                            imageSlider.startAutoCycle();
                            imageSlider.setVisibility(View.VISIBLE);
                            hint.setVisibility(View.GONE);
                        }else {
                            hint.setVisibility(View.VISIBLE);
                            imageSlider.setVisibility(View.GONE);
                        }

                        //////////////////////////////////////////////////////////////////////////
                        JSONArray seller_products = resultResponse.getJSONArray("seller_products");
                        ArrayList<HashMap<String, String>> detalsProductArrList = new ArrayList<>();
                        for (int i = 0; i < seller_products.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID, seller_products.getJSONObject(i).getString("id"));
                            hashMap.put(PRICE, seller_products.getJSONObject(i).getString("price"));
                            hashMap.put(DISCOUNT_PRICE, seller_products.getJSONObject(i).getString("discount_price"));
                            hashMap.put(FROM_DATE, seller_products.getJSONObject(i).getString("from_date"));
                            hashMap.put(TO_DATE, seller_products.getJSONObject(i).getString("to_date"));
                            if (!seller_products.getJSONObject(i).getString("default_image").equals("null")) {
                                hashMap.put(IMAGE, seller_products.getJSONObject(i).getJSONObject("default_image").getString("image"));
                            }
                            hashMap.put(TITLE, seller_products.getJSONObject(i).getJSONObject("product_by_language").getString("title"));
                            detalsProductArrList.add(hashMap);
                        }
                        ProductDetailsAdapter productDetailsAdapter = new ProductDetailsAdapter(ProductDetailsActivity.this, detalsProductArrList);
                        productDetailsAdapter.setHasStableIds(true);
                        recy_view_ral_prod.setAdapter(productDetailsAdapter);
                        recy_view_ral_prod.setFocusable(false);

                        ///////////////////////////////////////////////////////////////////////////
                        JSONArray product_variants = product_details.getJSONArray("product_variants");
                        for (int i = 0; i < product_variants.length(); i++) {

                            if(!product_variants.getJSONObject(0).getString("price").equals("0.000")){
                                float priceS = Float.parseFloat(product_variants.getJSONObject(0).getString("price"));
                                float discount_priceS = Float.parseFloat(product_variants.getJSONObject(0).getString("discount_price"));
                                String from_dateS = product_variants.getJSONObject(0).getString("from_date");
                                String to_dateS = product_variants.getJSONObject(0).getString("to_date");
                                if (discount_priceS == 0) {
                                    newPrice_tv.setText(getString(R.string.kd)+" " + priceS);
                                    priceF = priceS;
                                    countPriceCal();
                                    oldPrice_tv.setVisibility(View.GONE);
                                } else {
                                    if (from_dateS.equals("null") || from_dateS.equals(null) || from_dateS.equals("")) {
                                        newPrice_tv.setText(getString(R.string.kd)+" " + priceS);
                                        priceF = priceS;
                                        countPriceCal();
                                        oldPrice_tv.setVisibility(View.GONE);
                                    } else {
                                        try {
                                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                            Date fromDate = dateFormat.parse(from_dateS);
                                            Date toDate = dateFormat.parse(to_dateS);


//                                    Date dateC = Calendar.getInstance().getTime();
//                                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//                                    String formattedDate = df.format(dateC);
//                                    Date currentDate = dateFormat.parse(formattedDate);
                                            Date dCurrentDate = dateFormat.parse(dateFormat.format(new Date()));
                                            Log.e("TIME", String.valueOf(dCurrentDate)+"      "+String.valueOf(fromDate) );
                                            if ((dCurrentDate.equals(fromDate) || dCurrentDate.after(fromDate)) && (dCurrentDate.before(toDate) || dCurrentDate.equals(toDate))) {
                                                priceF = discount_priceS;
                                                OldpriceF = priceS;
                                                countPriceCal2();
                                                oldPrice_tv.setVisibility(View.VISIBLE);
                                                savelayout.setVisibility(View.VISIBLE);


                                            } else {
                                                newPrice_tv.setText(getString(R.string.kd)+" " + priceS);
                                                priceF = priceS;
                                                countPriceCal();
                                                oldPrice_tv.setVisibility(View.GONE);
                                            }
                                        } catch (ParseException e) {
                                            e.printStackTrace();
                                        }

                                    }
                                    //newPrice_tv.setText(getString(R.string.kd)+" " + discount_price);

                                }
                            }


                            String stock_quantity = product_variants.getJSONObject(i).getString("stock_quantity");
                            String str1 = product_variants.getJSONObject(i).getString("variants").replace("]","");
                            String strfinal = str1.replace("[","");
                            String[] tempArray = TextUtils.split(strfinal, ",");
                            ArrayList<String> strings = new ArrayList<>();
                            for (int j = 0; j < tempArray.length; j++) {
                                strings.add(tempArray[j]);
                            }
                            COMBINATION_ARRAY.add(strings);
                            STOCK_ARRAY.add(stock_quantity);
                        }

                        if (COMBINATION_ARRAY.size()>0){
                            SELECTED_COMMBINATION=COMBINATION_ARRAY.get(0);
                            if (STOCK_ARRAY.get(0).equals("0")){
                                hidecart();
                            }else {
                                showcart();
                            }
                            System.out.println(COMBINATION_ARRAY);
                        }
                        Log.e("COMBINATION_ARRAY", COMBINATION_ARRAY.toString());


                        JSONArray all_variants = product.getJSONArray("all_variants");
                        Log.e("all_variants", String.valueOf(all_variants.length()));
                        if (all_variants.length() > 0) {
                            JSONObject object = all_variants.getJSONObject(0);
                            String variant_id = object.getString("id");
                            String variant_name = object.getJSONObject("variant_by_language").getString("name");
                            variant_tv1.setText(variant_name);
                            JSONArray variantValues = object.getJSONArray("variant_values");
                            variaArrayList1=new ArrayList<>();
                            for (int j = 0; j < variantValues.length(); j++) {
                                String variant_value_id = variantValues.getJSONObject(j).getString("variant_value_id");
                                String variant_value_name = variantValues.getJSONObject(j).getJSONObject("variant_value_by_language").getString("name");

                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put(ID, variant_value_id);
                                hashMap.put(NAME, variant_value_name);
                                if(variant_name.equals("Colour")){
                                    String image = variantValues.getJSONObject(j).getJSONObject("variant_value_name").getString("image");
                                    hashMap.put(IMAGE, image);
                                    hashMap.put(COLOR,"Y");
                                }else {
                                    hashMap.put(COLOR,"N");
                                }

                                variaArrayList1.add(hashMap);
                            }
                            ProductDeatilsVariantValueAdapter1 valueAdapter1 = new ProductDeatilsVariantValueAdapter1(ProductDetailsActivity.this, variaArrayList1);
                            vari_valu_recyc1.setAdapter(valueAdapter1);
                            vari_valu_recyc1.setFocusable(false);
                            variant_lay1.setVisibility(View.VISIBLE);
                        } else {
                            variant_lay1.setVisibility(View.GONE);
                        }
                        if (all_variants.length() > 1) {
                            JSONObject object = all_variants.getJSONObject(1);
                            String variant_id = object.getString("id");
                            String variant_name = object.getJSONObject("variant_by_language").getString("name");
                            variant_tv2.setText(variant_name);
                            variaArrayList2 = new ArrayList<>();
                            JSONArray variantValues = object.getJSONArray("variant_values");
                            for (int j = 0; j < variantValues.length(); j++) {
                                String variant_value_id = variantValues.getJSONObject(j).getString("variant_value_id");
                                String variant_value_name = variantValues.getJSONObject(j).getJSONObject("variant_value_by_language").getString("name");
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put(ID, variant_value_id);
                                hashMap.put(NAME, variant_value_name);
                                if(variant_name.equals("Colour")){
                                    String image = variantValues.getJSONObject(j).getJSONObject("variant_value_name").getString("image");
                                    hashMap.put(IMAGE, image);
                                    hashMap.put(COLOR,"Y");
                                }else {
                                    hashMap.put(COLOR,"N");
                                }
                                variaArrayList2.add(hashMap);
                            }
                            ProductDeatilsVariantValueAdapter2 valueAdapter2 = new ProductDeatilsVariantValueAdapter2(ProductDetailsActivity.this, variaArrayList2);
                            vari_valu_recyc2.setAdapter(valueAdapter2);
                            vari_valu_recyc2.setFocusable(false);
                            variant_lay2.setVisibility(View.VISIBLE);
                        } else {
                            variant_lay2.setVisibility(View.GONE);
                        }
                        if (all_variants.length() > 2) {
                            JSONObject object = all_variants.getJSONObject(2);
                            String variant_id = object.getString("id");
                            String variant_name = object.getJSONObject("variant_by_language").getString("name");
                            variant_tv3.setText(variant_name);
                            variaArrayList3 = new ArrayList<>();
                            JSONArray variantValues = object.getJSONArray("variant_values");
                            for (int j = 0; j < variantValues.length(); j++) {
                                String variant_value_id = variantValues.getJSONObject(j).getString("variant_value_id");
                                String variant_value_name = variantValues.getJSONObject(j).getJSONObject("variant_value_by_language").getString("name");
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put(ID, variant_value_id);
                                hashMap.put(NAME, variant_value_name);
                                if(variant_name.equals("Colour")){
                                    String image = variantValues.getJSONObject(j).getJSONObject("variant_value_name").getString("image");
                                    hashMap.put(IMAGE, image);
                                    hashMap.put(COLOR,"Y");
                                }else {
                                    hashMap.put(COLOR,"N");
                                }
                                variaArrayList3.add(hashMap);
                            }
                            ProductDeatilsVariantValueAdapter3 valueAdapter3 = new ProductDeatilsVariantValueAdapter3(ProductDetailsActivity.this, variaArrayList3);
                            vari_valu_recyc3.setAdapter(valueAdapter3);
                            vari_valu_recyc3.setFocusable(false);
                            variant_lay3.setVisibility(View.VISIBLE);
                        } else {
                            variant_lay3.setVisibility(View.GONE);
                        }

                        ///////////////////////////////////////
                        JSONObject seller = resultResponse.getJSONObject("seller");
                        final String id = seller.getString("id");
                        String slug = seller.getString("slug");
                        String fname = seller.getString("fname");
                        String lname = seller.getString("lname");
                        String company_name = seller.getString("company_name");
                        String rate = seller.getString("rate");
                        String image = seller.getString("image");
                        mer_rating.setRating(Float.parseFloat(rate));
                        seller_name.setText(fname + " " + lname);
                        Picasso.get().load(SElLER_IMAGE_URL + image).error(R.drawable.person_icon).placeholder(R.drawable.person_icon).into(seller_pic);
                        vew_pofile_seller.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(ProductDetailsActivity.this, MarchandProfileActivity.class);
                                intent.putExtra("id", id);
                                startActivity(intent);
                            }
                        });
                        //////////////////////////////////////////////////////////////////
                        JSONArray product_features = product.getJSONArray("product_features");
                        if (product_features.length() > 0) {
                            ArrayList<HashMap<String, String>> feacherArrayList = new ArrayList<>();
                            for (int i = 0; i < product_features.length(); i++) {
                                String name = product_features.getJSONObject(i).getJSONObject("product_variant_by_language").getJSONObject("variant_by_language").getString("name");

                                //Log.e("VARIANT", product_features.getJSONObject(i).getJSONObject("product_variant_by_language").getJSONObject("variant_by_language").getString("product_variant_details"));
                                JSONArray valueArray = product_features.getJSONObject(i).getJSONArray("product_variant_details");
                                ArrayList<String> strings = new ArrayList<>();
                                for (int j = 0; j < valueArray.length(); j++) {
                                    String value = valueArray.getJSONObject(j).getJSONObject("variant_value_by_language").getString("name");
                                    strings.add(value);
                                }
                                HashMap<String, String> hashMap = new HashMap<>();
                                Log.e("NAME", name );
                                hashMap.put(NAME, name);
                                hashMap.put(VARY_VALUE, TextUtils.join(",", strings));
                                feacherArrayList.add(hashMap);
                            }
                            ProductDetailsFeaturAdapter featurAdapter = new ProductDetailsFeaturAdapter(ProductDetailsActivity.this, feacherArrayList);
                            feature_recy.setAdapter(featurAdapter);
                            feature_recy.setFocusable(false);
                            feature_text.setVisibility(View.VISIBLE);
                            feature_line.setVisibility(View.VISIBLE);
                        } else {
                            feature_text.setVisibility(View.GONE);
                            feature_line.setVisibility(View.GONE);
                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                                main_lay.setVisibility(View.VISIBLE);
                                scrollView.setVisibility(View.VISIBLE);
                            }},2000);

                    }
                } catch (JSONException e) {
                    MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                    MethodClass.error_alert(ProductDetailsActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProductDetailsActivity.this);
                } else {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(ProductDetailsActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void product_price() {
        if (!isNetworkConnected(ProductDetailsActivity.this)) {
            MethodClass.network_error_alert(ProductDetailsActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ProductDetailsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "product-price";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("product_id", product_id);
        params.put("value_id", "[" + TextUtils.join(",", productVariantValueArrayID) + "]");
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("product-priceqqq: ", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject product = resultResponse.getJSONObject("product");
                        if(!product.getString("price").equals("0.000")){
                            float price = Float.parseFloat(product.getString("price"));
                            float discount_price = Float.parseFloat(product.getString("discount_price"));
                            String from_date = product.getString("from_date");
                            String to_date = product.getString("to_date");
                            if (discount_price == 0) {
                                newPrice_tv.setText(getString(R.string.kd)+" " + price);
                                priceF = price;
                                countPriceCal();
                                oldPrice_tv.setVisibility(View.GONE);
                            } else {
                                if (from_date.equals("null") || from_date.equals(null) || from_date.equals("")) {
                                    newPrice_tv.setText(getString(R.string.kd)+" " + price);
                                    priceF = price;
                                    countPriceCal();
                                    oldPrice_tv.setVisibility(View.GONE);
                                } else {
                                    try {
                                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                                        Date fromDate = dateFormat.parse(from_date);
                                        Date toDate = dateFormat.parse(to_date);


//                                    Date dateC = Calendar.getInstance().getTime();
//                                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
//                                    String formattedDate = df.format(dateC);
//                                    Date currentDate = dateFormat.parse(formattedDate);
                                        Date dCurrentDate = dateFormat.parse(dateFormat.format(new Date()));
                                        Log.e("TIME", String.valueOf(dCurrentDate)+"      "+String.valueOf(fromDate) );
                                        if ((dCurrentDate.equals(fromDate) || dCurrentDate.after(fromDate)) && (dCurrentDate.before(toDate) || dCurrentDate.equals(toDate))) {
                                            priceF = discount_price;
                                            OldpriceF = price;
                                            countPriceCal2();
                                            oldPrice_tv.setVisibility(View.VISIBLE);
                                            savelayout.setVisibility(View.VISIBLE);


                                        } else {
                                            newPrice_tv.setText(getString(R.string.kd)+" " + price);
                                            priceF = price;
                                            countPriceCal();
                                            oldPrice_tv.setVisibility(View.GONE);
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                }
                                //newPrice_tv.setText(getString(R.string.kd)+" " + discount_price);

                            }
                        }

                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProductDetailsActivity.this);
                } else {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(ProductDetailsActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void addToCart() {
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if (!isNetworkConnected(ProductDetailsActivity.this)) {
            MethodClass.network_error_alert(ProductDetailsActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ProductDetailsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "add-to-cart";
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("product_id", product_id);
        params.put("quantity", String.valueOf(countF));
        //params.put("variants", "[" + TextUtils.join(",", productVariantValueArrayID) + "]");
        params.put("variants", productVariantValueArrayID);
        if (!PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in", false)) {
            params.put("device_id", android_id);
        }
        JSONObject jsonObject = MethodClass.Json_rpc_format_obj(params);
        Log.e("addToCart", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                Log.e("addToCartRES", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject message = resultResponse.getJSONObject("message");
                        new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(message.getString("message"))
                                .setContentText(message.getString("meaning"))
                                .setConfirmText(getResources().getString(R.string.done))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                }).show();
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProductDetailsActivity.this);
                } else {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(ProductDetailsActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void countPriceCal() {
        count_tv.setText(String.valueOf(countF));
        float finalPrice = priceF * countF;
        newPrice_tv.setText(getResources().getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f",finalPrice));
    }
    private void countPriceCal2() {
        count_tv.setText(String.valueOf(countF));
        float finalPrice = priceF * countF;
        float OldfinalPrice = OldpriceF * countF;
        float finalPrices = Math.round(((OldpriceF - priceF) / OldpriceF) * 100);


        oldPrice_tv.setText(getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f",OldfinalPrice) );
        newPrice_tv.setText(getString(R.string.kd)+" " + String.format(Locale.ENGLISH,"%.3f",finalPrice) );

        Double aprice = Double.valueOf(OldpriceF);
        Double bprice = Double.valueOf(priceF);
        save.setText(" "+getString(R.string.kd)+" "+String.format(Locale.ENGLISH,"%.3f", ((aprice-bprice)*countF))+"("+String.valueOf(finalPrices)+getString(R.string.per_off)+")");

    }

    public void updateApdater1(){
        if(COMBINATION_ARRAY.contains(SELECTED_COMMBINATION)){

            if (STOCK_ARRAY.get(COMBINATION_ARRAY.indexOf(SELECTED_COMMBINATION)).equals("0")){
                hidecart();
            }else {
                showcart();
            }
        }
        Log.e("Call","111111111");
        vari_valu_recyc2.setAdapter(null);

        ProductDeatilsVariantValueAdapter2 valueAdapter2 = new ProductDeatilsVariantValueAdapter2(ProductDetailsActivity.this, variaArrayList2);
        vari_valu_recyc2.setAdapter(valueAdapter2);
        vari_valu_recyc2.setFocusable(false);

        vari_valu_recyc3.setAdapter(null);
        ProductDeatilsVariantValueAdapter3 valueAdapter3 = new ProductDeatilsVariantValueAdapter3(ProductDetailsActivity.this, variaArrayList3);
        vari_valu_recyc3.setAdapter(valueAdapter3);
        vari_valu_recyc3.setFocusable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                product_price();
            }
        },500);
    }
    public void updateApdater2(){
        if(COMBINATION_ARRAY.contains(SELECTED_COMMBINATION)){

            if (STOCK_ARRAY.get(COMBINATION_ARRAY.indexOf(SELECTED_COMMBINATION)).equals("0")){
                hidecart();
            }else {
                showcart();
            }
        }
        Log.e("Call","22222222");
        vari_valu_recyc1.setAdapter(null);
        ProductDeatilsVariantValueAdapter1 valueAdapter1 = new ProductDeatilsVariantValueAdapter1(ProductDetailsActivity.this, variaArrayList1);
        vari_valu_recyc1.setAdapter(valueAdapter1);
        vari_valu_recyc1.setFocusable(false);

        vari_valu_recyc3.setAdapter(null);
        ProductDeatilsVariantValueAdapter3 valueAdapter3 = new ProductDeatilsVariantValueAdapter3(ProductDetailsActivity.this, variaArrayList3);
        vari_valu_recyc3.setAdapter(valueAdapter3);
        vari_valu_recyc3.setFocusable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                product_price();
            }
        },500);

    }
    public void updateApdater3(){
        if(COMBINATION_ARRAY.contains(SELECTED_COMMBINATION)){

            Log.e("Index", String.valueOf(COMBINATION_ARRAY.indexOf(SELECTED_COMMBINATION)));
            if (STOCK_ARRAY.get(COMBINATION_ARRAY.indexOf(SELECTED_COMMBINATION)).equals("0")){
                hidecart();
            }else {
                showcart();
            }
        }
        Log.e("Call","33333333");
        vari_valu_recyc1.setAdapter(null);
        ProductDeatilsVariantValueAdapter1 valueAdapter1 = new ProductDeatilsVariantValueAdapter1(ProductDetailsActivity.this, variaArrayList1);
        vari_valu_recyc1.setAdapter(valueAdapter1);
        vari_valu_recyc1.setFocusable(false);

        vari_valu_recyc2.setAdapter(null);
        ProductDeatilsVariantValueAdapter2 valueAdapter2 = new ProductDeatilsVariantValueAdapter2(ProductDetailsActivity.this, variaArrayList2);
        vari_valu_recyc2.setAdapter(valueAdapter2);
        vari_valu_recyc2.setFocusable(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                product_price();
            }
        },500);

    }
    public void scrollToPOs1(int pos){
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(ProductDetailsActivity.this) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        smoothScroller.setTargetPosition(pos);
        ((LinearLayoutManager) vari_valu_recyc1.getLayoutManager()).startSmoothScroll(smoothScroller);
    }
    public void scrollToPOs2(int pos){
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(ProductDetailsActivity.this) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        smoothScroller.setTargetPosition(pos);
        ((LinearLayoutManager) vari_valu_recyc2.getLayoutManager()).startSmoothScroll(smoothScroller);
    }
    public void scrollToPOs3(int pos){
        RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(ProductDetailsActivity.this) {
            @Override protected int getVerticalSnapPreference() {
                return LinearSmoothScroller.SNAP_TO_START;
            }
        };

        smoothScroller.setTargetPosition(pos);
        ((LinearLayoutManager) vari_valu_recyc3.getLayoutManager()).startSmoothScroll(smoothScroller);
    }
    public void showcart(){
        addTOCartBtn.setVisibility(View.VISIBLE);
        outofstock.setVisibility(View.GONE);
        notiLayout.setVisibility(View.GONE);
    }
    public void hidecart(){
        addTOCartBtn.setVisibility(View.GONE);
        outofstock.setVisibility(View.VISIBLE);
        if (PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in", false)) {
            notiLayout.setVisibility(View.VISIBLE);
            notiEmail.setVisibility(View.GONE);
            notiApply.setVisibility(View.VISIBLE);
        } else {
            notiLayout.setVisibility(View.VISIBLE);
            notiEmail.setVisibility(View.VISIBLE);
            notiApply.setVisibility(View.VISIBLE);
        }
    }


    public void addToNoti(){
        if (!PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in", false)) {
            if(!MethodClass.emailValidator(notiEmail.getText().toString().trim())){
                notiEmail.setError(getString(R.string.please_enter_email_address));
                notiEmail.requestFocus();
                return;
            }

        }

        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        if (!isNetworkConnected(ProductDetailsActivity.this)) {
            MethodClass.network_error_alert(ProductDetailsActivity.this);
            return;
        }
        SharedPreferences pref = getSharedPreferences(SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        MethodClass.showProgressDialog(ProductDetailsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "update-firebase-token";
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("product_id", product_id);
        params.put("device_type", "A");
        params.put("firebase_token", regId);
        if (!PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in", false)) {
            params.put("email", notiEmail.getText().toString().trim());
        }
        //params.put("variants", "[" + TextUtils.join(",", productVariantValueArrayID) + "]");
        params.put("product_variant_id", productVariantValueArrayID);
        if (!PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in", false)) {
            params.put("device_id", android_id);
        }
        JSONObject jsonObject = MethodClass.Json_rpc_format_obj(params);
        Log.e("addToCart", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                Log.e("addToCartRES", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProductDetailsActivity.this, response);
                    if (resultResponse != null) {
                        if(resultResponse.has("message")){
                            String message = resultResponse.getString("message");
                            new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.WARNING_TYPE)
                                    .setTitleText(getString(R.string.reminderAlladded))
                                    .setContentText(message)
                                    .setConfirmText(getResources().getString(R.string.done))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    }).show();
                        }else {
                            String message = resultResponse.getString("email");
                            new SweetAlertDialog(ProductDetailsActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText(getString(R.string.reminderadded))
                                    .setContentText(getString(R.string.youwillno))
                                    .setConfirmText(getResources().getString(R.string.done))
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sDialog) {
                                            sDialog.dismissWithAnimation();
                                        }
                                    }).show();
                        }

                    }
                } catch (Exception e) {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ProductDetailsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProductDetailsActivity.this);
                } else {
                    MethodClass.error_alert(ProductDetailsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(ProductDetailsActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProductDetailsActivity.this).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ProductDetailsActivity.this).addToRequestQueue(jsonObjectRequest);

    }

}












 /* ArrayList<HashMap<String, String>> variantProductArrList = new ArrayList<>();
                        for (int j = 0; j < all_variants.length(); j++) {
                            String variant_id = all_variants.getJSONObject(j).getString("id");
                            String variant_name = all_variants.getJSONObject(j).getJSONObject("variant_by_language").getString("name");
                            String variant_value = all_variants.getJSONObject(j).getString("variant_values");
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID, variant_id);
                            hashMap.put(NAME, variant_name);
                            hashMap.put(VARY_VALUE, variant_value);
                            variantProductArrList.add(hashMap);
                        }
                        ProductDetailsVariantAdapter variantAdapter = new ProductDetailsVariantAdapter(ProductDetailsActivity.this, variantProductArrList);
                 */       /*variant_recy.setAdapter(variantAdapter);
                        variant_recy.setFocusable(false);*/