package com.aswagnacustomer.aswagnacustomer.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class ConactUsActivity extends AppCompatActivity {

    private TextView email_tv,phone_tv,address_tv;
    private EditText first_name_et,last_name_et,email_et,phone_et,msg_et;
    private Button send_btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_contact_us);
        email_tv=findViewById(R.id.email_tv);
        phone_tv=findViewById(R.id.phone_tv);
        address_tv=findViewById(R.id.address_tv);
        first_name_et=findViewById(R.id.first_name_et);
        last_name_et=findViewById(R.id.last_name_et);
        email_et=findViewById(R.id.email_et);
        phone_et=findViewById(R.id.phone_et);
        msg_et=findViewById(R.id.msg_et);
        send_btn=findViewById(R.id.send_btn);
        data();
        send_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fname, lname, phone, email, msg;
                fname = first_name_et.getText().toString().trim();
                lname = last_name_et.getText().toString().trim();
                phone = phone_et.getText().toString().trim();
                email = email_et.getText().toString().trim();
                msg = msg_et.getText().toString().trim();
                if (fname.length() == 0) {
                    first_name_et.setError(getResources().getString(R.string.please_enter_first_name));
                    first_name_et.requestFocus();
                    return;
                }
                if (lname.length() == 0) {
                    last_name_et.setError(getResources().getString(R.string.please_enter_last_name));
                    last_name_et.requestFocus();
                    return;
                }
                if (phone.length() == 0) {
                    phone_et.setError(getResources().getString(R.string.please_enter_mobile_no));
                    phone_et.requestFocus();
                    return;
                }
                if (email.length() == 0) {
                    email_et.setError(getResources().getString(R.string.please_enter_email_address));
                    email_et.requestFocus();
                    return;
                }
                if (!MethodClass.emailValidator(email)) {
                    email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
                    email_et.requestFocus();
                    return;
                }
                if (msg.length() == 0) {
                    msg_et.setError(getResources().getString(R.string.please_enter_your_message));
                    msg_et.requestFocus();
                    return;
                }

                sendMSG();

            }
        });
    }
    public void back(View view) {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(ConactUsActivity.this);
        MethodClass.setMenuColor(ConactUsActivity.this,"no");
    }

    public void sendMSG() {
        if (!isNetworkConnected(ConactUsActivity.this)) {
            MethodClass.network_error_alert(ConactUsActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ConactUsActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "post-contact-us-form";
        Log.e("server_url", server_url);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("firstname", first_name_et.getText().toString().trim());
        hashMap.put("lastname", last_name_et.getText().toString().trim());
        hashMap.put("email", email_et.getText().toString().trim());
        hashMap.put("phone", phone_et.getText().toString().trim());
        hashMap.put("message", msg_et.getText().toString().trim());
        JSONObject jsonObject = MethodClass.Json_rpc_format(hashMap);

        Log.e("jsonObject", jsonObject.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ConactUsActivity.this);
                Log.e("ChekoutResponceRES", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(ConactUsActivity.this, response);
                    SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(ConactUsActivity.this, SweetAlertDialog.SUCCESS_TYPE);
                    sweetAlertDialog.setTitleText(resultResponse.getJSONObject("success").getString("message"));
                    sweetAlertDialog.setContentText(resultResponse.getJSONObject("success").getString("meaning"));
                    sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                    sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                        }
                    });
                    sweetAlertDialog.show();
                } catch (Exception e) {
                    MethodClass.error_alert(ConactUsActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(ConactUsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ConactUsActivity.this);
                } else {
                    MethodClass.error_alert(ConactUsActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(ConactUsActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ConactUsActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ConactUsActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void data() {
        String server_url = getString(R.string.SERVER_URL) + "show-contact-us";
        HashMap<String, Object> hashMap = new HashMap<>();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, MethodClass.Json_rpc_format_obj(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ConactUsActivity.this);
                Log.e("respLogin", response.toString());
                try {
                    JSONObject resultResponce = MethodClass.get_result_from_webservice(ConactUsActivity.this, response);
                    if (resultResponce != null) {
                        JSONObject contact_content = resultResponce.getJSONObject("contact_content");
                        String email = contact_content.getString("email");
                        String phone = contact_content.getString("phone");
                        String address = contact_content.getString("address");
                        email_tv.setText(email);
                        phone_tv.setText(phone);
                        address_tv.setText(address);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ConactUsActivity.this);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(ConactUsActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ConactUsActivity.this);
                } else {
                    MethodClass.error_alert(ConactUsActivity.this);
                }
            }
        });
        MySingleton.getInstance(ConactUsActivity.this).addToRequestQueue(jsonObjectRequest);
    }


}
