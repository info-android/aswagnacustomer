package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Adapter.OrderHistoryAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_ITEM;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_TOTAL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_TYPE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.STATUS;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class OrderHistoryActivity extends AppCompatActivity {

    private RecyclerView recy_view;
    private ArrayList<HashMap<String,String>> arrayList ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_order_history);
        recy_view=findViewById(R.id.recy_view);
        order_history();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(OrderHistoryActivity.this);
        MethodClass.setMenuColor(OrderHistoryActivity.this,"order");
    }

    public void back(View view) {
        if(isTaskRoot()){
            startActivity(new Intent(OrderHistoryActivity.this,HomeActivity.class));
            // using finish() is optional, use it if you do not want to keep currentActivity in stack
            finish();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        if(isTaskRoot()){
            startActivity(new Intent(OrderHistoryActivity.this,HomeActivity.class));
            // using finish() is optional, use it if you do not want to keep currentActivity in stack
            finish();
        }else{
            super.onBackPressed();
        }
    }

    private void order_history() {
        if (!isNetworkConnected(OrderHistoryActivity.this)) {
            MethodClass.network_error_alert(OrderHistoryActivity.this);
            return;
        }
        MethodClass.showProgressDialog(OrderHistoryActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "order-history";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(OrderHistoryActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(OrderHistoryActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray orders = resultResponse.getJSONArray("orders");
                        arrayList=new ArrayList<>();
                        for (int i = 0; i < orders.length(); i++) {
                            HashMap<String,String>  hashMap=new HashMap<>();
                            hashMap.put(ID,orders.getJSONObject(i).getString("id"));
                            hashMap.put(ORDER_NO,orders.getJSONObject(i).getString("order_no"));
                            hashMap.put(CREATED_AT,orders.getJSONObject(i).getString("created_at"));
                            hashMap.put(ORDER_TOTAL,orders.getJSONObject(i).getString("order_total"));
                            hashMap.put(PAYMENT_METHODE,orders.getJSONObject(i).getString("payment_method"));
                            hashMap.put(STATUS,orders.getJSONObject(i).getString("status"));
                            hashMap.put(ORDER_TYPE,orders.getJSONObject(i).getString("order_type"));
                            hashMap.put(ORDER_ITEM, ""+orders.getJSONObject(i).getJSONArray("order_master_details").length());
                            arrayList.add(hashMap);
                        }
                        OrderHistoryAdapter historyAdapter=new OrderHistoryAdapter(OrderHistoryActivity.this,arrayList);
                        recy_view.setAdapter(historyAdapter);
                        recy_view.setFocusable(false);

                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(OrderHistoryActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(OrderHistoryActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(OrderHistoryActivity.this);
                } else {
                    MethodClass.error_alert(OrderHistoryActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(OrderHistoryActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(OrderHistoryActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(OrderHistoryActivity.this).addToRequestQueue(jsonObjectRequest);
    }
}
