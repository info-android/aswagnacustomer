package com.aswagnacustomer.aswagnacustomer.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SHARED_PREF;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.error_alert;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.showProgressDialog;

public class SignUpActivity extends AppCompatActivity {

    private EditText full_name_et, email_et, mobile_et, password_et, confirm_pass;
    private ImageView pass_hide_show_img, cof_pass_hide_show_img;
    private LinearLayout google_btn_lin, facebook_btn_lin;
    private Integer ASWAGNA_SIGN_IN = 1;
    private SignInButton google_button;
    private LoginButton facebook_login_button;
    CallbackManager callbackManager;
    private String g_first_name, g_last_name, g_id, g_email, g_photo = "";
    private String f_first_name, f_last_name, f_id, f_email, f_photo = "";
    private TextView term_and_con_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());//here initialize Facebook Sdk
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_sign);
        full_name_et = findViewById(R.id.full_name_et);
        email_et = findViewById(R.id.email_et);
        mobile_et = findViewById(R.id.mobile_et);
        password_et = findViewById(R.id.password_et);
        confirm_pass = findViewById(R.id.confirm_pass);
        term_and_con_tv = findViewById(R.id.term_and_con_tv);
        cof_pass_hide_show_img = findViewById(R.id.cof_pass_hide_show_img);
        pass_hide_show_img = findViewById(R.id.pass_hide_show_img);
        google_btn_lin = findViewById(R.id.google_btn_lin);
        google_button = findViewById(R.id.google_button);
        facebook_login_button = findViewById(R.id.facebook_login_button);
        facebook_btn_lin = findViewById(R.id.facebook_btn_lin);

        SpannableStringBuilder spanTxt = new SpannableStringBuilder("By creating an account, you agree to Aswagna ");
        spanTxt.append("Privacy Policy");
        spanTxt.setSpan(new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(ContextCompat.getColor(SignUpActivity.this, R.color.colorPrimary));
            }
            @Override
            public void onClick(@NonNull View widget) {
              privacyCon();
            }
        }, spanTxt.length() - "Privacy Policy".length(), spanTxt.length(), 0);

        spanTxt.append(" and ");
        spanTxt.append("Terms of Use");

        spanTxt.setSpan(new ClickableSpan() {

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setColor(ContextCompat.getColor(SignUpActivity.this, R.color.colorPrimary));
            }
            @Override
            public void onClick(@NonNull View widget) {
                termCon();
            }
        }, spanTxt.length() - "Terms of Use".length(), spanTxt.length(), 0);
        term_and_con_tv.setMovementMethod(LinkMovementMethod.getInstance());
        term_and_con_tv.setText(spanTxt, TextView.BufferType.SPANNABLE);

        pass_hide_show_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password_et.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    pass_hide_show_img.setImageDrawable(getResources().getDrawable(R.drawable.icon6));
                    password_et.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    password_et.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    pass_hide_show_img.setImageDrawable(getResources().getDrawable(R.drawable.view));
                    password_et.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    password_et.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });
        cof_pass_hide_show_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (confirm_pass.getInputType() == InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD) {
                    cof_pass_hide_show_img.setImageDrawable(getResources().getDrawable(R.drawable.icon6));
                    confirm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    confirm_pass.setTransformationMethod(new PasswordTransformationMethod());
                } else {
                    cof_pass_hide_show_img.setImageDrawable(getResources().getDrawable(R.drawable.view));
                    confirm_pass.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    confirm_pass.setTransformationMethod(new HideReturnsTransformationMethod());
                }
            }
        });


        //here google sign in create option
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        final GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);//here create google sign client

        //here google button click event
        google_btn_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //here check my google have loged in or not if loged in first log outthen log in
                if (mGoogleSignInClient != null) {
                    mGoogleSignInClient.signOut();
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, ASWAGNA_SIGN_IN);
                } else {
                    //here first log in with google
                    Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                    startActivityForResult(signInIntent, ASWAGNA_SIGN_IN);
                }

            }
        });

        //here Facebook callback Manager
        callbackManager = CallbackManager.Factory.create();
        //here facebook login click button
        facebook_btn_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Profile profile = Profile.getCurrentProfile().getCurrentProfile();
                if (profile != null) {
                    // user has logged in
                    LoginManager.getInstance().logOut();
                    facebook_login_button.performClick();
                } else {
                    // user has not logged in
                    facebook_login_button.performClick();
                }
            }
        });

        //here facebook call back when come the response
        facebook_login_button.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                System.out.println("onSuccess");
                String accessToken = loginResult.getAccessToken().getToken();//here facebook accessToken provided by facebook
                Log.i("accessToken", accessToken);

                //here create GraphRequest for get user information  with passing accessToken (loginResult.getAccessToken())
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        //this is response  method of facebook GraphRequest in this method got response(user details)
                        Log.i("SignUpActivity", response.toString());
                        try {
                            //here get facebook user data like id and name;
                            f_id = object.getString("id");
                            f_first_name = object.getString("first_name");
                            f_last_name = object.getString("last_name");
                            f_photo = "";
                            JSONObject data = response.getJSONObject();
                            if (data.has("picture")) {
                                f_photo = data.getJSONObject("picture").getJSONObject("data").getString("url");
                            }
                            f_email = "";
                            if (object.has("email")) {
                                f_email = object.getString("email");
                                facebookLogin();
                            } else {
                                getEmailadress("F");
                            }

                        } catch (JSONException e) {
                            //here facebook graph request JSONException
                            Log.e("facebookGraphExce", e.toString());
                            e.printStackTrace();
                            //Toast.makeText(SignUpActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                //here request user data parameter by Bundle
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender, birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();//here send request to facebook server
            }

            //here facebook onCancel method
            @Override
            public void onCancel() {
                System.out.println("onCancel");
            }

            //here facebook onError method
            @Override
            public void onError(FacebookException exception) {
                //here network problem any type then come this catch function here and show pop up to something wrong
                System.out.println("onError");
                Toast.makeText(SignUpActivity.this, exception.getMessage().toString(), Toast.LENGTH_LONG).show();
                Log.v("SignUpActivity", exception.getMessage().toString());
                Log.e("SignUpActivity", exception.getMessage().toString());
            }
        });

    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void sign_up(View view) {
        String full_name = "", email = "", mobile = "", password = "", conf_password = "";
        full_name = full_name_et.getText().toString().trim();
        email = email_et.getText().toString().trim();
        mobile = mobile_et.getText().toString().trim();
        password = password_et.getText().toString().trim();
        conf_password = confirm_pass.getText().toString().trim();

        if (full_name.length() == 0) {
            full_name_et.setError(getResources().getString(R.string.please_enter_your_first_name));
            full_name_et.requestFocus();
            return;
        }
        if (email.length() == 0) {
            email_et.setError(getResources().getString(R.string.please_enter_your_email_address));
            email_et.requestFocus();
            return;
        }
        if (!MethodClass.emailValidator(email)) {
            email_et.setError(getResources().getString(R.string.invalid_email_address));
            email_et.requestFocus();
            return;
        }
        if (mobile.length() == 0) {
            mobile_et.setError(getResources().getString(R.string.please_enter_valid_number));
            mobile_et.requestFocus();
            return;
        }
        if (password.length() == 0) {
            password_et.setError(getResources().getString(R.string.please_enter_password));
            password_et.requestFocus();
            return;
        }
        if (password.length() < 6) {
            password_et.setError(getResources().getString(R.string.please_enter_minimum_six_password));
            password_et.requestFocus();
            return;
        }
        if (conf_password.length() == 0) {
            confirm_pass.setError(getResources().getString(R.string.please_enter_your_confirm_password));
            confirm_pass.requestFocus();
            return;
        }
        if (!password.equals(conf_password)) {
            confirm_pass.setError(getResources().getString(R.string.password_is_not_matched_with_confirm_password));
            confirm_pass.requestFocus();
            return;
        }

        if (!isNetworkConnected(SignUpActivity.this)) {
            MethodClass.network_error_alert(SignUpActivity.this);
            return;
        }

        showProgressDialog(SignUpActivity.this);
        String url = getString(R.string.SERVER_URL) + "register";
        Log.e("url", url);
        HashMap<String, String> hashMap = new HashMap<>();//create mapping model class to send data of server
        hashMap.put("name", full_name);
        hashMap.put("email", email);
        hashMap.put("phone", mobile);
        hashMap.put("password", password);
        Log.e("register", MethodClass.Json_rpc_format(hashMap).toString());
        final String finalEmail = email;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, MethodClass.Json_rpc_format(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("response", response.toString());
                MethodClass.hideProgressDialog(SignUpActivity.this);
                final JSONObject resultResponce = MethodClass.get_result_from_webservice(SignUpActivity.this, response);
                try {
                    if (resultResponce != null) {
                        Log.e("resultResponce", resultResponce.toString());
                        new SweetAlertDialog(SignUpActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getResources().getString(R.string.success))
                                .setContentText("Registration successful")
                                .setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                                try {
                                    String code = resultResponce.getString("otp");
                                    Intent intent = new Intent(SignUpActivity.this, VerifyActivity.class);
                                    intent.putExtra("email", finalEmail);
                                    intent.putExtra("code", code);
                                    intent.putExtra("type", "S");
                                    startActivity(intent);
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }

                            }
                        }).show();

                    }
                } catch (Exception e) {
                    Log.e("ExceptionElse", e.toString());
                    error_alert(SignUpActivity.this);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("onErrorResponse: ", error.toString());
                MethodClass.hideProgressDialog(SignUpActivity.this);
                if (error.toString().contains("ConnectException") || error.toString().contains("NoConnectionError")) {
                    MethodClass.network_error_alert(SignUpActivity.this);
                } else {
                    Log.e("ErrorElse", " Else");
                    error_alert(SignUpActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(SignUpActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(SignUpActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void login(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }


    //here onActivityResult of this activity for get google response
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);//here call  callbackManager when come from facebook response

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == ASWAGNA_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);//call google handleSignInResult method
        }

        super.onActivityResult(requestCode, resultCode, data);//here call super onActivityResult
    }

    //here create google handleSignInResult
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class); //here create class to get google user data after completed sign in authentication
            // Signed in successfully, show authenticated UI.
            //here fetch user data of google user
            Log.w("Googlesign", account.getEmail());
            Log.w("Googlesign", account.getDisplayName());
            Log.w("Googlesign", account.getGivenName());
            Log.w("Googlesign", account.getFamilyName());
            Log.w("Googlesign", account.getId());
            g_first_name = account.getDisplayName();
            g_last_name = account.getFamilyName();
            g_email = account.getEmail();
            g_id = account.getId();
            if (account.getPhotoUrl() != null) {
                g_photo = account.getPhotoUrl().toString();
                Log.e("g_photp", account.getPhotoUrl().toString());
            } else {
                g_photo = "";
            }
            googleLogin();

        } catch (Exception e) {
            //here google Exception handling function
            e.printStackTrace();
            Log.e("ERRORGoogl", e.toString());
        }
    }

    private void facebookLogin() {
        //here send facebook login (user data) to own server database
        MethodClass.showProgressDialog(SignUpActivity.this);
        SharedPreferences pref = getSharedPreferences(SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String server_url = getString(R.string.SERVER_URL) + "facebook-login";
        HashMap<String, Object> hashMap = new HashMap<>();//create mapping model class to send data of server
        hashMap.put("facebook_id", f_id);
        hashMap.put("name", f_first_name + " " + f_last_name);
        hashMap.put("email", f_email);
        hashMap.put("image", f_photo);
        hashMap.put("device_id", android_id);
        hashMap.put("firebase_reg_no", regId);
        hashMap.put("device_type", "A");
        Log.e("facebooklogin", MethodClass.Json_rpc_format_obj(hashMap).toString());

        //create JsonObjectRequest(communicate to server data) to get response and send data
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, MethodClass.Json_rpc_format_obj(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(SignUpActivity.this);
                Log.e("respLogin", response.toString());
                try {
                    JSONObject resultResponce = MethodClass.get_result_from_webservice(SignUpActivity.this, response);
                    if (resultResponce != null) {
                        String status = resultResponce.getJSONObject("user").getString("status");
                        String token = resultResponce.getString("token");
                        JSONObject userdata = resultResponce.getJSONObject("user");
                        String id = userdata.getString("id");
                        String first_name = userdata.getString("fname");
                        String last_name = userdata.getString("lname");
                        String name = first_name + " " + last_name;
                        String email = userdata.getString("email");
                        String loyalty_balance = userdata.getString("loyalty_balance");
                        String user_type = userdata.getString("user_type");

                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("token", token).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_fname", first_name).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_lname", last_name).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_name", name).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_id", id).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("email", email).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("loyalty_balance", loyalty_balance).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putBoolean("is_logged_in", true).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_type", user_type).commit();
                        Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(SignUpActivity.this);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(SignUpActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(SignUpActivity.this);
                } else {
                    MethodClass.error_alert(SignUpActivity.this);
                }
            }
        });
        MySingleton.getInstance(SignUpActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void googleLogin() {
        SharedPreferences pref = getSharedPreferences(SHARED_PREF, 0);
        String regId = pref.getString("regId", null);
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String server_url = getString(R.string.SERVER_URL) + "google-login";
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("google_id", g_id);
        hashMap.put("name", g_first_name + " " + g_last_name);
        hashMap.put("email", g_email);
        hashMap.put("image", g_photo);
        hashMap.put("device_id", android_id);
        hashMap.put("firebase_reg_no", regId);
        hashMap.put("device_type", "A");
        Log.e("googlelogin", MethodClass.Json_rpc_format_obj(hashMap).toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, MethodClass.Json_rpc_format_obj(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(SignUpActivity.this);
                Log.e("respLogin", response.toString());
                try {
                    JSONObject resultResponce = MethodClass.get_result_from_webservice(SignUpActivity.this, response);
                    if (resultResponce != null) {
                        String token = resultResponce.getString("token");
                        JSONObject userdata = resultResponce.getJSONObject("user");
                        String id = userdata.getString("id");
                        String first_name = userdata.getString("fname");
                        String last_name = userdata.getString("lname");
                        String name = first_name + " " + last_name;
                        String email = userdata.getString("email");
                        String loyalty_balance = userdata.getString("loyalty_balance");
                        String user_type = userdata.getString("user_type");

                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("token", token).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_fname", first_name).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_lname", last_name).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_name", name).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_id", id).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("email", email).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("loyalty_balance", loyalty_balance).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putBoolean("is_logged_in", true).commit();
                        PreferenceManager.getDefaultSharedPreferences(SignUpActivity.this).edit().putString("user_type", user_type).commit();
                        Intent intent = new Intent(SignUpActivity.this, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(SignUpActivity.this);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(SignUpActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(SignUpActivity.this);
                } else {
                    MethodClass.error_alert(SignUpActivity.this);
                }
            }
        });
        MySingleton.getInstance(SignUpActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void getEmailadress(final String callFun) {
        final Dialog dialog = new Dialog(SignUpActivity.this);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.get_email_popup);
        dialog.setCancelable(false);
        final EditText email_et = dialog.findViewById(R.id.email_et);
        ImageView cross_img = dialog.findViewById(R.id.cross_img);
        Button done_btn = dialog.findViewById(R.id.done_btn);
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String get_email = email_et.getText().toString().trim();
                if (get_email.length() == 0) {
                    email_et.setError(getResources().getString(R.string.please_enter_email_address));
                    email_et.requestFocus();
                    return;
                }
                if (!MethodClass.emailValidator(get_email)) {
                    email_et.setError(getResources().getString(R.string.please_enter_valid_email_address));
                    email_et.requestFocus();
                    return;
                }
                if (callFun.equals("F")) {
                    f_email = get_email;
                    facebookLogin();
                }

                dialog.dismiss();
            }
        });

        cross_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private void privacyCon() {
        String server_url = getString(R.string.SERVER_URL) + "show-privacy";
        HashMap<String, Object> hashMap = new HashMap<>();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, MethodClass.Json_rpc_format_obj(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(SignUpActivity.this);
                Log.e("respLogin", response.toString());
                try {
                    JSONObject resultResponce = MethodClass.get_result_from_webservice(SignUpActivity.this, response);
                    if (resultResponce != null) {
                        JSONObject privecy_content = resultResponce.getJSONObject("privecy_content");
                        String title = privecy_content.getString("title");
                        String description = privecy_content.getString("description");

                        final Dialog dialog = new Dialog(SignUpActivity.this);
                        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.term_and_con_lay);
                        dialog.setCancelable(false);
                        ImageView close_iv = dialog.findViewById(R.id.close_iv);
                        TextView title_tv = dialog.findViewById(R.id.title_tv);
                        TextView desc_tv = dialog.findViewById(R.id.desc_tv);
                        title_tv.setText(title);
                        desc_tv.setText(description);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            desc_tv.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            desc_tv.setText(Html.fromHtml(description));
                        }
                        close_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(SignUpActivity.this);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(SignUpActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(SignUpActivity.this);
                } else {
                    MethodClass.error_alert(SignUpActivity.this);
                }
            }
        });
        MySingleton.getInstance(SignUpActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    private void termCon() {
        String server_url = getString(R.string.SERVER_URL) + "show-terms";
        HashMap<String, Object> hashMap = new HashMap<>();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, MethodClass.Json_rpc_format_obj(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(SignUpActivity.this);
                Log.e("respLogin", response.toString());
                try {
                    JSONObject resultResponce = MethodClass.get_result_from_webservice(SignUpActivity.this, response);
                    if (resultResponce != null) {
                        JSONObject term_content = resultResponce.getJSONObject("term_content");
                        String title = term_content.getString("title");
                        String description = term_content.getString("description");

                        final Dialog dialog = new Dialog(SignUpActivity.this);
                        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.term_and_con_lay);
                        dialog.setCancelable(false);
                        ImageView close_iv = dialog.findViewById(R.id.close_iv);
                        TextView title_tv = dialog.findViewById(R.id.title_tv);
                        TextView desc_tv = dialog.findViewById(R.id.desc_tv);
                        title_tv.setText(title);
                        desc_tv.setText(description);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            desc_tv.setText(Html.fromHtml(description, Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            desc_tv.setText(Html.fromHtml(description));
                        }
                        close_iv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(SignUpActivity.this);
                    e.printStackTrace();
                    Log.e("login_parce", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(SignUpActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(SignUpActivity.this);
                } else {
                    MethodClass.error_alert(SignUpActivity.this);
                }
            }
        });
        MySingleton.getInstance(SignUpActivity.this).addToRequestQueue(jsonObjectRequest);
    }


}
