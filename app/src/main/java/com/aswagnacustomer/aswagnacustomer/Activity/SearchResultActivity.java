package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Adapter.SearchResultProductAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;

import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

public class SearchResultActivity extends AppCompatActivity {
    private RecyclerView category_recyclerView,child_category_recyclerView;
    private RecyclerView recy_view,recy_view2;
    private ArrayList<HashMap<String,String>> arrayList,arrayList4 ;
    private ImageView filter_img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_search_result);
        filter_img=(ImageView)findViewById(R.id.filter_img);
        category_recyclerView=(RecyclerView)findViewById(R.id.category_recyclerView);
        child_category_recyclerView=(RecyclerView)findViewById(R.id.child_category_recyclerView);
        recy_view=findViewById(R.id.recy_view);
        recy_view2=findViewById(R.id.recy_view2);
       /* setCategoriesData();
        setChildCategoriesData();*/
        list();
        filter_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(SearchResultActivity.this,FilterActivity.class);
                startActivity(intent);
            }
        });
    }

    public void back(View view) {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(SearchResultActivity.this);
        MethodClass.setMenuColor(SearchResultActivity.this,"browse");
    }

/*    public void setCategoriesData(){
        ArrayList<HashMap<String,String>>  arrayList = new ArrayList<>();
        for (int i=0;i<10;i++){
            HashMap<String,String> hashMap=new HashMap<String,String>();
            hashMap.put("sssss","ssss");
            arrayList.add(hashMap);
        }
        SearchCategoryAdapter categoryAdater = new SearchCategoryAdapter(SearchResultActivity.this, arrayList);
        category_recyclerView.setAdapter(categoryAdater);
    }

    public void setChildCategoriesData(){
        ArrayList<HashMap<String,String>>  arrayList = new ArrayList<>();
        for (int i=0;i<10;i++){
            HashMap<String,String> hashMap=new HashMap<String,String>();
            hashMap.put("sssss","ssss");
            arrayList.add(hashMap);
        }
        SearchChildCategoryAdapter categoryAdater = new SearchChildCategoryAdapter(SearchResultActivity.this, arrayList);
        child_category_recyclerView.setAdapter(categoryAdater);
    }*/
    private void list(){
        arrayList=new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            HashMap<String,String>  hashMap=new HashMap<>();
            hashMap.put("strKey","somevalue");
            arrayList.add(hashMap);
        }
        SearchResultProductAdapter resultProductAdapter = new SearchResultProductAdapter(SearchResultActivity.this, arrayList);
        recy_view.setAdapter(resultProductAdapter);
        recy_view.setFocusable(false);
        recy_view2.setAdapter(resultProductAdapter);
        recy_view2.setFocusable(false);
    }

}
