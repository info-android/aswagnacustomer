package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Adapter.SearchCategoryAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.SearchProductAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity.catArrayListID_1;
import static com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity.catArrayListID_2;
import static com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity.tagArrayListId_1;
import static com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity.tagArrayListId_2;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CAT_SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FROM_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TO_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class SearchActivity extends AppCompatActivity {
    public RecyclerView category_recyclerView, child_category_recyclerView;
    private RecyclerView recy_view;
    private ImageView search_img;
    private AutoCompleteTextView search_auto_text;
    /*public String min_price = "";
    public String max_price = "";*/
    String categoryfinal = "", sub_categoryfinal = "";
    public String cat_slug = "", sub_cat_slug = "", brand = "", variant = "", other_variant = "", price = "", order_by = "",product = "";
    public String cat_name = "", sub_cat_name = "";
    public JSONArray finalvarientJonarry;
    public static String MAX_PRIZE="";
    public static String MIN_PRIZE="";
    public  SearchCategoryAdapter categoryAdater;
    private Integer pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_search);
        category_recyclerView = (RecyclerView) findViewById(R.id.category_recyclerView);
        child_category_recyclerView = (RecyclerView) findViewById(R.id.child_category_recyclerView);
        recy_view = findViewById(R.id.recy_view);
        search_img = findViewById(R.id.search_icon_img);
        search_auto_text = findViewById(R.id.search_auto_text);

        if (getIntent().getStringExtra("cat_slug").equals("0")){
            cat_slug = "";
        }else {
            cat_slug = getIntent().getStringExtra("cat_slug");
        }

        if (getIntent().getStringExtra("sub_cat_slug").equals("0")){
            sub_cat_slug = "";
            Log.e("Sub",sub_cat_slug);
        }else {
            sub_cat_slug = getIntent().getStringExtra("sub_cat_slug");
            Log.e("Sub",sub_cat_slug);
        }

        if (getIntent().getStringExtra("brand").equals("0")){
            brand = "";
        }else {
            brand = getIntent().getStringExtra("brand");
        }
        if (getIntent().getStringExtra("variant").equals("0")){
            variant = "";
        }else {
            variant = getIntent().getStringExtra("variant");
            try {
                finalvarientJonarry=new JSONArray(variant);
                Log.e("jsonArray", String.valueOf(finalvarientJonarry));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if (getIntent().getStringExtra("other_variant").equals("0")){
            other_variant = "";
        }else {
            other_variant = getIntent().getStringExtra("other_variant");
        }
        if (getIntent().getStringExtra("price").equals("0")){
            price = "";
        }else {
            price = getIntent().getStringExtra("price");
        }
        if (getIntent().getStringExtra("order_by").equals("0")){
            order_by = "";
        }else {
            order_by = getIntent().getStringExtra("order_by");
        }
        if (getIntent().getStringExtra("product").equals("")){
            product = "";
        }else {
            product = getIntent().getStringExtra("product");
            search_auto_text.setText(product);
        }
        search_categorySet();

       /* if (getIntent().hasExtra("key")){
            key=getIntent().getStringExtra("key");
        }else{
            key="";
        }
        if (getIntent().hasExtra("order_by")){
            order_by=getIntent().getStringExtra("order_by");
        }else{
            order_by="";
        }*/
       MIN_PRIZE="";
       MAX_PRIZE="";
        searchList();
    }

    public void search_categorySet() {
        if (PreferenceManager.getDefaultSharedPreferences(SearchActivity.this).getString("LANG", "").equals("en")) {
            ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(SearchActivity.this, android.R.layout.simple_dropdown_item_1line, tagArrayListId_1);
            search_auto_text.setAdapter(adapter);
            search_auto_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                    String key = (String) stringWithTag.tag;
                    for (int i = 0; i < catArrayListID_1.size(); i++) {
                        HashMap<String, Object> hashMap = catArrayListID_1.get(i);
                        Log.e("SLUGG2", String.valueOf(hashMap.get(SLUG))+"     "+key);
                        if (String.valueOf(hashMap.get(SLUG)).equals(key)) {
                            Log.e("SLUGG", String.valueOf(hashMap.get(SLUG))+"     "+key);
                            cat_slug = key;
                            sub_cat_slug="";
                            pos = position;
                        }
                    }
                    if (cat_slug.equals("")) {
                        sub_cat_slug = key;
                        cat_slug="";
                    }
                    MIN_PRIZE="";
                    MAX_PRIZE="";
                    categoryAdater.notifyDataSetChanged();
                    searchList();
                }
            });
            categoryAdater = new SearchCategoryAdapter(SearchActivity.this, catArrayListID_1);
            category_recyclerView.setAdapter(categoryAdater);
            RecyclerView.SmoothScroller smoothScroller = new LinearSmoothScroller(SearchActivity.this) {
                @Override protected int getVerticalSnapPreference() {
                    return LinearSmoothScroller.SNAP_TO_START;
                }
            };

            smoothScroller.setTargetPosition(pos);
            ((LinearLayoutManager) category_recyclerView.getLayoutManager()).startSmoothScroll(smoothScroller);
            for (int i = 0; i <catArrayListID_1.size() ; i++) {
                //Log.e("SLUG", String.valueOf(catArrayListID_2.get(i).get(CAT_SLUG)));
                final HashMap<String, Object> map = catArrayListID_2.get(i);
                Log.e("SLUG", String.valueOf(map.get(SLUG))+" "+cat_slug);
                if(cat_slug.equals(String.valueOf(map.get(SLUG)))){
                    category_recyclerView.scrollToPosition(i);
                }
            }
        } else {
            ArrayAdapter<MethodClass.StringWithTag> adapter = new ArrayAdapter<MethodClass.StringWithTag>(SearchActivity.this, android.R.layout.simple_dropdown_item_1line, tagArrayListId_2);
            search_auto_text.setAdapter(adapter);
            search_auto_text.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                    String key = (String) stringWithTag.tag;
                    for (int i = 0; i < catArrayListID_2.size(); i++) {
                        HashMap<String, Object> hashMap = catArrayListID_2.get(i);
                        Log.e("SLUG", String.valueOf(hashMap.get(SLUG)));
                        if (String.valueOf(hashMap.get(SLUG)).equals(key)) {
                            cat_slug = key;
                            sub_cat_slug="";
                            pos = position;
                        }
                    }
                    if (cat_slug.equals("")) {
                        sub_cat_slug = key;
                        cat_slug="";
                    }
                    MIN_PRIZE="";
                    MAX_PRIZE="";
                    categoryAdater.notifyDataSetChanged();
                    searchList();
                }
            });

            categoryAdater = new SearchCategoryAdapter(SearchActivity.this, catArrayListID_2);
            category_recyclerView.setAdapter(categoryAdater);
            RecyclerView.SmoothScroller smoothScroller2 = new LinearSmoothScroller(SearchActivity.this) {
                @Override protected int getVerticalSnapPreference() {
                    return LinearSmoothScroller.SNAP_TO_ANY;
                }
            };

            smoothScroller2.setTargetPosition(pos);
            ((LinearLayoutManager) category_recyclerView.getLayoutManager()).startSmoothScroll(smoothScroller2);
            for (int i = 0; i <catArrayListID_2.size() ; i++) {
                //Log.e("SLUG", String.valueOf(catArrayListID_2.get(i).get(CAT_SLUG)));
                final HashMap<String, Object> map = catArrayListID_2.get(i);
                Log.e("SLUG", String.valueOf(map.get(SLUG))+" "+cat_slug);
                if(cat_slug.equals(String.valueOf(map.get(SLUG)))){
                    category_recyclerView.scrollToPosition(i);
                }
            }
            //category_recyclerView.scrollToPosition(pos);
        }
        search_auto_text.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    product = search_auto_text.getText().toString();
                    searchList();
                    handled = true;
                }
                return handled;
            }
        });
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void filter(View view) {
        Intent intent = new Intent(this, FilterActivity.class);
        intent.putExtra("cat_slug", cat_slug);
        intent.putExtra("cat_name", cat_name);
        intent.putExtra("sub_cat_slug", sub_cat_slug);
        intent.putExtra("sub_cat_name", sub_cat_name);
        intent.putExtra("min_price", MIN_PRIZE);
        intent.putExtra("max_price", MAX_PRIZE);
        intent.putExtra("from", "search");
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(SearchActivity.this);
        MethodClass.setMenuColor(SearchActivity.this, "browse");
    }

    public void searchList() {

       /* for (int i = 0; i < catArrayListID_1.size(); i++) {
            HashMap<String,Object> hashMap=catArrayListID_1.get(i);
            if (String.valueOf(hashMap.get(SLUG)).equals(key)){
                categoryfinal = key;
            }
        }
        if (categoryfinal.equals("")){
            sub_categoryfinal=key;
        }*/

        if (!isNetworkConnected(SearchActivity.this)) {
            MethodClass.network_error_alert(SearchActivity.this);
            return;
        }
        MethodClass.showProgressDialog(SearchActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "search";
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("category", cat_slug);
        params.put("sub_category", sub_cat_slug);
        params.put("brands", brand);
        params.put("variant", finalvarientJonarry);
        params.put("other_variant", other_variant);
        params.put("price", price);
        params.put("order_by", order_by);
        params.put("product",product);
        JSONObject jsonObject = MethodClass.Json_rpc_format_obj(params);
        Log.e("searchParames", jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(SearchActivity.this);
                Log.e("respSearch", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(SearchActivity.this, response);
                    if (resultResponse != null) {

                        MIN_PRIZE = resultResponse.getString("min_price");
                        MAX_PRIZE = resultResponse.getString("max_price");
                        JSONArray products = resultResponse.getJSONArray("products");
                        ArrayList<HashMap<String, String>> productArrayList = new ArrayList<>();
                        for (int i = 0; i < products.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID, products.getJSONObject(i).getString("id"));
                            hashMap.put(PRICE, products.getJSONObject(i).getString("price"));
                            hashMap.put(FROM_DATE, products.getJSONObject(i).getString("from_date"));
                            hashMap.put(TO_DATE, products.getJSONObject(i).getString("to_date"));
                            hashMap.put(DISCOUNT_PRICE, products.getJSONObject(i).getString("discount_price"));
                            if (!products.getJSONObject(i).getString("default_image").equals("null")) {
                                hashMap.put(IMAGE, products.getJSONObject(i).getJSONObject("default_image").getString("image"));
                            }
                            hashMap.put(TITLE, products.getJSONObject(i).getJSONObject("product_by_language").getString("title"));
                            productArrayList.add(hashMap);
                        }
                        SearchProductAdapter searchProductAdapter = new SearchProductAdapter(SearchActivity.this, productArrayList);
                        recy_view.setAdapter(searchProductAdapter);
                        recy_view.setFocusable(false);

                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(SearchActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(SearchActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(SearchActivity.this);
                } else {
                    MethodClass.error_alert(SearchActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization",MethodClass.check_locale_lang(SearchActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(SearchActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(SearchActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
