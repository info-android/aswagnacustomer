package com.aswagnacustomer.aswagnacustomer.Activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.MyRewardPointAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.OrderHistoryAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_AMOUNT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_RECEIVED;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.LOYALTY_USED;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class MyRewardPointActivity extends AppCompatActivity {

    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> arrayList;
    private TextView point_earned_tv, point_used_tv, point_available_tv;
    private LinearLayout main_lay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.layout_my_reward);
        recy_view = findViewById(R.id.recy_view);
        point_earned_tv = findViewById(R.id.point_earned_tv);
        point_used_tv = findViewById(R.id.point_used_tv);
        point_available_tv = findViewById(R.id.point_available_tv);
        main_lay = findViewById(R.id.main_lay);
        main_lay.setVisibility(View.GONE);
        customer_reward_points();
    }


    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(MyRewardPointActivity.this);
        MethodClass.setMenuColor(MyRewardPointActivity.this, "no");
    }

    public void back(View view) {
        super.onBackPressed();
    }

    private void customer_reward_points() {
        if (!isNetworkConnected(MyRewardPointActivity.this)) {
            MethodClass.network_error_alert(MyRewardPointActivity.this);
            return;
        }
        MethodClass.showProgressDialog(MyRewardPointActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "customer-reward-points";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(MyRewardPointActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(MyRewardPointActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject user_info=resultResponse.getJSONObject("user_info");
                        point_available_tv.setText(user_info.getString("loyalty_balance"));
                        point_earned_tv.setText(user_info.getString("loyalty_total"));
                        point_used_tv.setText(user_info.getString("loyalty_used"));
                        JSONArray rewards = resultResponse.getJSONArray("rewards");
                        arrayList = new ArrayList<>();
                        for (int i = 0; i < rewards.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            // hashMap.put(ID,rewards.getJSONObject(i).getString("id"));
                            hashMap.put(ORDER_NO, rewards.getJSONObject(i).getString("order_no"));
                            hashMap.put(CREATED_AT, rewards.getJSONObject(i).getString("created_at"));
                            hashMap.put(LOYALTY_RECEIVED, rewards.getJSONObject(i).getString("loyalty_point_received"));
                            hashMap.put(LOYALTY_USED, rewards.getJSONObject(i).getString("loyalty_point_used"));
                            hashMap.put(LOYALTY_AMOUNT, user_info.getString("loyalty_balance"));
                            arrayList.add(hashMap);
                        }
                        MyRewardPointAdapter myRewardPointAdapter = new MyRewardPointAdapter(MyRewardPointActivity.this, arrayList);
                        recy_view.setAdapter(myRewardPointAdapter);
                        recy_view.setFocusable(false);
                        main_lay.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(MyRewardPointActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(MyRewardPointActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(MyRewardPointActivity.this);
                } else {
                    MethodClass.error_alert(MyRewardPointActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(MyRewardPointActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(MyRewardPointActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(MyRewardPointActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
