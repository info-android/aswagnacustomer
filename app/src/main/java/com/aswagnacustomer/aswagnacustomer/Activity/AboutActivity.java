package com.aswagnacustomer.aswagnacustomer.Activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ABOUT_IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class AboutActivity extends AppCompatActivity {

    private ImageView image;
    private TextView head_tv, sub_head_tv, desc_tv;
    private TextView w_w_d_head_1, w_w_d_desc_1;
    private TextView w_w_d_head_2, w_w_d_desc_2;
    private TextView w_w_d_head_3, w_w_d_desc_3;
    private TextView w_w_d_head_4, w_w_d_desc_4;
    private LinearLayout main_lay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_about);
        image = findViewById(R.id.image);
        head_tv = findViewById(R.id.head_tv);
        sub_head_tv = findViewById(R.id.sub_head_tv);
        desc_tv = findViewById(R.id.desc_tv);
        w_w_d_head_1 = findViewById(R.id.w_w_d_head_1);
        w_w_d_head_2 = findViewById(R.id.w_w_d_head_2);
        w_w_d_head_3 = findViewById(R.id.w_w_d_head_3);
        w_w_d_head_4 = findViewById(R.id.w_w_d_head_4);
        w_w_d_desc_1 = findViewById(R.id.w_w_d_desc_1);
        w_w_d_desc_2 = findViewById(R.id.w_w_d_desc_2);
        w_w_d_desc_3 = findViewById(R.id.w_w_d_desc_3);
        w_w_d_desc_4 = findViewById(R.id.w_w_d_desc_4);
        main_lay = findViewById(R.id.main_lay);
        main_lay.setVisibility(View.GONE);
        getAboutUS();
    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(AboutActivity.this);
        MethodClass.setMenuColor(AboutActivity.this, "no");
    }


    public void getAboutUS() {
        if (!isNetworkConnected(AboutActivity.this)) {
            MethodClass.network_error_alert(AboutActivity.this);
            return;
        }
        MethodClass.showProgressDialog(AboutActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "view-about-us";
        Log.e("server_url", server_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AboutActivity.this);
                Log.e("Responce::", response.toString());
                try {
                    final JSONObject resultResponse = MethodClass.get_result_from_webservice(AboutActivity.this, response);
                    if (resultResponse != null) {
                        JSONObject about_content = resultResponse.getJSONObject("about_content");
                        String first_sec_right_first_image = about_content.getString("first_sec_right_first_image");
                        Picasso.get().load(ABOUT_IMAGE + first_sec_right_first_image).placeholder(R.drawable.applogo).error(R.drawable.applogo).into(image);
                        head_tv.setText(about_content.getString("second_sec_first_heading"));
                        sub_head_tv.setText(about_content.getString("second_sec_first_content"));
                        desc_tv.setText(about_content.getString("first_sec_desc_who_we_are"));

                        w_w_d_head_1.setText(about_content.getString("fourth_section_first_title"));
                        w_w_d_desc_1.setText(about_content.getString("fourth_section_first_content"));

                        w_w_d_head_2.setText(about_content.getString("fourth_section_second_title"));
                        w_w_d_desc_2.setText(about_content.getString("fourth_section_second_content"));

                        w_w_d_head_3.setText(about_content.getString("fourth_section_third_title"));
                        w_w_d_desc_3.setText(about_content.getString("fourth_section_third_content"));

                        w_w_d_head_4.setText(about_content.getString("fourth_section_four_title"));
                        w_w_d_desc_4.setText(about_content.getString("fourth_section_four_content"));
                        main_lay.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(AboutActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(AboutActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AboutActivity.this);
                } else {
                    MethodClass.error_alert(AboutActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AboutActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AboutActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AboutActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
