package com.aswagnacustomer.aswagnacustomer.Activity;

import android.Manifest;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.android.volley.request.SimpleMultiPartRequest;
import com.aswagnacustomer.aswagnacustomer.Helper.FetchPath;
import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.Helper.RequestManager;
import com.aswagnacustomer.aswagnacustomer.R;
import com.google.android.material.snackbar.Snackbar;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PICK_FROM_GALLERY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PROFILE_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TAKE_PHOTO_CODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class ProfileActivity extends AppCompatActivity {

    private ImageView pro_pic_edit_img, pro_pic;
    ///////image////////////
    private Uri outputFileUri;
    private String imageFilePath;
    private String img_path = "";
    ///////image close////////////

    private TextView user_name_tv, email_tv;
    private Spinner spin_country;
    private EditText f_name_et, l_name_et, email_et, mobile_et, address_et, city_et, state_et, pin_et, old_pass_et, new_pass_et, confirm_pass_et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_profile);
        pro_pic_edit_img = findViewById(R.id.pro_pic_edit_img);
        pro_pic = findViewById(R.id.pro_pic);
        user_name_tv = findViewById(R.id.user_name_tv);
        email_tv = findViewById(R.id.email_tv);
        f_name_et = findViewById(R.id.f_name_et);
        l_name_et = findViewById(R.id.l_name_et);
        email_et = findViewById(R.id.email_et);
        mobile_et = findViewById(R.id.mobile_et);
        address_et = findViewById(R.id.address_et);
        spin_country = findViewById(R.id.spin_country);
        city_et = findViewById(R.id.city_et);
        state_et = findViewById(R.id.state_et);
        pin_et = findViewById(R.id.pin_et);
        old_pass_et = findViewById(R.id.old_pass_et);
        new_pass_et = findViewById(R.id.new_pass_et);
        confirm_pass_et = findViewById(R.id.confirm_pass_et);
        pro_pic_edit_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickImage();
            }
        });
        getUserDetails();
    }

    private void setSpin_country(JSONArray jsonArray) {
        try {
            ArrayList<MethodClass.StringWithTag> countryArrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                countryArrayList.add(new MethodClass.StringWithTag(jsonArray.getJSONObject(i).getString("country_name"), jsonArray.getJSONObject(i).getString("id")));
            }
            ArrayAdapter spinAdapter = new ArrayAdapter(ProfileActivity.this, R.layout.spinner_tv_layout, countryArrayList);
            spin_country.setAdapter(spinAdapter);
            spin_country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    MethodClass.StringWithTag stringWithTag = (MethodClass.StringWithTag) parent.getItemAtPosition(position);
                    spin_country.setTag(stringWithTag.tag);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } catch (JSONException e) {

        }

    }
    private void getUserDetails() {
        if (!isNetworkConnected(ProfileActivity.this)) {
            MethodClass.network_error_alert(ProfileActivity.this);
            return;
        }
        MethodClass.showProgressDialog(ProfileActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "user-details";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ProfileActivity.this);
                Log.e("respUser", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ProfileActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray countryJSONArray = resultResponse.getJSONArray("country");
                        setSpin_country(countryJSONArray);
                        JSONObject user = resultResponse.getJSONObject("user");
                        String id = user.getString("id");
                        String first_name = user.getString("fname");
                        String last_name = user.getString("lname");
                        String name = first_name + " " + last_name;
                        String mobile = user.getString("phone");
                        String email = user.getString("email");
                        String profile_img = user.getString("image");
                        String address = user.getString("address");
                        String city = user.getString("city");
                        String state = user.getString("state");
                        String zipcode = user.getString("zipcode");
                        String country = user.getString("country");

                        String profile_im = PROFILE_IMAGE_URL + profile_img;
                        Log.e("profile_im", profile_im);

                        Picasso.get().load(profile_im).placeholder(R.drawable.ic_user_circle_black_24dp).error(R.drawable.ic_user_circle_black_24dp).into(pro_pic);

                        user_name_tv.setText(name);
                        email_tv.setText(email);
                        f_name_et.setText(first_name);
                        if (!last_name.equals("") && !last_name.equals("null") && !last_name.equals(null)){
                            l_name_et.setText(last_name);
                        }
                        email_et.setText(email);
                        if (!mobile.equals("") && !mobile.equals("null") && !mobile.equals(null)){
                            mobile_et.setText(mobile);
                        }

                        if (!address.equals("") && !address.equals("null") && !address.equals(null)){
                            address_et.setText(address);
                        }
                        if (!city.equals("") && !city.equals("null") && !city.equals(null)){
                            city_et.setText(city);
                        }
                        if (!state.equals("") && !state.equals("null") && !state.equals(null)){
                            state_et.setText(state);
                        }
                        if (!zipcode.equals("") && !zipcode.equals("null") && !zipcode.equals(null)){
                            pin_et.setText(zipcode);
                        }

                        if (!country.equals("null") && !country.equals(null) && !country.equals("") ){
                            for (int i = 0; i < countryJSONArray.length(); i++) {
                                if (countryJSONArray.getJSONObject(i).getString("id").equals(country)) {
                                    spin_country.setSelection(i);
                                    spin_country.setTag(country);
                                }
                            }
                        }

                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_fname", first_name).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_lname", last_name).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_name", name).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("user_id", id).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putBoolean("is_logged_in", true).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("profile_pic", profile_img).commit();
                        PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).edit().putString("mobile", mobile).commit();

                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(ProfileActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ProfileActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProfileActivity.this);
                } else {
                    MethodClass.error_alert(ProfileActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization",MethodClass.check_locale_lang(ProfileActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ProfileActivity.this).addToRequestQueue(jsonObjectRequest);
    }
    public void saveAllChanges(View view) {
        String fname, lname, phone, email, address, country, state, city, zipcode, password, new_password, confirm_password, image;

        fname = f_name_et.getText().toString().trim();
        lname = l_name_et.getText().toString().trim();
        phone = mobile_et.getText().toString().trim();
        email = email_et.getText().toString().trim();
//        address = address_et.getText().toString().trim();
//        country = spin_country.getTag().toString();
//        state = state_et.getText().toString().trim();
//        city = city_et.getText().toString().trim();
//        zipcode = pin_et.getText().toString().trim();
        password = old_pass_et.getText().toString().trim();
        new_password = new_pass_et.getText().toString().trim();
        confirm_password = confirm_pass_et.getText().toString().trim();


        if (fname.length() == 0) {
            f_name_et.setError(getResources().getString(R.string.please_enter_first_name));
            f_name_et.requestFocus();
            return;
        }
        if (lname.length() == 0) {
            l_name_et.setError(getResources().getString(R.string.please_enter_last_name));
            l_name_et.requestFocus();
            return;
        }
        if (email.length() == 0) {
            email_et.setError(getResources().getString(R.string.please_enter_your_email_address));
            email_et.requestFocus();
            return;
        }
        if (!MethodClass.emailValidator(email)) {
            email_et.setError(getResources().getString(R.string.invalid_email_address));
            email_et.requestFocus();
            return;
        }
        if (phone.length() == 0) {
            mobile_et.setError(getResources().getString(R.string.please_enter_mobile_no));
            mobile_et.requestFocus();
            return;
        }
//        if (address.length() == 0) {
//            address_et.setError(getResources().getString(R.string.please_enter_address));
//            address_et.requestFocus();
//            return;
//        }
//
//        if (city.length() == 0) {
//            city_et.setError(getResources().getString(R.string.please_enter_city));
//            city_et.requestFocus();
//            return;
//        }
//        if (state.length() == 0) {
//            state_et.setError(getResources().getString(R.string.please_enter_state));
//            state_et.requestFocus();
//            return;
//        }
//        if (zipcode.length() == 0) {
//            pin_et.setError(getResources().getString(R.string.pleas_enter_zipcode));
//            pin_et.requestFocus();
//            return;
//        }

        if (password.length() != 0 || new_password.length() != 0 || confirm_password.length() != 0) {
            if (password.length() == 0) {
                old_pass_et.setError(getResources().getString(R.string.pleas_enter_old_pass));
                old_pass_et.requestFocus();
                return;
            }
            if (new_password.length() == 0) {
                new_pass_et.setError(getResources().getString(R.string.pleas_enter_new_pass));
                new_pass_et.requestFocus();
                return;
            }
            if (confirm_password.length() == 0) {
                confirm_pass_et.setError(getResources().getString(R.string.pleas_enter_conf_pass));
                confirm_pass_et.requestFocus();
                return;
            }
            if (!confirm_password.equals(new_password)) {
                confirm_pass_et.setError(getResources().getString(R.string.password_is_not_matched_with_confirm_password));
                confirm_pass_et.requestFocus();
                return;
            }
        }

        MethodClass.showProgressDialog(ProfileActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "user-update";
        Log.e("server_url", server_url);
        SimpleMultiPartRequest simpleMultiPartRequest = new SimpleMultiPartRequest(Request.Method.POST, server_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                MethodClass.hideProgressDialog(ProfileActivity.this);
                Log.e("onResponse: ", response.toString());
                try {
                    JSONObject jsonObject = MethodClass.get_result_from_webservice(ProfileActivity.this, new JSONObject(response));
                    if (jsonObject != null) {
                        if (jsonObject.has("email_updated")){
                            if (jsonObject.getString("email_updated").equals("Y")){
                                Intent intent=new Intent(ProfileActivity.this,EmailChangeVerifyActivity.class);
                                intent.putExtra("code",new JSONObject(response).getString("otp"));
                                intent.putExtra("email",email_et.getText().toString().trim());
                                startActivity(intent);
                                finish();
                            }else {
                                new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.SUCCESS_TYPE).setTitleText(jsonObject.getString("message")).setContentText(jsonObject.getString("meaning")).setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                        getUserDetails();
                                    }
                                }).show();
                            }
                        }else {
                            new SweetAlertDialog(ProfileActivity.this, SweetAlertDialog.SUCCESS_TYPE).setTitleText(jsonObject.getString("message")).setContentText(jsonObject.getString("meaning")).setConfirmText(getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                    getUserDetails();
                                }
                            }).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    MethodClass.error_alert(ProfileActivity.this);
                    Log.e("Exception", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MethodClass.hideProgressDialog(ProfileActivity.this);
                Log.e("error", error.toString());
                MethodClass.hideProgressDialog(ProfileActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ProfileActivity.this);
                } else {
                    MethodClass.error_alert(ProfileActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ProfileActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        RequestQueue requestQueue = RequestManager.getnstance(ProfileActivity.this);
        simpleMultiPartRequest.addMultipartParam("fname", "text", fname);
        Log.d("fname", fname);
        simpleMultiPartRequest.addMultipartParam("lname", "text", lname);
        Log.d("lname", lname);
        simpleMultiPartRequest.addMultipartParam("phone", "text", phone);
        Log.d("phone", phone);
        simpleMultiPartRequest.addMultipartParam("email", "text", email);
        Log.d("email", email);
//        simpleMultiPartRequest.addMultipartParam("address", "text", address);
//        Log.d("address", address);
//        simpleMultiPartRequest.addMultipartParam("country", "text", country);
//        Log.d("country", country);
//        simpleMultiPartRequest.addMultipartParam("state", "text", state);
//        Log.d("state", state);
//        simpleMultiPartRequest.addMultipartParam("city", "text", city);
//        Log.d("city", city);
//        simpleMultiPartRequest.addMultipartParam("zipcode", "text", zipcode);
//        Log.d("zipcode", zipcode);
        if (password.length() !=0){
            simpleMultiPartRequest.addMultipartParam("password", "text", password);
            Log.d("password", password);
            simpleMultiPartRequest.addMultipartParam("new_password", "text", new_password);
            Log.d("new_password", new_password);
            simpleMultiPartRequest.addMultipartParam("confirm_password", "text", confirm_password);
            Log.d("confirm_password", confirm_password);
        }

        if (!img_path.equals("null") && !img_path.equals(null) && !img_path.equals("")) {
            simpleMultiPartRequest.addFile("image", img_path);
            Log.e("image", img_path);
        }
        simpleMultiPartRequest.setFixedStreamingMode(true);
        int socketTimeout = 100000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, 3, 5);
        simpleMultiPartRequest.setRetryPolicy(policy);
        requestQueue.add(simpleMultiPartRequest);


    }



    ////////////////////////Q_IMAGE FUNS////////////////////////////////
    private void clickImage() {
        //In this method sho popup to select which functionality to choose your profile pic
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.chose_from_library), getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    //in this section when choose Take photo opec camera
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        //here check CAMERA permission
                        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            //if not have CAMERA permission to requestPermissions
                            requestPermissions(new String[]{Manifest.permission.CAMERA}, TAKE_PHOTO_CODE);
                        } else {
                            //if have CAMERA permission call  camera open function
                            cameraIntent();
                        }
                    }

                } else if (items[item].equals(getString(R.string.chose_from_library))) {
                    //in this section when choose from gallery opec gallery
                    try {
                        //here check Storage permission
                        if (ActivityCompat.checkSelfPermission(ProfileActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ProfileActivity.this,Manifest.permission.WRITE_EXTERNAL_STORAGE)!=PackageManager.PERMISSION_GRANTED) {
                            //if not have Storage permission to requestPermissions
                            ActivityCompat.requestPermissions(ProfileActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
                        } else {
                            //if have Storage permission call  gallery open function
                            get_intent();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (items[item].equals(getString(R.string.cancel))) {
                    //in this section when choose cancel to close popup
                    dialog.dismiss();
                }
            }
        });
        builder.show();//here show popup


    }

    private void get_intent() {
        //here open Gallery functionality and select picture
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_FROM_GALLERY);
    }

    private void cameraIntent() {
        //here open camera functionality and capture picture and get picture path
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        File photoFile = null;
        try {
            photoFile = createImageFile();
        } catch (Exception ex) {
            //here Error occurred while creating the File with  show Snackbar
            ex.printStackTrace();
            Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.image_size_too_large_choose_other_image), Snackbar.LENGTH_SHORT);
            snackbar.show();
        }
        outputFileUri = FileProvider.getUriForFile(ProfileActivity.this, "com.aswagnacustomer.aswagnacustomer.fileprovider", photoFile);
        Log.e("outputFileUri", outputFileUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
        Log.e("Intentdata", intent.toString());
        startActivityForResult(intent, TAKE_PHOTO_CODE);
    }

    private File createImageFile() throws IOException {
        //here create temp file to hols Camera capture image
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */);

        imageFilePath = image.getAbsolutePath();
        return image;
    }

    //getPath function return selected image path
    public String getPath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{split[1]};
            }
        }
        Log.e("LOGURI", uri.getScheme());
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = null;
            try {
                cursor = ProfileActivity.this.getContentResolver().query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        } else {
            return uri.getPath();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    //here get data(image path) of selected image.
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Log.e("requestCode", String.valueOf(requestCode));
            if (requestCode == PICK_FROM_GALLERY && data != null) {
                //in this section get data of when sected from gallery
                if (!data.getData().getAuthority().equals("com.android.providers.downloads.documents")) {
                    Uri img_uri = data.getData();
                    Bitmap bitmap = null;
                    try {
                        String imagePath = FetchPath.getPath(ProfileActivity.this, img_uri);
                        Log.e("IMAGE", imagePath );
                        img_path = MethodClass.getRightAngleImage(imagePath);//here final image path
                        File imagefile = new File(imagePath);
                        if (imagefile.exists()) {
                            bitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());
                            pro_pic.setImageBitmap(bitmap);//set image of design(Image view)
                        }
                    } catch (OutOfMemoryError error) {
                        //here selected big size of image when handling OutOfMemoryError with show popup
                        img_path = "";
                        error.printStackTrace();
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.your_image_size_too_large), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                    } catch (Exception e) {
                        //here selected image when any type of issue handling Exception with show popup
                        e.printStackTrace();
                        img_path = "";
                        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_choose_image_from_other_folder), Snackbar.LENGTH_SHORT);
                        snackbar.show();
                        Log.d("ex", e.getMessage());
                    }
                } else {
                    img_path = "";
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.please_choose_image_from_other_folder), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                    Log.e("image_uri", "null");
                }
            } else if (requestCode == TAKE_PHOTO_CODE) {
                //in this section get data of when selected Tack Photo(Camera)
                File imagefile = null;
                Bitmap bitmap = null;
                try {
                    img_path = MethodClass.getRightAngleImage(imageFilePath);//here final image path
                    Log.e("imageFilePath", imageFilePath);
                    imagefile = new File(img_path);
                    if (imagefile.exists()) {
                        bitmap = BitmapFactory.decodeFile(imagefile.getAbsolutePath());
                        pro_pic.setImageBitmap(bitmap);//set image of design(Image view)
                    }
                } catch (OutOfMemoryError error) {
                    //here selected big size of image when handling OutOfMemoryError with show popup
                    img_path = "";
                    error.printStackTrace();
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.your_image_size_too_large), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                } catch (Exception e) {
                    //here selected image when any type of issue handling Exception with show popup
                    img_path = "";
                    e.printStackTrace();
                    Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), getResources().getString(R.string.something_went_wrong), Snackbar.LENGTH_SHORT);
                    snackbar.show();
                }
            }
        } else {

            //business_logo_iv.setVisibility(View.GONE);

        }
    }

    //here when user first time give permission to selected image then call onRequestPermissionsResult
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == TAKE_PHOTO_CODE) {
            //in this section get data of when sected tack photo
            //check permission if give permission to open camera otherwise show message "Camera Permission Denied"
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                cameraIntent();
            } else {
                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.camera_permission_denied), Toast.LENGTH_SHORT).show();
            }
        } else if (requestCode == PICK_FROM_GALLERY) {
            //in this section get data of when sected from gallery
            //check permission if give permission to open gallery otherwise show message "Storage Permission Denied"
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                get_intent();
            } else {
                Toast.makeText(ProfileActivity.this, getResources().getString(R.string.storage_permission_denied), Toast.LENGTH_SHORT).show();
            }
        }
    }


    //////////////////////////////CLOSE////////////////////////////////

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(ProfileActivity.this);
        MethodClass.setMenuColor(ProfileActivity.this, "no");


    }



}
