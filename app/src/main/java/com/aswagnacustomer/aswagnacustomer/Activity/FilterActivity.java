package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Adapter.FiltervariantAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Activity.SearchActivity.MAX_PRIZE;
import static com.aswagnacustomer.aswagnacustomer.Activity.SearchActivity.MIN_PRIZE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SELECTED_DATA;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class FilterActivity extends AppCompatActivity {

    private LinearLayout high_to_low, low_to_high, newwest_first, alphabetical;
    private TextView category_textView,sub_cat_tv,price_tv,brand_tv;
    private LinearLayout category_lin,brand_lin,cat_lin,price_lin;
    private RecyclerView varyantRecy;
    private Button apply_btn;
    String  cat_slug="",sub_cat_slug=""/*,min_price="",max_price=""*/;
    String  cat_name="",sub_cat_name="";
    public String varientId="";
    public static JSONArray variJsonArray;
    public static ArrayList<String> brandArrayID;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(this);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_filter);
        variJsonArray=new JSONArray();
        brandArrayID = new ArrayList<>();
        high_to_low = findViewById(R.id.high_to_low);
        apply_btn = findViewById(R.id.apply_btn);
        low_to_high = findViewById(R.id.low_to_high);
        newwest_first = findViewById(R.id.newwest_first);
        alphabetical = findViewById(R.id.alphabetical);
        category_textView = findViewById(R.id.category_textView);
        category_lin = findViewById(R.id.category_lin);
        varyantRecy = findViewById(R.id.varyantRecy);
        brand_lin = findViewById(R.id.brand_lin);
        cat_lin = findViewById(R.id.cat_lin);
        price_lin = findViewById(R.id.price_lin);
        sub_cat_tv = findViewById(R.id.sub_cat_tv);
        price_tv = findViewById(R.id.price_tv);
        brand_tv = findViewById(R.id.brand_tv);


        cat_slug=getIntent().getStringExtra("cat_slug");
        cat_name=getIntent().getStringExtra("cat_name");
        sub_cat_slug=getIntent().getStringExtra("sub_cat_slug");
        sub_cat_name=getIntent().getStringExtra("sub_cat_name");
        /*min_price=getIntent().getStringExtra("min_price");
        max_price=getIntent().getStringExtra("max_price");*/
       /* if (getIntent().getStringExtra("from").equals("search")){
            cat_slug=getIntent().getStringExtra("cat_slug");
            cat_name=getIntent().getStringExtra("cat_name");
            sub_cat_slug=getIntent().getStringExtra("sub_cat_slug");
            sub_cat_name=getIntent().getStringExtra("sub_cat_name");
            min_price=getIntent().getStringExtra("min_price");
            max_price=getIntent().getStringExtra("max_price");
        }else if (getIntent().getStringExtra("from").equals("sub_filter")){
           String value_id= getIntent().getStringExtra("vari_val_id");
            HashMap<String,String> hashMap=new HashMap<>();
            hashMap.put("variant_id",varientId);
            hashMap.put("value_id",value_id);
            variJsonArray.put(hashMap);
        }
*/



        high_to_low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterActivity.this, SearchActivity.class);
                intent.putExtra("cat_slug",cat_slug);
                intent.putExtra("sub_cat_slug",sub_cat_slug);
                intent.putExtra("brand","0");
                intent.putExtra("variant","0");
                intent.putExtra("other_variant","0");
                intent.putExtra("price",MIN_PRIZE+","+MAX_PRIZE);
                intent.putExtra("order_by","H");
                intent.putExtra("product","");
                startActivity(intent);
            }
        });
        low_to_high.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterActivity.this, SearchActivity.class);
                intent.putExtra("cat_slug",cat_slug);
                intent.putExtra("sub_cat_slug",sub_cat_slug);
                intent.putExtra("brand","0");
                intent.putExtra("variant","0");
                intent.putExtra("other_variant","0");
                intent.putExtra("price",MIN_PRIZE+","+MAX_PRIZE);
                intent.putExtra("order_by","L");
                intent.putExtra("product","");
                startActivity(intent);
            }
        });
        newwest_first.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterActivity.this, SearchActivity.class);
                intent.putExtra("cat_slug",cat_slug);
                intent.putExtra("sub_cat_slug",sub_cat_slug);
                intent.putExtra("brand","0");
                intent.putExtra("variant","0");
                intent.putExtra("other_variant","0");
                intent.putExtra("price",MIN_PRIZE+","+MAX_PRIZE);
                intent.putExtra("order_by","N");
                intent.putExtra("product","");
                startActivity(intent);
            }
        });
        alphabetical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterActivity.this, SearchActivity.class);
                intent.putExtra("cat_slug",cat_slug);
                intent.putExtra("sub_cat_slug",sub_cat_slug);
                intent.putExtra("brand","0");
                intent.putExtra("variant","0");
                intent.putExtra("other_variant","0");
                intent.putExtra("price",MIN_PRIZE+","+MAX_PRIZE);
                intent.putExtra("order_by","A");
                intent.putExtra("product","");
                startActivity(intent);
            }
        });


        high_to_low.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterActivity.this, SearchActivity.class);
                intent.putExtra("cat_slug",cat_slug);
                intent.putExtra("sub_cat_slug",sub_cat_slug);
                intent.putExtra("brand","0");
                intent.putExtra("variant", "0");
                intent.putExtra("other_variant","0");
                intent.putExtra("price",MIN_PRIZE+","+MAX_PRIZE);
                intent.putExtra("order_by","H");
                intent.putExtra("product","");
                startActivity(intent);
            }
        });

        apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterActivity.this, SearchActivity.class);
                intent.putExtra("cat_slug",cat_slug);
                intent.putExtra("sub_cat_slug",sub_cat_slug);
                intent.putExtra("brand",TextUtils.join(",",FilterActivity.brandArrayID));
                if (variJsonArray.length()>0){
                    intent.putExtra("variant", String.valueOf(variJsonArray));
                }else {
                    intent.putExtra("variant", "0");
                }
                intent.putExtra("other_variant","0");
                intent.putExtra("price",MIN_PRIZE+","+MAX_PRIZE);
                intent.putExtra("order_by","0");
                intent.putExtra("product","");
                startActivity(intent);
                finish();
            }
        });

    }

    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        filerMethod();
       /* max_price=MAX_PRIZE;
        min_price=MIN_PRIZE;*/
        category_textView.setText(cat_name);
        sub_cat_tv.setText(sub_cat_name);
        price_tv.setText(MIN_PRIZE+" to "+MAX_PRIZE);
        brand_tv.setText(FilterActivity.brandArrayID.size()+" item selected");
    }

    private void filerMethod() {
        if (!isNetworkConnected(FilterActivity.this)) {
            MethodClass.network_error_alert(FilterActivity.this);
            return;
        }
        MethodClass.showProgressDialog(FilterActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "filter";
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("cat_slug",cat_slug);
        params.put("sub_cat_slug",sub_cat_slug);
        params.put("min_price",MIN_PRIZE);
        params.put("max_price",MAX_PRIZE);
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        Log.e("filterParams",jsonObject.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(FilterActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(FilterActivity.this, response);
                    if (resultResponse != null) {
                        final String min_price=resultResponse.getString("min_price");
                        MIN_PRIZE=min_price;
                        final String max_price=resultResponse.getString("max_price");
                        MAX_PRIZE=max_price;
                        if (Float.valueOf(MIN_PRIZE)==0){
                            price_lin.setVisibility(View.GONE);
                        }else {
                            price_lin.setVisibility(View.VISIBLE);
                        }
                        price_lin.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(FilterActivity.this, FilterSubActivity.class);
                                intent.putExtra("array", "");
                                intent.putExtra("type", "price");
                                intent.putExtra("min_price",min_price);
                                intent.putExtra("max_price",max_price);
                                startActivity(intent);
                            }
                        });

                        if (resultResponse.has("category")){
                            JSONObject category= resultResponse.getJSONObject("category");
                            category_textView.setText(category.getJSONObject("category_by_language").getString("title"));
                            final String  cat = category.getString("slug");
                            final String  subCatgory = category.getString("categories");
                            if (!subCatgory.equals("null") && !subCatgory.equals(null) && !subCatgory.equals("")){
                                category_lin.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent=new Intent(FilterActivity.this,FilterSubActivity.class);
                                        intent.putExtra("array",subCatgory);
                                        intent.putExtra("cat_slug",cat);
                                        intent.putExtra("type","cat");
                                        intent.putExtra("min_price",min_price);
                                        intent.putExtra("max_price",max_price);
                                        startActivity(intent);
                                    }
                                });
                            }else {
                                cat_lin.setVisibility(View.GONE);
                            }
                        }else {
                            cat_lin.setVisibility(View.GONE);
                        }

                        if (resultResponse.has("brands")) {
                            final String brands = resultResponse.getString("brands");
                            brand_lin.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(FilterActivity.this, FilterSubActivity.class);
                                    intent.putExtra("array", brands);
                                    intent.putExtra("type", "brand");
                                    intent.putExtra("min_price",min_price);
                                    intent.putExtra("max_price",max_price);
                                    startActivity(intent);
                                }
                            });
                        }else {
                            brand_lin.setVisibility(View.GONE);
                        }

                        if (resultResponse.has("variants")) {
                            JSONArray variants = resultResponse.getJSONArray("variants");
                            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
                            for (int i = 0; i < variants.length(); i++) {
                                String id = variants.getJSONObject(i).getString("id");
                                String title = variants.getJSONObject(i).getJSONObject("variant_by_language").getString("name");
                                String vary_value = variants.getJSONObject(i).getString("variant_values");
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put(ID, id);
                                hashMap.put(SELECTED_DATA, String.valueOf(variJsonArray));
                                hashMap.put(TITLE, title);
                                hashMap.put(VARY_VALUE, vary_value);
                                arrayList.add(hashMap);
                            }
                            FiltervariantAdapter adapter = new FiltervariantAdapter(FilterActivity.this, arrayList);
                            varyantRecy.setAdapter(adapter);
                            varyantRecy.setFocusable(false);
                            varyantRecy.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(FilterActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(FilterActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(FilterActivity.this);
                } else {
                    MethodClass.error_alert(FilterActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization",MethodClass.check_locale_lang(FilterActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(FilterActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(FilterActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
