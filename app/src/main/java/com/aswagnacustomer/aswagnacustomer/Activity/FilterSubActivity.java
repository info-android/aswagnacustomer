package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Adapter.FilterSubBrandsAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.FilterSubCategoryAdapter;
import com.aswagnacustomer.aswagnacustomer.Adapter.FilterSubVariantAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CAT_SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SLUG;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VAR_ID;

public class FilterSubActivity extends AppCompatActivity {
    private RecyclerView categoryRecy, brand_recy, vari_recy;
    private TextView filterby_tv;
    RangeSeekBar sb_range;
    private Button apply_btn, done_btn;
    //public static ArrayList<String> brandArrayID = new ArrayList<>();
    public static ArrayList<String> variantArrayID = new ArrayList<>();
    public String variantcatID = "";
    //private String MIN_PRICE = "", MAX_PRICE = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(this);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_filter_sub);
        categoryRecy = findViewById(R.id.categoryRecy);
        brand_recy = findViewById(R.id.brand_recy);
        vari_recy = findViewById(R.id.vari_recy);
        filterby_tv = findViewById(R.id.filterby_tv);
        apply_btn = findViewById(R.id.apply_btn);
        done_btn = findViewById(R.id.done_btn);
        sb_range = (RangeSeekBar) findViewById(R.id.sb_range);
        String data = getIntent().getStringExtra("array");
        String type = getIntent().getStringExtra("type");
        if (type.equals("cat")) {
            apply_btn.setVisibility(View.GONE);
            sb_range.setVisibility(View.GONE);
            filterby_tv.setText(getString(R.string.filter_by) + "Sub Category");

            catlist(data);
        } else if (type.equals("brand")) {
            sb_range.setVisibility(View.GONE);
            filterby_tv.setText(getString(R.string.filter_by) + "Brands");
            brand(data);
        } else if (type.equals("var")) {
            sb_range.setVisibility(View.GONE);
            filterby_tv.setText(getString(R.string.filter_by) + getIntent().getStringExtra("filter_by"));
            variant(data);
        } else if (type.equals("price")) {
            apply_btn.setVisibility(View.GONE);
            done_btn.setVisibility(View.VISIBLE);
            float minPrice = (float) Float.valueOf(SearchActivity.MIN_PRIZE);
            float maxPrice = (float) Float.valueOf(SearchActivity.MAX_PRIZE)+1;

            /*MIN_PRIZE= String.valueOf(minPrice);
            MAX_PRIZE= String.valueOf(maxPrice);*/
            sb_range.setEnableThumbOverlap(false);
            sb_range.setRange(minPrice, maxPrice);
            sb_range.setProgress(minPrice, maxPrice);
            sb_range.getLeftSeekBar().setIndicatorText("KD " + String.valueOf((int) minPrice));
            sb_range.getRightSeekBar().setIndicatorText("KD " + String.valueOf((int) maxPrice));
            sb_range.setOnRangeChangedListener(new OnRangeChangedListener() {
                @Override
                public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                    int get_left_value = (int) leftValue;
                    int get_right_value = (int) rightValue;
                    view.getLeftSeekBar().setIndicatorText("KD " + get_left_value);
                    view.getRightSeekBar().setIndicatorText("KD " + get_right_value);
                    SearchActivity.MIN_PRIZE = String.valueOf(get_left_value);
                    SearchActivity.MAX_PRIZE = String.valueOf(get_right_value);
                }

                @Override
                public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

                }

                @Override
                public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

                }
            });
            //////////////////////////////////////////////////////
        }

        apply_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FilterSubActivity.this, SearchActivity.class);
                intent.putExtra("cat_slug", "0");
                intent.putExtra("sub_cat_slug", "0");
                intent.putExtra("brand", TextUtils.join(",", FilterActivity.brandArrayID));
                Log.e("ArrayBrand", TextUtils.join(",", FilterActivity.brandArrayID));
                intent.putExtra("variant", "0");
                intent.putExtra("other_variant", "0");
                if (SearchActivity.MIN_PRIZE.equals("")) {
                    intent.putExtra("price", "0");
                } else {
                    intent.putExtra("price", SearchActivity.MIN_PRIZE + "," + SearchActivity.MAX_PRIZE);
                }
                intent.putExtra("order_by", "0");
                intent.putExtra("product", "");
                startActivity(intent);
                finish();
            }
        });
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (variantArrayID.size()>0) {
                        for (int i = 0; i < FilterActivity.variJsonArray.length(); i++) {
                            if (variantcatID.equals(FilterActivity.variJsonArray.getJSONObject(i).getString("variant_id"))) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                    FilterActivity.variJsonArray.remove(i);
                                }
                            }
                        }
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("variant_id", variantcatID);
                        jsonObject.put("value_id", TextUtils.join(",", variantArrayID));
                        FilterActivity.variJsonArray.put(jsonObject);
                    }
                    FilterSubActivity.super.onBackPressed();
                } catch (JSONException e) {
                    Log.e("JSONException", e.toString());
                }
            }
        });
    }

    private void catlist(String data) {
        try {
            String cat_slug = getIntent().getStringExtra("cat_slug");
            JSONArray jsonArray = new JSONArray(data);
            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                String id = jsonArray.getJSONObject(i).getString("id");
                String slug = jsonArray.getJSONObject(i).getString("slug");
                String title = jsonArray.getJSONObject(i).getJSONObject("category_by_language").getString("title");
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(ID, id);
                hashMap.put(SLUG, slug);
                hashMap.put(CAT_SLUG, cat_slug);
                hashMap.put(TITLE, title);
                arrayList.add(hashMap);
            }
            FilterSubCategoryAdapter filterSubCategoryAdapter = new FilterSubCategoryAdapter(FilterSubActivity.this, arrayList);
            categoryRecy.setAdapter(filterSubCategoryAdapter);
            categoryRecy.setFocusable(false);
            categoryRecy.setVisibility(View.VISIBLE);
            apply_btn.setVisibility(View.GONE);
            done_btn.setVisibility(View.GONE);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONException", e.toString());
        }

    }

    private void brand(String data) {
        try {
            JSONArray jsonArray = new JSONArray(data);
            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                String id = jsonArray.getJSONObject(i).getString("id");
                String title = jsonArray.getJSONObject(i).getJSONObject("brand_details_by_language").getString("title");
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(ID, id);
                hashMap.put(TITLE, title);
                Log.e("hashMap", hashMap.toString());
                arrayList.add(hashMap);
            }
            FilterSubBrandsAdapter adapter = new FilterSubBrandsAdapter(FilterSubActivity.this, arrayList);
            brand_recy.setAdapter(adapter);
            brand_recy.setFocusable(false);
            brand_recy.setVisibility(View.VISIBLE);
            apply_btn.setVisibility(View.GONE);
            done_btn.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONException", e.toString());
        }
    }

    private void variant(String data) {
        try {
            JSONArray jsonArray = new JSONArray(data);
            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                String id = jsonArray.getJSONObject(i).getString("id");
                String varid = jsonArray.getJSONObject(i).getString("variant_id");
                String title = jsonArray.getJSONObject(i).getJSONObject("variant_value_by_language").getString("name");
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put(ID, id);
                hashMap.put(VAR_ID, varid);
                hashMap.put(TITLE, title);
                arrayList.add(hashMap);
            }
            FilterSubVariantAdapter adapter = new FilterSubVariantAdapter(FilterSubActivity.this, arrayList);
            vari_recy.setAdapter(adapter);
            vari_recy.setFocusable(false);
            vari_recy.setVisibility(View.VISIBLE);
            apply_btn.setVisibility(View.GONE);
            done_btn.setVisibility(View.VISIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("JSONException", e.toString());
        }

    }

    public void back(View view) {
        super.onBackPressed();
    }

}
