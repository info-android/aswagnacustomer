package com.aswagnacustomer.aswagnacustomer.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import io.fabric.sdk.android.Fabric;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.OrderHistoryAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CREATED_AT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_ITEM;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_TOTAL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ORDER_TYPE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PAYMENT_METHODE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PROFILE_IMAGE_URL;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.QTY;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.STATUS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TOTAL_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.UNIT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.VARY_VALUE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class DashboardActivity extends AppCompatActivity {
    private TextView name,email,Pname,pAdd,pPhone,pEmail;
    private ImageView usrPic;
    private NestedScrollView scrollView;

    private TextView order_no_tv,status_tv,order_date_tv,item_tv,total_price_tv,payment_method,view_tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_dashboard);
        usrPic = findViewById(R.id.usrPic);
        name = findViewById(R.id.name);
        email = findViewById(R.id.email);
        Pname = findViewById(R.id.Pname);
        pAdd = findViewById(R.id.pAdd);
        pPhone = findViewById(R.id.pPhone);
        pEmail = findViewById(R.id.pEmail);
        scrollView = findViewById(R.id.scrollView);

        order_no_tv = findViewById(R.id.order_no_tv);
        status_tv = findViewById(R.id.status_tv);
        order_date_tv = findViewById(R.id.order_date_tv);
        item_tv = findViewById(R.id.item_tv);
        total_price_tv = findViewById(R.id.total_price_tv);
        payment_method = findViewById(R.id.payment_method);
        view_tv = findViewById(R.id.view_tv);

        getDetails();
    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(DashboardActivity.this);
        MethodClass.setMenuColor(DashboardActivity.this,"no");
    }

    public void back(View view) {
        if(isTaskRoot()){
            startActivity(new Intent(DashboardActivity.this,HomeActivity.class));
            // using finish() is optional, use it if you do not want to keep currentActivity in stack
            finish();
        }else{
            super.onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        if(isTaskRoot()){
            startActivity(new Intent(DashboardActivity.this,HomeActivity.class));
            // using finish() is optional, use it if you do not want to keep currentActivity in stack
            finish();
        }else{
            super.onBackPressed();
        }
    }

    private void getDetails() {
        if (!isNetworkConnected(DashboardActivity.this)) {
            MethodClass.network_error_alert(DashboardActivity.this);
            return;
        }
        MethodClass.showProgressDialog(DashboardActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "user-dashboard";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(DashboardActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(DashboardActivity.this, response);
                    if (resultResponse != null) {

                        JSONObject user = resultResponse.getJSONObject("user");

                        name.setText(user.getString("fname")+" "+user.getString("lname"));
                        email.setText(user.getString("email"));
                        pEmail.setText(user.getString("email"));
                        pPhone.setText(user.getString("phone"));
                        Picasso.get().load(PROFILE_IMAGE_URL+user.getString("image")).placeholder(R.drawable.user_photo).error(R.drawable.user_photo).into(usrPic);
                        Pname.setText(user.getString("fname")+" "+user.getString("lname"));
                        if(!user.getString("address").equals("null") && !user.getString("city").equals("null") && !user.getString("state").equals("null")){
                            pAdd.setText(user.getString("address")+","+user.getString("city")+", "+user.getString("state"));
                        }else{
                            pAdd.setText("N/A");
                        }

                        scrollView.setVisibility(View.VISIBLE);


                        String order = resultResponse.getString("order");
                        if(!order.equals("null") && !order.equals(null) && !order.equals("")){
                            try {
                                JSONObject orders = resultResponse.getJSONObject("order");

                                view_tv.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(DashboardActivity.this, OrderDetailsActivity.class);

                                        try {
                                            intent.putExtra("id",orders.getString("id"));
                                            startActivity(intent);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }


                                    }
                                });
                                order_no_tv.setText(orders.getString("order_no"));
                                order_date_tv.setText(orders.getString("created_at"));
                                    Log.e("OTAO", String.valueOf(orders.getJSONArray("order_master_details").length()));
                                item_tv.setText(String.valueOf(orders.getJSONArray("order_master_details").length()));
                                total_price_tv.setText(orders.getString("order_total"));

                                if (orders.getString("payment_method").equals("O")){
                                    payment_method.setText("Online");
                                }else {
                                    payment_method.setText("Cash on delivery");
                                }
                                String status = orders.getString("status");
                                if (status.equals("N")) {
                                    status_tv.setText(getResources().getString(R.string.new_order));
                                } else if (status.equals("OA")) {
                                    status_tv.setText(getResources().getString(R.string.accepted));
                                } else if (status.equals("DA")) {
                                    status_tv.setText(getResources().getString(R.string.assigned));
                                } else if (status.equals("RP")) {
                                    status_tv.setText(getResources().getString(R.string.ready_to_pick_up));
                                } else if (status.equals("OP")) {
                                    status_tv.setText(getResources().getString(R.string.picked_up));
                                } else if (status.equals("OD")) {
                                    status_tv.setText(getResources().getString(R.string.delivered));
                                } else if (status.equals("OC")) {
                                    status_tv.setText(getResources().getString(R.string.cancelled));
                                } else if (status.equals("I")) {
                                    status_tv.setText(getResources().getString(R.string.incomplete));
                                }else if (status.equals("F")) {
                                    status_tv.setText(getResources().getString(R.string.payment_failed));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }

                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(DashboardActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(DashboardActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(DashboardActivity.this);
                } else {
                    MethodClass.error_alert(DashboardActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(DashboardActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(DashboardActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(DashboardActivity.this).addToRequestQueue(jsonObjectRequest);
    }

    public void changePass(View view){
        Intent I = new Intent(DashboardActivity.this,ChangePasswordActivity.class);
        I.putExtra("email",email.getText().toString().trim());
        startActivity(I);
    }

}
