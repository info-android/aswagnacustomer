package com.aswagnacustomer.aswagnacustomer.Activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.ShoppigCartAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CART_MASTER_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.CHECK;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCTS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRODUCT_VARIANT_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class ShoppingCartActivity extends AppCompatActivity {
    private RecyclerView recy_view;
    private TextView total_price_tv, total_items_tv;
    private Button checkouBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_shopping_cart);
        recy_view = findViewById(R.id.recy_view);
        total_price_tv = findViewById(R.id.total_price_tv);
        total_items_tv = findViewById(R.id.total_items_tv);
        checkouBtn = findViewById(R.id.checkouBtn);
    }


    @Override
    protected void onResume() {
        super.onResume();
        get_cart();
        MethodClass.bottomMenu(ShoppingCartActivity.this);
        MethodClass.setMenuColor(ShoppingCartActivity.this, "no");
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void proceedToCheckOut(View view) {
        if(!CHECK){
            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(ShoppingCartActivity.this, SweetAlertDialog.ERROR_TYPE);
            sweetAlertDialog.setTitleText(getResources().getString(R.string.out_of_stock));
            sweetAlertDialog.setContentText(getResources().getString(R.string.out_of_stock));
            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sDialog) {
                    sDialog.dismissWithAnimation();
                }
            });
            sweetAlertDialog.show();
        }else {
            checkouBtn.setEnabled(true);
            if (PreferenceManager.getDefaultSharedPreferences(ShoppingCartActivity.this).getBoolean("is_logged_in",false)){
                Intent intent = new Intent(this, CheckOutActivity.class);
                intent.putExtra("type","user");
                startActivity(intent);
            }else {
                final Dialog userSelectPopup= new Dialog(ShoppingCartActivity.this);
                userSelectPopup.setContentView(R.layout.user_select_popup);
                userSelectPopup.setCancelable(false);
                LinearLayout guest_user_lay=userSelectPopup.findViewById(R.id.guest_user_lay);
                LinearLayout new_sign_lay=userSelectPopup.findViewById(R.id.new_sign_lay);
                LinearLayout login_lay=userSelectPopup.findViewById(R.id.login_lay);
                ImageView close_btn=userSelectPopup.findViewById(R.id.close_btn);
                close_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        userSelectPopup.dismiss();
                    }
                });
                guest_user_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        userSelectPopup.dismiss();
                        Intent intent = new Intent(ShoppingCartActivity.this, CheckOutActivity.class);
                        intent.putExtra("type","guest");
                        startActivity(intent);
                    }
                });

                new_sign_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        userSelectPopup.dismiss();
                        Intent intent = new Intent(ShoppingCartActivity.this, SignUpActivity.class);
                        startActivity(intent);
                    }
                });

                login_lay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        userSelectPopup.dismiss();
                        Intent intent = new Intent(ShoppingCartActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
                userSelectPopup.show();
            }
        }


    }


    public void get_cart() {
        if (!isNetworkConnected(ShoppingCartActivity.this)) {
            MethodClass.network_error_alert(ShoppingCartActivity.this);
            return;
        }
        String android_id = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        String server_url = "";
        if (PreferenceManager.getDefaultSharedPreferences(ShoppingCartActivity.this).getBoolean("is_logged_in", false)) {
            server_url = getString(R.string.SERVER_URL) + "get-cart";
        } else {
            server_url = getString(R.string.SERVER_URL) + "get-cart/" + android_id;
        }
        Log.e("get-cart", server_url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, new JSONObject(), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(ShoppingCartActivity.this);
                Log.e("addToCartRES", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(ShoppingCartActivity.this, response);
                    if (resultResponse != null) {

                        NestedScrollView nestedScrollView = findViewById(R.id.scrollView);
                        if (resultResponse.getString("cart").equals("null")){
                            nestedScrollView.setVisibility(View.GONE);
                            SweetAlertDialog sweetAlertDialog=new SweetAlertDialog(ShoppingCartActivity.this, SweetAlertDialog.ERROR_TYPE);
                            sweetAlertDialog.setTitleText(getResources().getString(R.string.no_data));
                            sweetAlertDialog.setContentText(getResources().getString(R.string.no_data_available));
                            sweetAlertDialog.setConfirmText(getResources().getString(R.string.ok));
                            sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sDialog) {
                                    sDialog.dismissWithAnimation();
                                }
                            });
                            sweetAlertDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialogInterface) {
                                    ShoppingCartActivity.super.onBackPressed();
                                }
                            });
                            sweetAlertDialog.show();
                            return;
                        }else {
                            nestedScrollView.setVisibility(View.VISIBLE);
                        }
                        CHECK = true;
                        JSONObject cart = resultResponse.getJSONObject("cart");
                        String id = cart.getString("id");
                        String total_item = cart.getString("total_item");
                        String total_qty = cart.getString("total_qty");
                        String total = cart.getString("total");
                        total_price_tv.setText(getResources().getString(R.string.total_with_colon) + total);
                        total_items_tv.setText(total_item + getResources().getString(R.string.items));
                        JSONArray sellers=resultResponse.getJSONArray("sellers");
                        ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();
                        for (int i = 0; i < sellers.length(); i++) {
                            JSONObject object=sellers.getJSONObject(i);
                            HashMap<String,String> hashMap=new HashMap<>();
                            JSONObject seller_info=object.getJSONObject("seller_info");
                            hashMap.put(ID,object.getString("id"));
                            hashMap.put(NAME,seller_info.getString("fname")+" "+seller_info.getString("lname"));
                            hashMap.put(PRODUCTS,seller_info.getString("products"));
                            arrayList.add(hashMap);
                        }
                        ShoppigCartAdapter shoppigCartAdapter = new ShoppigCartAdapter(ShoppingCartActivity.this, arrayList);
                        recy_view.setAdapter(shoppigCartAdapter);
                        recy_view.setFocusable(false);
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(ShoppingCartActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(ShoppingCartActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(ShoppingCartActivity.this);
                } else {
                    MethodClass.error_alert(ShoppingCartActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(ShoppingCartActivity.this));
                if (PreferenceManager.getDefaultSharedPreferences(ShoppingCartActivity.this).getBoolean("is_logged_in", false)) {
                    headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(ShoppingCartActivity.this).getString("token", ""));
                }
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(ShoppingCartActivity.this).addToRequestQueue(jsonObjectRequest);
    }


}
