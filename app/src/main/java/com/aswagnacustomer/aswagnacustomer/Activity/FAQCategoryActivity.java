package com.aswagnacustomer.aswagnacustomer.Activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.FAQCategoryAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FAQ_DATA;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class FAQCategoryActivity extends AppCompatActivity {

    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> arrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_faq_category);
        recy_view = findViewById(R.id.recy_view);
        faqcatList();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(FAQCategoryActivity.this);
        MethodClass.setMenuColor(FAQCategoryActivity.this, "no");
    }

    public void back(View view) {
        super.onBackPressed();
    }


    private void faqcatList() {
        if (!isNetworkConnected(FAQCategoryActivity.this)) {
            MethodClass.network_error_alert(FAQCategoryActivity.this);
            return;
        }
        MethodClass.showProgressDialog(FAQCategoryActivity.this);
        String server_url = getString(R.string.SERVER_URL) + "show-faq";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(FAQCategoryActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(FAQCategoryActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray faqcat = resultResponse.getJSONArray("faqcat");
                        arrayList = new ArrayList<>();
                        for (int i = 0; i < faqcat.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID,faqcat.getJSONObject(i).getString("id"));
                            hashMap.put(TITLE,faqcat.getJSONObject(i).getJSONObject("category_details_language").getString("faq_category"));
                            hashMap.put(FAQ_DATA,faqcat.getJSONObject(i).getString("faq_master_t"));
                            arrayList.add(hashMap);
                        }
                        FAQCategoryAdapter faqCategoryAdapter = new FAQCategoryAdapter(FAQCategoryActivity.this, arrayList);
                        recy_view.setAdapter(faqCategoryAdapter);
                        recy_view.setFocusable(false);

                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(FAQCategoryActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(FAQCategoryActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(FAQCategoryActivity.this);
                } else {
                    MethodClass.error_alert(FAQCategoryActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(FAQCategoryActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(FAQCategoryActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(FAQCategoryActivity.this).addToRequestQueue(jsonObjectRequest);
    }
}
