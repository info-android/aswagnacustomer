package com.aswagnacustomer.aswagnacustomer.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.AddressBookAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.aswagnacustomer.aswagnacustomer.R;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ADDRESS;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.EMAIL_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IS_DEFAULT;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.NAME;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PHONE_NO;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.USER_ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class AddressBookActivity extends AppCompatActivity {
    private RecyclerView recy_view;
    private ArrayList<HashMap<String, String>> arrayList;
    private Button add_address_btn;
    private TextView total_saved_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_address_book);
        recy_view = findViewById(R.id.recy_view);
        add_address_btn = findViewById(R.id.add_address_btn);
        total_saved_tv = findViewById(R.id.total_saved_tv);
        add_address_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddressBookActivity.this, AddAddressActivity.class);
                startActivity(intent);
            }
        });

        address_book();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(AddressBookActivity.this);
        MethodClass.setMenuColor(AddressBookActivity.this, "no");
    }

    public void back(View view) {
        super.onBackPressed();
    }

    public void address_book() {
        if (!isNetworkConnected(AddressBookActivity.this)) {
            MethodClass.network_error_alert(AddressBookActivity.this);
            return;
        }
        MethodClass.showProgressDialog(AddressBookActivity.this);//sssss
        String server_url = getString(R.string.SERVER_URL) + "user-address-book";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(AddressBookActivity.this);
                Log.e("respHome", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(AddressBookActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray user_address_book = resultResponse.getJSONArray("user_address_book");
                        total_saved_tv.setText(user_address_book.length()+" "+getResources().getString(R.string.saved_addresses));
                        arrayList = new ArrayList<>();
                        for (int i = 0; i < user_address_book.length(); i++) {
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put(ID, user_address_book.getJSONObject(i).getString("id"));
                            hashMap.put(USER_ID, user_address_book.getJSONObject(i).getString("user_id"));

                            String shipping_fname = user_address_book.getJSONObject(i).getString("shipping_fname");
                            String shipping_lname = user_address_book.getJSONObject(i).getString("shipping_lname");
                            if (shipping_lname.equals("") || shipping_lname.equals(null) || shipping_lname.equals("null")){
                                shipping_lname="";
                            }
                            String name = shipping_fname +" "+ shipping_lname;
                            String city = user_address_book.getJSONObject(i).getString("city");
                            String street = user_address_book.getJSONObject(i).getString("street");
                            String block = user_address_book.getJSONObject(i).getString("block");
                            if (block.equals("") || block.equals(null) || block.equals("null")){
                                block="";
                            }
                            String building = user_address_book.getJSONObject(i).getString("building");
                            if (building.equals("") || building.equals(null) || building.equals("null")){
                                building="";
                            }
                            String more_address = user_address_book.getJSONObject(i).getString("more_address");
                            if (more_address.equals("") || more_address.equals(null) || more_address.equals("null")){
                                more_address="";
                            }
                            String country = user_address_book.getJSONObject(i).getJSONObject("get_country").getString("name");
                            String postal_code = user_address_book.getJSONObject(i).getString("postal_code");
                            if (postal_code.equals("") || postal_code.equals(null) || postal_code.equals("null")){
                                postal_code="";
                            }
                            String phone = user_address_book.getJSONObject(i).getString("phone");
                            String email = user_address_book.getJSONObject(i).getString("email");
                            String is_default = user_address_book.getJSONObject(i).getString("is_default");
                            String address = building + ", " + block + ", " + street + ", " + city + ", " + more_address + ", " + country + ", " + postal_code;

                            hashMap.put(NAME, name);
                            hashMap.put(ADDRESS, address);
                            hashMap.put(IS_DEFAULT, is_default);
                            hashMap.put(PHONE_NO, phone);
                            hashMap.put(EMAIL_ID, email);
                            arrayList.add(hashMap);
                        }
                        AddressBookAdapter bookAdapter = new AddressBookAdapter(AddressBookActivity.this, arrayList);
                        recy_view.setAdapter(bookAdapter);
                        recy_view.setFocusable(false);
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(AddressBookActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(AddressBookActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(AddressBookActivity.this);
                } else {
                    MethodClass.error_alert(AddressBookActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(AddressBookActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(AddressBookActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(AddressBookActivity.this).addToRequestQueue(jsonObjectRequest);
    }

}
