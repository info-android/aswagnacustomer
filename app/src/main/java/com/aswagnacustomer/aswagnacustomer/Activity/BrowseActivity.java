package com.aswagnacustomer.aswagnacustomer.Activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Adapter.BrowseCatAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity.catArrayListID_1;
import static com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity.catArrayListID_2;

public class BrowseActivity extends AppCompatActivity {
    private RecyclerView categoryRecy;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MethodClass.set_locale(this);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_browse);
        categoryRecy=findViewById(R.id.categoryRecy);
        if (PreferenceManager.getDefaultSharedPreferences(BrowseActivity.this).getString("LANG", "").equals("en")) {
            BrowseCatAdapter adapter = new BrowseCatAdapter(BrowseActivity.this, catArrayListID_1);
            categoryRecy.setAdapter(adapter);
            categoryRecy.setFocusable(false);
        } else {
            BrowseCatAdapter adapter = new BrowseCatAdapter(BrowseActivity.this, catArrayListID_2);
            categoryRecy.setAdapter(adapter);
            categoryRecy.setFocusable(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(BrowseActivity.this);
        MethodClass.setMenuColor(BrowseActivity.this,"browse");
    }
    public void back(View view) {
        super.onBackPressed();
    }

}
