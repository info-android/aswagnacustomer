package com.aswagnacustomer.aswagnacustomer.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.aswagnacustomer.aswagnacustomer.Adapter.PaymentHistoryAdapter;
import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import io.fabric.sdk.android.Fabric;

public class MoreActivity extends AppCompatActivity {

    private LinearLayout dashboard_lin,order_history_lay,address_book_lin,my_wish_list_lin,payment_lin,my_reward_point_lin;
    private LinearLayout about_us_lin,faq_lin,contact_lin,logout_lay_lin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_more);
        dashboard_lin=findViewById(R.id.dashboard_lin);
        order_history_lay=findViewById(R.id.order_history_lay);
        address_book_lin=findViewById(R.id.address_book_lin);
        my_wish_list_lin=findViewById(R.id.my_wish_list_lin);
        payment_lin=findViewById(R.id.payment_lin);
        my_reward_point_lin=findViewById(R.id.my_reward_point_lin);
        about_us_lin=findViewById(R.id.about_us_lin);
        faq_lin=findViewById(R.id.faq_lin);
        contact_lin=findViewById(R.id.contact_lin);
        logout_lay_lin=findViewById(R.id.logout_lay_lin);

        dashboard_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this,DashboardActivity.class);
                startActivity(intent);
            }
        });
        order_history_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this,OrderHistoryActivity.class);
                startActivity(intent);
            }
        });
        address_book_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this,AddressBookActivity.class);
                startActivity(intent);
            }
        });
        my_wish_list_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this,WishListActivity.class);
                startActivity(intent);
            }
        });
        my_reward_point_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this,MyRewardPointActivity.class);
                startActivity(intent);
            }
        });
        payment_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this, MyPaymentHistoryActivity.class);
                startActivity(intent);
            }
        });
        about_us_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this,AboutActivity.class);
                startActivity(intent);
            }
        });
        faq_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this,FAQCategoryActivity.class);
                startActivity(intent);
            }
        }); contact_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MoreActivity.this,ConactUsActivity.class);
                startActivity(intent);
            }
        });
        logout_lay_lin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(MoreActivity.this, SweetAlertDialog.WARNING_TYPE).setTitleText(getResources().getString(R.string.log_ouy))
                        .setContentText(getResources().getString(R.string.do_you_want_to_log_out)).setConfirmText(getResources().getString(R.string.okay)).setCancelText(getResources().getString(R.string.cancel)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                        MethodClass.Userlogout(MoreActivity.this);
                    }
                }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismissWithAnimation();
                    }
                }).show();

            }
        });

    }
    public void back(View view) {
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(MoreActivity.this);
        MethodClass.setMenuColor(MoreActivity.this,"more");
    }

}
