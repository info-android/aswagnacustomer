package com.aswagnacustomer.aswagnacustomer.Activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Adapter.HomeNewProdAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MySingleton;
import com.crashlytics.android.Crashlytics;
import com.aswagnacustomer.aswagnacustomer.Adapter.WishListAdapter;
import com.aswagnacustomer.aswagnacustomer.Helper.MethodClass;
import com.aswagnacustomer.aswagnacustomer.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.fabric.sdk.android.Fabric;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.DISCOUNT_PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.FROM_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.ID;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.IMAGE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.PRICE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TITLE;
import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.TO_DATE;
import static com.aswagnacustomer.aswagnacustomer.Helper.MethodClass.isNetworkConnected;

public class WishListActivity extends AppCompatActivity {
    private RecyclerView recy_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        MethodClass.set_locale(this);
        setContentView(R.layout.activity_wish_list);
        recy_view=findViewById(R.id.recy_view);
        myWishList();
    }

    public void back(View view) {
        super.onBackPressed();
    }
    @Override
    protected void onResume() {
        super.onResume();
        MethodClass.bottomMenu(WishListActivity.this);
        MethodClass.setMenuColor(WishListActivity.this,"no");
    }



    public void myWishList() {
        if (!isNetworkConnected(WishListActivity.this)) {
            MethodClass.network_error_alert(WishListActivity.this);
            return;
        }
        MethodClass.showProgressDialog(WishListActivity.this);
        String server_url  = getString(R.string.SERVER_URL) + "my-wish-list";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(WishListActivity.this);
                Log.e("responce::", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(WishListActivity.this, response);
                    if (resultResponse != null) {
                        JSONArray favProduct = resultResponse.getJSONArray("favProduct");
                        recy_view.setAdapter(null);
                        if (favProduct.length()>0){
                            ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
                            for (int i = 0; i < favProduct.length(); i++) {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put(ID, favProduct.getJSONObject(i).getJSONObject("gett_product").getString("id"));
                                hashMap.put(FROM_DATE, favProduct.getJSONObject(i).getJSONObject("gett_product").getString("from_date"));
                                hashMap.put(TO_DATE, favProduct.getJSONObject(i).getJSONObject("gett_product").getString("to_date"));
                                hashMap.put(PRICE, favProduct.getJSONObject(i).getJSONObject("gett_product").getString("price"));
                                hashMap.put(DISCOUNT_PRICE, favProduct.getJSONObject(i).getJSONObject("gett_product").getString("discount_price"));
                                if (!favProduct.getJSONObject(i).getJSONObject("gett_product").getString("default_image").equals("null")) {
                                    hashMap.put(IMAGE, favProduct.getJSONObject(i).getJSONObject("gett_product").getJSONObject("default_image").getString("image"));
                                }
                                hashMap.put(TITLE, favProduct.getJSONObject(i).getJSONObject("gett_product").getJSONObject("product_by_language").getString("title"));
                                arrayList.add(hashMap);
                            }
                            WishListAdapter wishListAdapter = new WishListAdapter(WishListActivity.this, arrayList);
                            recy_view.setAdapter(wishListAdapter);
                            recy_view.setFocusable(false);
                        }
                    }
                } catch (JSONException e) {
                    MethodClass.error_alert(WishListActivity.this);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());

                MethodClass.hideProgressDialog(WishListActivity.this);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(WishListActivity.this);
                } else {
                    MethodClass.error_alert(WishListActivity.this);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization", MethodClass.check_locale_lang(WishListActivity.this));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(WishListActivity.this).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(WishListActivity.this).addToRequestQueue(jsonObjectRequest);
    }


}
