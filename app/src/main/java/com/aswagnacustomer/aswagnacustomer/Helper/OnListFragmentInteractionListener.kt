package com.aswagnacustomer.aswagnacustomer.Helper

import com.myfatoorah.sdk.model.initiatepayment.PaymentMethod

interface OnListFragmentInteractionListener {
    fun onListFragmentInteraction(position: Int, item: PaymentMethod)
}