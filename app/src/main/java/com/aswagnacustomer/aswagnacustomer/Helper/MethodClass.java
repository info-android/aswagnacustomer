package com.aswagnacustomer.aswagnacustomer.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.JsonObjectRequest;
import com.aswagnacustomer.aswagnacustomer.Activity.BrowseActivity;

import com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity;

import com.aswagnacustomer.aswagnacustomer.Activity.LoginActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.MoreActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.OrderHistoryActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.SignUpActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.SplashActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.VerifyActivity;
import com.aswagnacustomer.aswagnacustomer.Activity.WishListActivity;
import com.aswagnacustomer.aswagnacustomer.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MethodClass {

    static public void bottomMenu(final Activity activity) {
        LinearLayout home_lay, browse_lay, order_lay, more_lay,login_lay,signup_lay;
        home_lay = activity.findViewById(R.id.home_lay);
        browse_lay = activity.findViewById(R.id.browse_lay);
        order_lay = activity.findViewById(R.id.order_lay);
        more_lay = activity.findViewById(R.id.more_lay);
        login_lay = activity.findViewById(R.id.login_lay);
        signup_lay = activity.findViewById(R.id.signup_lay);
        ImageView home_img, browse_img, order_img, more_img;
        home_img = activity.findViewById(R.id.home_img);
        browse_img = activity.findViewById(R.id.browse_img);
        order_img = activity.findViewById(R.id.order_img);
        more_img = activity.findViewById(R.id.more_img);
        TextView home_tv, browse_tv, order_tv, more_tv;
        home_tv = activity.findViewById(R.id.home_tv);
        browse_tv = activity.findViewById(R.id.browse_tv);
        order_tv = activity.findViewById(R.id.order_tv);
        more_tv = activity.findViewById(R.id.more_tv);

        if (PreferenceManager.getDefaultSharedPreferences(activity).getBoolean("is_logged_in", false)) {
            login_lay.setVisibility(View.GONE);
            signup_lay.setVisibility(View.GONE);
            order_lay.setVisibility(View.VISIBLE);
            more_lay.setVisibility(View.VISIBLE);
        }else {
            login_lay.setVisibility(View.VISIBLE);
            signup_lay.setVisibility(View.VISIBLE);
            order_lay.setVisibility(View.GONE);
            more_lay.setVisibility(View.GONE);
        }

        home_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activity.getLocalClassName().equals("Activity.HomeActivity")){
                    Intent intent = new Intent(activity, HomeActivity.class);
                    activity.startActivity(intent);
                }
            }
        });

        browse_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activity.getLocalClassName().equals("Activity.BrowseActivity")) {
                    Intent intent = new Intent(activity, BrowseActivity.class);
                    activity.startActivity(intent);
                }
                /*Intent intent = new Intent(activity, SearchActivity.class);
                intent.putExtra("cat_slug","0");
                intent.putExtra("sub_cat_slug","0");
                intent.putExtra("brand","0");
                intent.putExtra("variant","0");
                intent.putExtra("other_variant","0");
                intent.putExtra("price","0");
                intent.putExtra("order_by","0");
                activity.startActivity(intent);*/
            }
        });

        order_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activity.getLocalClassName().equals("Activity.OrderHistoryActivity")) {
                    Intent intent = new Intent(activity, OrderHistoryActivity.class);
                    activity.startActivity(intent);
                }
            }
        });
        more_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!activity.getLocalClassName().equals("Activity.MoreActivity")) {
                    Intent intent = new Intent(activity, MoreActivity.class);
                    activity.startActivity(intent);
                }
            }
        });
        login_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity,LoginActivity.class);
                activity.startActivity(intent);
            }
        });
        signup_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity, SignUpActivity.class);
                activity.startActivity(intent);
            }
        });
    }


    public static void signout(Activity activity) {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();
        final GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(activity, gso);
        if (mGoogleSignInClient != null) {
            mGoogleSignInClient.signOut();
            Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(activity).getAll();
            for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {
                if (!prefToReset.getKey().equals("LANG")) {
                    PreferenceManager.getDefaultSharedPreferences(activity).edit().remove(prefToReset.getKey()).commit();
                }
            }
            Intent intent = new Intent(activity, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
            activity.finish();
        }

        CallbackManager callbackManager = CallbackManager.Factory.create();
        FacebookSdk.sdkInitialize(activity);
        if (AccessToken.getCurrentAccessToken() != null) {
            new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest.Callback() {
                @Override
                public void onCompleted(GraphResponse graphResponse) {
                    LoginManager.getInstance().logOut();
                }
            }).executeAsync();
            Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(activity).getAll();
            for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {
                if (!prefToReset.getKey().equals("LANG")) {
                    PreferenceManager.getDefaultSharedPreferences(activity).edit().remove(prefToReset.getKey()).commit();
                }
            }
            Intent intent = new Intent(activity, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
            activity.finish();
        } else {
            Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(activity).getAll();
            for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {
                if (!prefToReset.getKey().equals("LANG")) {
                    PreferenceManager.getDefaultSharedPreferences(activity).edit().remove(prefToReset.getKey()).commit();
                }
            }
            Intent intent = new Intent(activity, SplashActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
            activity.finish();
        }
    }


    public static void setMenuColor(Activity activity, String wichClass) {
        setMenuDefoultColor(activity);
        LinearLayout home_lay, browse_lay, order_lay, more_lay;
        home_lay = activity.findViewById(R.id.home_lay);
        browse_lay = activity.findViewById(R.id.browse_lay);
        order_lay = activity.findViewById(R.id.order_lay);
        more_lay = activity.findViewById(R.id.more_lay);
        ImageView home_img, browse_img, order_img, more_img;
        home_img = activity.findViewById(R.id.home_img);
        browse_img = activity.findViewById(R.id.browse_img);
        order_img = activity.findViewById(R.id.order_img);
        more_img = activity.findViewById(R.id.more_img);
        TextView home_tv, browse_tv, order_tv, more_tv;
        home_tv = activity.findViewById(R.id.home_tv);
        browse_tv = activity.findViewById(R.id.browse_tv);
        order_tv = activity.findViewById(R.id.order_tv);
        more_tv = activity.findViewById(R.id.more_tv);
        if (wichClass.equals("home")) {
            home_lay.setBackgroundColor(activity.getResources().getColor(R.color.red));
            home_img.setColorFilter(activity.getResources().getColor(R.color.white));
            home_tv.setTextColor(activity.getResources().getColor(R.color.white));
        }
        if (wichClass.equals("browse")) {
            browse_lay.setBackgroundColor(activity.getResources().getColor(R.color.red));
            browse_img.setColorFilter(activity.getResources().getColor(R.color.white));
            browse_tv.setTextColor(activity.getResources().getColor(R.color.white));
        }
        if (wichClass.equals("order")) {
            order_lay.setBackgroundColor(activity.getResources().getColor(R.color.red));
            order_img.setColorFilter(activity.getResources().getColor(R.color.white));
            order_tv.setTextColor(activity.getResources().getColor(R.color.white));
        }
        if (wichClass.equals("more")) {
            more_lay.setBackgroundColor(activity.getResources().getColor(R.color.red));
            more_img.setColorFilter(activity.getResources().getColor(R.color.white));
            more_tv.setTextColor(activity.getResources().getColor(R.color.white));
        }

    }

    public static void setMenuDefoultColor(Activity activity) {
        LinearLayout home_lay, browse_lay, order_lay, more_lay;
        home_lay = activity.findViewById(R.id.home_lay);
        browse_lay = activity.findViewById(R.id.browse_lay);
        order_lay = activity.findViewById(R.id.order_lay);
        more_lay = activity.findViewById(R.id.more_lay);
        ImageView home_img, browse_img, order_img, more_img;
        home_img = activity.findViewById(R.id.home_img);
        browse_img = activity.findViewById(R.id.browse_img);
        order_img = activity.findViewById(R.id.order_img);
        more_img = activity.findViewById(R.id.more_img);
        TextView home_tv, browse_tv, order_tv, more_tv;
        home_tv = activity.findViewById(R.id.home_tv);
        browse_tv = activity.findViewById(R.id.browse_tv);
        order_tv = activity.findViewById(R.id.order_tv);
        more_tv = activity.findViewById(R.id.more_tv);

        home_lay.setBackgroundColor(activity.getResources().getColor(R.color.null_color));
        home_img.setColorFilter(null);
        home_tv.setTextColor(activity.getResources().getColor(R.color.textGreyColor));

        browse_lay.setBackgroundColor(activity.getResources().getColor(R.color.null_color));
        browse_img.setColorFilter(null);
        browse_tv.setTextColor(activity.getResources().getColor(R.color.textGreyColor));

        order_lay.setBackgroundColor(activity.getResources().getColor(R.color.null_color));
        order_img.setColorFilter(null);
        order_tv.setTextColor(activity.getResources().getColor(R.color.textGreyColor));

        more_lay.setBackgroundColor(activity.getResources().getColor(R.color.null_color));
        more_img.setColorFilter(null);
        more_tv.setTextColor(activity.getResources().getColor(R.color.textGreyColor));
    }



    public static void set_locale(Activity activity) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        Resources res = activity.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration conf = res.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            conf.setLocale(new Locale(check_locale_lang(activity).toLowerCase())); // API 17+ only.
        } else {
            conf.locale = new Locale(check_locale_lang(activity));
        }
        res.updateConfiguration(conf, dm);
    }

    public static String check_locale_lang(Activity activity) {
        String languageToLoad = PreferenceManager.getDefaultSharedPreferences(activity).getString("LANG", "en");
        return languageToLoad;
    }

    public static void hide_keyboard(Context context) {
        ((AppCompatActivity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    public static void go_to_next_activity(Activity activity, Class next_activity) {
        activity.startActivity(new Intent(activity, next_activity));
    }

    static Dialog mDialog;
    public static void showProgressDialog(Activity activity) {
        mDialog = new Dialog(activity);
        mDialog.setCancelable(false);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mDialog.setContentView(R.layout.custom_progress_dialog);
        mDialog.show();
    }

    public static void hideProgressDialog(Activity activity) {
        if (mDialog != null) {
            mDialog.dismiss();
        }
    }

    public static boolean emailValidator(String email) {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }


    public static JSONObject Json_rpc_format(HashMap<String, String> params) {
        HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        return new JSONObject(main_param);
    }

    public static JSONObject Json_rpc_format_obj(HashMap<String, Object> params) {
        HashMap<String, Object> main_param = new HashMap<String, Object>();
        main_param.put("params", new JSONObject(params));
        main_param.put("jsonrpc", "2.0");
        return new JSONObject(main_param);
    }

    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public static JSONObject get_result_from_webservice(Activity activity, JSONObject response) {
        JSONObject result = null;
        if (response.has("error")) {
            try {
                String error = response.getString("error");
                JSONObject jsonObject = new JSONObject(error);
                if (jsonObject.getString("code").equals("-33085")) {
                    Toast.makeText(activity, jsonObject.getString("meaning"), Toast.LENGTH_SHORT).show();
                    Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(activity).getAll();
                    for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {
                        if (!prefToReset.getKey().equals("LANG")) {
                            PreferenceManager.getDefaultSharedPreferences(activity).edit().remove(prefToReset.getKey()).commit();
                        }
                    }

                    Intent intent = new Intent(activity, LoginActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    activity.startActivity(intent);


                } else {
                    Log.e("RESPO", jsonObject.getString("message") );
                    if(jsonObject.getString("message").equals("The token has been blacklisted") || jsonObject.getString("message").equals("Token Signature could not be verified.")){
                        Map<String, ?> prefs = PreferenceManager.getDefaultSharedPreferences(activity).getAll();
                        for (Map.Entry<String, ?> prefToReset : prefs.entrySet()) {
                            if (!prefToReset.getKey().equals("LANG")) {
                                PreferenceManager.getDefaultSharedPreferences(activity).edit().remove(prefToReset.getKey()).commit();
                            }
                        }

                        Intent intent = new Intent(activity, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        activity.startActivity(intent);
                    }else {
                        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                                .setTitleText(jsonObject.getString("message"))
                                .setContentText(jsonObject.getString("meaning"))
                                .setConfirmText(activity.getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.dismissWithAnimation();
                            }
                        }).show();
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (response.has("result")) {
            try {
                result = response.getJSONObject("result");
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("methodeJSONException", e.toString());
            }

        } else {
            Log.e("methodeElse", "methode Else");
            error_alert(activity);
        }
        return result;
    }

    public static void error_alert(Activity activity) {
        try {
            if (!activity.isFinishing()) {
                new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE).setTitleText(activity.getResources().getString(R.string.oops))
                        .setContentText(activity.getResources().getString(R.string.something_went_wrong)).setConfirmText(activity.getResources().getString(R.string.ok)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {
                        sDialog.dismissWithAnimation();
                    }
                }).show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void network_error_alert(final Activity activity) {
        new SweetAlertDialog(activity, SweetAlertDialog.ERROR_TYPE)
                .setTitleText(activity.getResources().getString(R.string.network_error))
                .setContentText(activity.getResources().getString(R.string.please_check_your_internet_connection))
                .setConfirmText(activity.getResources().getString(R.string.settings)).setCancelText(activity.getResources().getString(R.string.okay)).setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
                activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 1);
            }
        }).setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.dismissWithAnimation();
            }
        }).show();
    }

    public static String getRightAngleImage(String photoPath) {

        try {
            ExifInterface ei = new ExifInterface(photoPath);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            int degree = 0;

            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    degree = 0;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
                case ExifInterface.ORIENTATION_UNDEFINED:
                    degree = 0;
                    break;
                default:
                    degree = 90;
            }

            return rotateImage(degree, photoPath);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return photoPath;
    }

    public static String rotateImage(int degree, String imagePath) {

        if (degree <= 0) {
            return imagePath;
        }
        try {
            Bitmap b = BitmapFactory.decodeFile(imagePath);

            Matrix matrix = new Matrix();
            if (b.getWidth() > b.getHeight()) {
                matrix.setRotate(degree);
                b = Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
            }

            FileOutputStream fOut = new FileOutputStream(imagePath);
            String imageName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
            String imageType = imageName.substring(imageName.lastIndexOf(".") + 1);

            FileOutputStream out = new FileOutputStream(imagePath);
            if (imageType.equalsIgnoreCase("png")) {
                b.compress(Bitmap.CompressFormat.PNG, 100, out);
            } else if (imageType.equalsIgnoreCase("jpeg") || imageType.equalsIgnoreCase("jpg")) {
                b.compress(Bitmap.CompressFormat.JPEG, 100, out);
            }
            fOut.flush();
            fOut.close();

            b.recycle();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return imagePath;
    }

    public static class StringWithTag {
        public String string;
        public Object tag;

        public StringWithTag(String stringPart, Object tagPart) {
            string = stringPart;
            tag = tagPart;
        }
        @Override
        public String toString() {
            return string;
        }
    }
    public static class StringWithTag2 {
        public String string;
        public String string2;
        public Object tag;

        public StringWithTag2(String stringPart,String stringPart2, Object tagPart) {
            string = stringPart;
            string2 = stringPart2;
            tag = tagPart;
        }
        @Override
        public String toString() {
            return string;
        }
    }

    public static void Userlogout(final Activity activity){
        if (!isNetworkConnected(activity)) {
            MethodClass.network_error_alert(activity);
            return;
        }
        MethodClass.showProgressDialog(activity);
        String server_url = activity.getString(R.string.SERVER_URL) + "logout";
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject jsonObject = MethodClass.Json_rpc_format(params);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, server_url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MethodClass.hideProgressDialog(activity);
                Log.e("respLogout", response.toString());
                try {
                    JSONObject resultResponse = MethodClass.get_result_from_webservice(activity, response);
                    if (resultResponse != null) {
                        signout(activity);
                        //PreferenceManager.getDefaultSharedPreferences(activity).edit().putBoolean("is_logged_in", false).commit();
                        /*Intent  intent=new Intent(activity, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent);*/
                    }
                } catch (Exception e) {
                    MethodClass.error_alert(activity);
                    e.printStackTrace();
                    Log.e("JSONException", e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // Log.e("error2", error.getMessage());
                MethodClass.hideProgressDialog(activity);
                if (error.toString().contains("ConnectException")) {
                    MethodClass.network_error_alert(activity);
                } else {
                    MethodClass.error_alert(activity);
                }
            }
        }) {
            //* Passing some request headers*
            @Override
            public Map getHeaders() throws AuthFailureError {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
headers.put("X-device", "A");
                headers.put("X-localization",MethodClass.check_locale_lang(activity));
                headers.put("Authorization", "Bearer " + PreferenceManager.getDefaultSharedPreferences(activity).getString("token", ""));
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        MySingleton.getInstance(activity).addToRequestQueue(jsonObjectRequest);
    }

    public  static boolean equalLists(List<String> one, List<String> two){
        if (one == null && two == null){
            return true;
        }

        if((one == null && two != null)
                || one != null && two == null
                || one.size() != two.size()){
            return false;
        }

        //to avoid messing the order of the lists we will use a copy
        //as noted in comments by A. R. S.
        one = new ArrayList<String>(one);
        two = new ArrayList<String>(two);

        Collections.sort(one);
        Collections.sort(two);

        Log.e("ONE", one.toString()+"                "+two.toString() );
        return one.equals(two);
        //return one.containsAll(two) && two.containsAll(one);
    }



}
