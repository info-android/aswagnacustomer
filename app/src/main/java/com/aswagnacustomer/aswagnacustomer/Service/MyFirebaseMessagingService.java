package com.aswagnacustomer.aswagnacustomer.Service;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity;
import com.aswagnacustomer.aswagnacustomer.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.aswagnacustomer.aswagnacustomer.Helper.Constants.SHARED_PREF;

/**
 * Created by Computer29 on 9/21/2017.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();
    private String id = "", notification_type = "", title = "", description = "", date = "";
    @Override
    public void onNewToken(String s) {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                        String token = task.getResult().getToken();
                        Log.e("My Token",token);
                        SharedPreferences pref = getApplicationContext().getSharedPreferences(SHARED_PREF, 0);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("regId", token);
                        editor.commit();
                    }
                });
    }
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage);
        Log.e(TAG, "remoteMessage: " + remoteMessage.getData());
        Log.e(TAG, "remoteMessage: " + remoteMessage);
        if (remoteMessage == null)
            return;
        for (int i = 0; i < 5; i++) {
            Log.i(TAG, "Working... " + (i + 1)
                    + "/5 @ " + SystemClock.elapsedRealtime());
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
        JSONObject jsonObj = new JSONObject(remoteMessage.getData());
        Log.e("jsonObj", jsonObj.toString());

        try {
            title = jsonObj.getString("title");
            description = jsonObj.getString("body");
            handleSimpleNoti(title, description, notification_type);

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "remoteError: " + e.toString());
        }
    }


    public void handleSimpleNoti(String title, String description, String notification_type) {
        int icon = R.drawable.logo;
        int requestID = (int) System.currentTimeMillis();
        String CHANNEL_ID = "1002";// The id of the channel.
        CharSequence name = "Aswagna Customer";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        final NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        RemoteViews contentView;
        contentView = new RemoteViews(getPackageName(), R.layout.custom_notification);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            contentView.setTextViewText(R.id.text, (Html.fromHtml(description.replace("\n", "<br>"), Html.FROM_HTML_MODE_COMPACT)));
            contentView.setTextViewText(R.id.title, Html.fromHtml(title.replace("\n", "<br>"), Html.FROM_HTML_MODE_COMPACT));
        } else {
            contentView.setTextViewText(R.id.text, Html.fromHtml(description.replace("\n", "<br>")));
            contentView.setTextViewText(R.id.title, Html.fromHtml(title.replace("\n", "<br>")));
        }

        Intent notificationIntent = null;
        notificationIntent = new Intent(this, HomeActivity.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent contentIntent = PendingIntent.getActivity(this, requestID, notificationIntent, 0);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
            builder.setContentTitle(title);
            builder.setContentText(description);
            builder.setContent(contentView);
            builder.setSmallIcon(icon);
            builder.setStyle(new NotificationCompat.BigPictureStyle().setBigContentTitle(title));
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(description));
            builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
            builder.setContentIntent(contentIntent);
            builder.setChannelId(CHANNEL_ID);
            builder.setAutoCancel(true);
            final Notification notification = builder.build();

            mNotificationManager.createNotificationChannel(mChannel);
            String activity = getForegroundActivity();
            if (activity.equals("com.aswagnacustomer.aswagnacustomer.Activity.HomeActivity")) {
                Intent intent = new Intent("updateTicket");
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
                mNotificationManager.notify(1, notification);
            } else {
                mNotificationManager.notify(1, notification);
            }
        } else {
            Log.e("else","asaaaaaaa");
            final Notification notification = new Notification.Builder(getApplicationContext())
                    .setContentTitle(title)
                    .setContentText(description)
                    .setContent(contentView)
                    .setSmallIcon(icon)
                    .setStyle(new Notification.BigTextStyle().setBigContentTitle(title))
                    .setStyle(new Notification.BigTextStyle().bigText(description))
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setContentIntent(contentIntent)
                    .setAutoCancel(true)
                    .build();
            mNotificationManager.notify(1, notification);
        }
    }

    @SuppressLint("NewApi")
    public String getForegroundActivity() {
        ActivityManager am = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        Log.d("topActivity", "CURRENT Activity ::" + taskInfo.get(0).topActivity.getClassName());
        return taskInfo.get(0).topActivity.getClassName();
    }
}
